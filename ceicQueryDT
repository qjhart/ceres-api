#!/usr/bin/perl
##
## Spatial search for catalog entries
##
use CGI qw/:standard/;
use DBI;
use pfLib;


sanitize;
db_connect();

open (LOGFILE, '>>/var/log/ceicQueryDT.log');

#my $xmlBase = "http://ceic.resources.ca.gov/xml/";
my $xmlBase = "http://atlas.ca.gov/catalog/";

print LOGFILE "\n$ENV{QUERY_STRING}\n";
my $topic = lc(param('topic'));
my $cat = lc(param('cat'));
my $content = lc(param('content'));
my $term = lc(param('term'));
my $title = lc(param('title'));
my $titlesub = lc(param('titlesub'));
my $phrase = lc(param('phrase'));
my $org = lc(param('org'));
my $role = lc(param('role'));
my $issue = lc(param('issue'));
my $projectType = lc(param('projectType'));
my $fundType = lc(param('fundType'));
my $fmt = lc(param('fmt'));
my $bbox = lc(param('bbox'));
my $node = param('node');
my $srid = param('srid');
my $collection = param('collection');
my $qStrOrder = param('order');
my $callback = param('callback');
my $linked = param('linked');
my $statewide = param('statewide');

$srid = 4269 if (!($srid));

$term = $phrase if (!($term));

my $regterm = $term; 
$regterm =~ s/\s/&/g;

$topic =~ s/geoscientific/Geoscientific/i;
$topic =~ s/atmosphereAndClimate/Atmosphere/i;
$topic =~ s/imageryBaseMapsLandCover/Imagery-Base Maps-Land Cover Land Cover/i;
$topic =~ s/landCover/Imagery-Base Maps-Land Cover Land Cover Land Cover/i;
$topic =~ s/imagery/Imagery-Base Maps-Land Cover Land Cover Imagery/i;
$topic =~ s/baseMaps/Imagery-Base Maps-Land Cover Land Cover Base Maps/i;
$topic =~ s/planning|planningAndDevelopment/Planning/i;
$topic =~ s/inlandWaters/Inland Waters/i;
$topic =~ s/demographics/Society Demographics/i;
$topic =~ s/cultural/Society Cultural/i;
$topic =~ s/utilitiesAndCommunication/Utilities/i;


## Check for no query params
if (!($phrase || $topic || $cat || $term || $title || $titlesub || $org || $bbox || $issue || $projectType || $content || $collection || ($node))) {
    print qq/{"ResultSet":{"Where":"No Filters","Result":[]}}/;
    exit;
}

my $dbh= DBI->connect("dbi:Pg:dbname=gforge");
my $st,$sql,$cnt,$where;
my @names;

my @idArray = (); # results in array
my @titleArray = (); # results in array
my @catArray = (); # results in array
my @srcArray = (); # results in array
my @areaArray = (); # results in array
my @linkArray = (); # results in array
my @xmlArray = (); # results in array
my @orgArray = (); # results in array

$sql = qq[SELECT distinct d.id, d.title, d.category, n.website,0,n.shortname,d.link_url,'$xmlBase' || n.shortname || '/' || d.shortname || '.html' as meta_url,d.rank ];
$from = qq[FROM dataset d, ceic_node n ];
$where = qq[WHERE (d.status = 'public') AND (d.ceic_node_id <> 6) AND ((d.link_url_resp = '200') OR (d.dist_url_resp = '200')) AND d.ceic_node_id = n.id];

if ($cat) {
    if ($cat =~ /gis/) { $cat .= ",vector digital data,raster digital data,map,spatial data,wms,gis-data,gis-map,gis-service"};
    if ($cat =~ /other/) { $cat .= ",website,computer file,tabular digital data"};
    if ($cat =~ /org/) { $cat .= ",organization"};
    my @type = split(/,/, $cat);
    my $quotedTypes = uc(join("','",@type)); 
    $where .= qq[ and upper(d.category) IN ('$quotedTypes')];
    $whereText .= qq[resource type IN ('$cat') ];
}
if ($term || $phrase) {
    $sql = qq[SELECT d.id, f.headline, d.category, astext(transform(d.centroid,$srid)), d.area,n.shortname,d.link_url,'$xmlBase' || n.shortname || '/' || d.shortname || '.xml' as meta_url, f.rank, d.rank ];
    $from .= qq[, dresults('$regterm') f ];

    if ($phrase) {
	$where .= qq[ and f.id = d.id and (lower(d.title) ~ '.*$phrase.*' or lower(abstract) ~ '.*$phrase.*' or lower(searchterms) ~ '.*$phrase.*') ];
    }
    else {
	$where .= qq[ and f.id = d.id ];
    }
    $whereText .= qq[term of '$term' ];
    $order = qq[  f.rank DESC ];
}

if ($topic) {
    $from .= qq[, dataset_theme t ];
    $where .= qq[ AND t.dataset_id = d.id];
    $where .= qq[ AND upper(t.themekey) LIKE upper('%$topic%') AND upper(t.thesaurusname) = 'CAL ATLAS TOPIC CATEGORIES'];
}

if ($projectType) {
    $from .= qq[, dataset_content c ];
    $where .= qq[ AND c.dataset_id = d.id];
    $where .= qq[ AND lower(c.content) LIKE '$projectType%'];
}

if ($content) {
    $from .= qq[, dataset_content c ];
    $where .= qq[ AND c.dataset_id = d.id];
    my @cnt = split(/,/, $content);
    my $quotedContents = uc(join("','",@cnt)); 
    $where .= qq[ AND upper(c.content) IN ('$quotedContents')];
}

if ($fundType) {
    $from .= qq[, dataset_funding f ];
    $where .= qq[ AND f.dataset_id = d.id ];
    $where .= qq[ AND lower(f.program_type) LIKE '$fundType%'];
}

if ($collection) {
    $from .= qq[, ds_collection coll ];
    $where .= qq[ AND coll.dataset_id = d.id];
    $where .= qq[ AND coll.collection_id = $collection];
}

if ($node) {
    $where .= qq[ and d.ceic_node_id IN ($node) ];
    $whereText .= qq[ CEIC Node IN $node ];
}

## if ($statewide) {
if (0) {
    $where .= qq[ and d.statewide ];
    $whereText .= qq[ statewide dataset ];
}

if ($title) {
    $where .= qq[ and upper(d.title) like upper('$title%')];
    $whereText .= qq[ Title like $title ];
}

if ($titlesub) {
    $where .= qq[ and upper(d.title) like upper('%$titlesub%')];
    $whereText .= qq[ Title like $titlesub ];
}

if ($linked) {
    $where .= qq[ and (d.link_url_resp = '200' OR d.dist_url_resp = '200')];
}

if ($org) {
    $sql .= qq[, o.role ];
    $from .= qq[, dataset_org o ];
    $org = smartName($org);
    if ($role) {
	if ($role eq 'participant') {
         $where .= qq[ AND o.dataset_id = d.id and (upper(o.shortname) LIKE upper('%$org%') AND upper(o.role) NOT IN ('FUNDER','LEAD AGENCY'))];
	}
	else {
	  my @roles = split(/,/,$role);
	  my $roleStr = "'" . uc(join("','",@roles)) . "'";
         $where .= qq[ AND o.dataset_id = d.id and (upper(o.shortname) LIKE upper('%$org%') AND upper(o.role) IN (] . $roleStr . '))';
	}
    }
    else {
       $where .= qq[ AND o.dataset_id = d.id and upper(o.shortname) LIKE upper('%$org%') ];
   }
    $whereText .= qq[ AND organization = $org ];
}

if ($poly) {
    $sql .= qq[, distance(centroid(envelope(GeometryFromText('$poly',1))), d.centroid) as delta ];
    $order = qq[  delta ];
#    $where .= qq[and (d.geom && GeometryFromText('$poly',1) and Intersects(Buffer(GeometryFromText('$poly',1),10000),envelope(d.geom))) ];
    $where .= qq[and (d.centroid && GeometryFromText('$poly',1)) ];
    $whereText .= qq[ intersecting map extent ]; 
}
elsif ($poly && 0)  {
    $where .= qq[ AND (d.geom && GeometryFromText('$poly',1))];
    $whereText .= qq[ intersecting map extent ]; 
}

if (!($qStrOrder)) {
    $order = qq[  d.title ] if (!($order));
}
else {
    $qStrOrder = 'd.last_update DESC' if ($qStrOrder =~ /recent/i);  
    $order = qq[  $qStrOrder ];
}

#$sql .= $from . $where . $order . "OFFSET $offset LIMIT $page_size";

	if ($statewide) {
	    $sql .= $from . $where . "ORDER BY d.rank," . $order . " LIMIT 20";
	}
	else {
	    $sql .= $from . $where . "ORDER BY " . $order . " LIMIT 20";
	}

my $sqlCount = 'select count(*) ' . $from . $where;
$st = $dbh->prepare($sqlCount);
$st->execute();
@row = $st->fetchrow_array;
my $recCount = $row[0];

## Log the SQL
print LOGFILE "\n$sqlCount\n";
print LOGFILE "\n$sql\n";


my @timeData = localtime(time);
my $t = $timeData[1] . ':' . $timeData[0];
print LOGFILE "\n$t";

$st = $dbh->prepare($sql);
$st->execute();


@timeData = localtime(time);
$t = $timeData[1] . ':' . $timeData[0];
print LOGFILE " -- $t\n";
close(LOGFILE);
while (@row = $st->fetchrow_array) {
     ## purge all double quotes!
    foreach $i (0,1,2,3,4,5,6) {
        $row[$i] =~ s/(['"])/\\$1/g;
        $row[$i] =~ s/\n//g;
        $row[$i] = 'Null' if ($row[$i] =~ /^HASH/);
#        $row[$i] =~ s/([()])/-/g;
#	$row[$i] =~ s/\"/\'/g;
        $row[$i] = '' if ($row[$i] =~ /^\\/);
    }

    ## Taking website domain as a "src" value
    $row[3] =~ s/^http:\/\///i;
    $row[3] =~ s/\/.*//;
    $row[3] =~ s/^www\.//;
    ## preventing some repeats
    my $currentIds = join(' ',@idArray);
    my $id = $row[0];
    if (!($currentIds =~ /$id/)) {

	push(@idArray,"$row[0]");
	push(@titleArray,"$row[1]");
	push(@catArray,"$row[2]");
	push(@srcArray,"$row[3]");
	push(@linkArray,"$row[6]");
        push(@xmlArray,"$row[7]");
	push(@roleArray,"$row[8]") if ($org);
   }
}

    print "Content-type: text/html\n\n";
##    print header;
    my $jsonText;

$whereText = '';
my $jsonText = '';
if ($callback) { $jsonText = qq/$callback(/ }

    $jsonText .=  qq/{"ResultSet":{"ResultType":"ds","Where":"$whereText","recCount":"$recCount","Result":[/;
    for $i (0..((@idArray) - 1)) {
	$jsonText .= "{\"id\":\"$idArray[$i]\",\"label\":\"$titleArray[$i]\",\"cat\":\"$catArray[$i]\"";
	$jsonText .= ",\"src\":\"$srcArray[$i]\"";
	$jsonText .= ",\"link\":\"$linkArray[$i]\"";
	$jsonText .= ",\"xml\":\"$xmlArray[$i]\"";
	$jsonText .= ",\"idx\":\"$i\"";
	$jsonText .= "},";
    }


$jsonText =~ s/\,$//;
$jsonText .= qq/]}}/;
if ($callback) { $jsonText .= qq/)/ }
print $jsonText;



