sub sanitize {
#### Input Sanitation ####################################
foreach $param (param()) {
    if (param($param)) {
    unless ( param($param) =~ /^[\w .!-?:\/]+$/ ) {
        print    "You entered an invalid character. " .
               "You may only enter letters, numbers, " .
               "underscores, spaces, periods, exclamation " .
               "points, question marks, and hyphens.";
        exit;
    }
}
}
##########################################################
}

sub db_connect {
     $dbh = DBI->connect("dbi:Pg:dbname=gis");
     $dbh3 = DBI->connect("dbi:Pg:dbname=gforge");
     $dbh3->do("SET search_path TO ceic, public");
}

sub db_disconnect {
    $dbh->disconnect();
    $dbh3->disconnect();
}

sub gnis2shp {
    my $clip = shift;
    my $sql,$name;

    my @id = split(":",$clip); 
    $sql = qq[select name,type from geonamea where gid = '$id[1]'];
    my $st = $dbh->prepare($sql);    
    $st->execute();
    my @row = $st->fetchrow_array();

    if (($row[1] = 'civil') && ($row[0] =~ /county$/i)) {
	## it's a county
	$clip = "counties:$row[0]";
	$clip =~ s/county//i;
	$clip =~ s/\s+$//;
    }

    return uc($clip)
    }

sub coverageLbl {
    my $coverage = shift;

    my %Shplbl;
    $Shplbl{calwater22} = 'Watershed';
    $Shplbl{counties} = 'County';
    $Shplbl{bioregions} = 'Bioregion';
    $Shplbl{usgsquads} = 'Mapname';
    $Shplbl{city2k} = 'City';
    $Shplbl{airbasins} = 'Airbasin';
    $Shplbl{bioregions} = 'Bioregion';
    $Shplbl{water_bodies} = 'Waterbody';
    $Shplbl{states} = 'State';

    return $Shplbl{$coverage};
}

sub clip2coverage {
    my $clip = shift;

    my @clipparse = split(':',$clip);
    my $coverage = lc($clipparse[0]);
    my $name = lc($clipparse[1]);

    if ($coverage =~ /bioregion/) {return 'bioregion'}
    elsif ($coverage =~ /calwater|watershed/) {return 'calwater22'}
    elsif ($coverage =~ /county|counties/) {return 'counties'}
    elsif ($coverage =~ /grid|quad/) {return 'usgsquads'}
    elsif ($coverage =~ /city2k|urban_area/) {return 'city2k'}
    elsif ($coverage =~ /airbasin/) {return 'airbasins'}
    elsif ($coverage =~ /water_bod/) {return 'water_bodies'}
    elsif ($coverage =~ /states|ca_outline|california|statewide/) {return 'states'}

}
sub coverage2auth {
    my $coverage = shift;

    if ($$coverage =~ /bioregion|calwater22|urban_areas/) {return 'FRAP'}
    elsif ($$coverage =~ /counties|asmplan4|sen_law|cng_law|hydrogp|water_bodies/) {return 'CMCC'}
    elsif ($$coverage =~ /geoname|gnis|calgnis/) {return 'GNIS'}
    elsif ($$coverage =~ /zip_codes|city2k|school/) {return 'Census'}
    elsif ($$coverage =~ /usgsquads/) {return 'DOQQ'}
}

sub clip2names {
    my $clip = shift;
    $$auth = shift;
    $$coverage = shift;
    $$name = shift;
    $$lbl = shift;
    $coverage_lbl = shift;

    my @clipparse = split(':',$clip);

    $name = lc($clipparse[1]);
    $coverage = clip2coverage($clip);
    $lbl = coverageLbl($coverage);
    $auth = coverage2auth($coverage);
}

sub smartName {

    my $name = shift;

    @parts = map ucfirst, split(' ',$name);
    $name = join(' ' , @parts);

    $name =~ s/^California //gi;
    $name =~ s/California/CA/gi;
    $name =~ s/Department Of //gi;
    $name =~ s/Town Of //gi;
    $name =~ s/County Of //gi;
    $name =~ s/City Of //gi;
    $name =~ s/Ca Dept. Of //gi;
    $name =~ s/County/Co/gi;
    $name =~ s/City/Ci/gi;
    $name =~ s/Catalog//gi;
    $name =~ s/Metropolitan/Metro/gi;
    $name =~ s/Association/Assoc/gi;
    $name =~ s/Department/Depart/gi;
    $name =~ s/District/Dist/gi;
    $name =~ s/Consortium/Consort/gi;
    $name =~ s/Commission/Comm/gi;
    $name =~ s/Division/Div/gi;
    $name =~ s/Environmental/Env/gi;
    $name =~ s/Program/Prog/gi;
    $name =~ s/Management/Man/gi;
    $name =~ s/Conservancy/Consrv/gi;
    $name =~ s/Government/Gov/gi;
    $name =~ s/Municipal/Muni/gi;
    $name =~ s/Transportation/Trans/gi;

    # Get rid of blanks and non word chars
    $name =~ s/[\s,\.]//gi;
    $name =~ s/\W//g;
 
    return $name;

}

sub transformBBox {
    my $eastbc = shift;
    my $westbc = shift;
    my $northbc = shift;
    my $southbc = shift;
    my $fromSRID = shift; 
    my $toSRID = shift; 
    
    my $sql = qq/select ST_Box2d(srid2srid($$westbc,$$southbc,$$eastbc,$$northbc,$fromSRID,$toSRID))/;
    $st = $dbh->prepare($sql);
    $st->execute();
    my @row = $st->fetchrow_array();
    $r = $row[0];
    $r =~ s/^BOX\(//i;
    $r =~ s/\)$//;
    $r =~ s/,/ /g;
    my @v = split(/ /,$r);
    $$westbc = $v[0];
    $$southbc = $v[1];
    $$eastbc = $v[2];
    $$northbc = $v[3];
    return;
}

sub getExtent {

    my $clip = shift;
    my $srid = shift;

    my ($coverage,$name) = split /:/,$clip;
    $coverage = clip2coverage($clip);
    my ($sql,$st);

    $sql = qq/SELECT xmax(extent(transform(footprint,$srid))), xmin(extent(transform(footprint,$srid))), ymax(extent(transform(footprint,$srid))), ymin(extent(transform(footprint,$srid))) FROM gazetteer2 WHERE  coverage = '$coverage' AND upper(name) like upper('$name')/;
    
    $st = $dbh->prepare($sql);
    $st->execute();
    @ext = $st->fetchrow_array();
    $st->finish;
    
    return \@ext;

}

sub getPolygon {

    my $clip = shift;
    my $srid = shift;
    my $area = shift;
    $dbHandle = shift;

    my ($coverage,$name) = split(/:/,$clip);
#    $coverage = clip2coverage($clip);
    my ($sql,$poly);

    if ($coverage eq 'city2k') {
	$sql = qq/select AsText(ST_MEMUnion(footprint)) from gazetteer2 where coverage = '$coverage' and upper(trim(name)) = upper('$name') group by name/;
    }
    else {
	$sql = qq/select AsText(ST_Simplify(footprint,.001)) from gazetteer2 where coverage = '$coverage' and upper(trim(name)) = upper('$name') order by area DESC/;
    }

	if ($dbHandle) {
	    $st = $dbHandle->prepare($sql) || return 0;
	}
	else {
	    $st = $dbh->prepare($sql) || return 0;
	}

	$st->execute() || return 0;
        return 0 if (!(@row = $st->fetchrow_array)); 
        $st->finish;
	$poly = $row[0];
	## sorry, area causes prob
	##$$area = $row[1];
	$$area = 0;

    return $poly
}

sub getExtPolygon {

    my $ext = shift;
    my $srid = shift;
    $area = shift;
    my $inPrj = shift;

    $inPrj = 4269 if (!($inPrj));
    my ($sql,$poly);

    ## format the ext values for the BOX3D syntax
    @extArray = split(/,/,$ext);

    $sql = qq/select AsText(transform(envelope(SetSRID('BOX3D($extArray[1] $extArray[0],$extArray[3] $extArray[2])'::BOX3D,$inPrj)),$srid)), Area(envelope(transform(SetSRID('BOX3D($extArray[0] $extArray[1],$extArray[2] $extArray[3])'::box3d,$inPrj),$srid)))/;
#    $sql = qq/select AsText(transform(envelope(SetSRID('BOX3D($extArray[0] $extArray[2],$extArray[1] $extArray[3])'::BOX3D,$inPrj)),$srid)), Area(envelope(transform(SetSRID('BOX3D($extArray[0] $extArray[2],$extArray[1] $extArray[3])'::box3d,$inPrj),$srid)))/;
    $st = $dbh->prepare($sql);
    $st->execute();
    @row = $st->fetchrow_array; 
    $st->finish;
    $poly = $row[0];
    $$area = $row[1];

    return $poly
}

sub getCentroid {

    my $clip = shift;
    my $srid = shift;

    $srid = 4249 if (!($srid));

    my ($sql,$lat_long);
    my ($coverage,$name) = split(/:/,$clip);
    $coverage = clip2coverage($clip);

	$sql = qq/select astext(centroid(footprint)) from gazetteer2 where coverage = '$coverage' AND upper(trim(name)) = upper('$name')/;

	$st = $dbh->prepare($sql);
	$st->execute();
        @row = $st->fetchrow_array; 
        $st->finish;
	$lat_long = $row[0];
    $lat_long =~ s/^POINT\(//i;
	$lat_long =~ s/\)$//;
	@ll = split(/ /,$lat_long);
	return \@ll;
}

sub gaz_intersect {

    my $clip = shift;
    my $lbl = shift;

    my ($sql,$poly);
    my (@views,@matchList);
    my $area;

    $lbl = $name if (!($lbl));
    $poly = getPolygon($clip,3310,\$area);

    $sql = qq[select coverage, description from gazetteer where (transform(simplify(footprint,.2),3310) && GeometryFromText('$poly',3310)) and (coverage IN ('zip_codes','urban_areas','counties')) and intersects(transform(footprint,3310),GeometryFromText('$poly',3310)) and area(difference(transform(footprint,3310),GeometryFromText('$poly',3310))) < (area(GeometryFromText('$poly',3310)) * .8) and description <> '' order by coverage];
#    $sql = qq[select coverage, description from gazetteer where (simplify(footprint,.2) && GeometryFromText('$poly',4269)) and (coverage IN ('zip_codes','urban_areas','calwater22','counties')) and intersects(footprint,GeometryFromText('$poly',4269)) and description <> '' order by coverage];

    my $nq = $dbh->prepare($sql);
    $nq->execute();

    my ($shpname,$coverage);
    while (@row = $nq->fetchrow_array) {
      $coverage = $row[0];
      $shpname = $row[1];
      push(@matchList,"$coverage:$shpname");
   }

    return \@matchList;
}

sub related {

    my $clip = shift;
    my $lbl = shift;
    my $toShp = shift;
    my $mode = shift;
    my $ext = shift;
    
    my ($sql,$poly);
    my (@views,@terms,@types);
    my $area;
    my %results = (); # results in a hash of arrays

    my ($coverage,$name) = split(/:/,$clip);
    $coverage = clip2coverage($clip);

    $lbl = $name if (!($lbl));

    if ($ext) {
	$poly = getExtPolygon($ext,4269,\$area);
    }
    else {
	$poly = getPolygon($clip,4269,\$area);
    }
    
    $sql = qq/SELECT clip,label,coverage FROM gazetteer2 WHERE ST_Intersects(footprint,GeometryFromText('$poly',4269)) AND coverage = ? ORDER BY ST_Distance(centroid(footprint),centroid(GeometryFromText('$poly',4269)))  LIMIT 10/;

#    $sql = qq/SELECT clip,label,coverage FROM gazetteer2 WHERE ST_Intersects(footprint,GeometryFromText('$poly',4269)) AND coverage = ? AND (ST_Area(footprint) < ST_Area(GeometryFromText('$poly',4269))*? OR ?) ORDER BY area DESC LIMIT 20/;

#    $sql = qq/SELECT clip,label,coverage FROM gazetteer2 WHERE ST_Intersects(footprint,GeometryFromText('$poly',4269)) AND coverage = ? AND (ST_Area(footprint) < ST_Area(GeometryFromText('$poly',4269))*? OR ?) ORDER BY ST_Distance(centroid(footprint),centroid(GeometryFromText('$poly',4269))) LIMIT 20/;

#    $sql = qq/SELECT clip,label,coverage FROM gazetteer2 WHERE coverage = ? AND (footprint && GeometryFromText('$poly',4269)) ORDER BY ST_Distance(footprint,GeometryFromText('$poly',4269)) LIMIT 5/;
#    $sql = qq/SELECT clip,label,coverage FROM gazetteer2 WHERE coverage = ? AND ST_Intersects(footprint,GeometryFromText('$poly',4269)) AND ST_DWithin(centroid(footprint), centroid(GeometryFromText('$poly',4269)), 1) ORDER BY ST_Distance(centroid(footprint),centroid(GeometryFromText('$poly',4269)))  LIMIT 5/;
#    $sql = qq/SELECT clip,label,coverage FROM gazetteer2 WHERE coverage = ? AND ST_Intersects(footprint,GeometryFromText('$poly',4269)) AND (length(clip) < 18 OR coverage <> 'calwater22' ) ORDER BY ST_Distance(centroid(footprint),centroid(GeometryFromText('$poly',4269)))  LIMIT 5/;
    $nq = $dbh->prepare($sql);

#    foreach $t ('counties','bioregion','airbasin','city2k','water_bodies','calwater22','usgsquads') {
    foreach $t ('counties','city2k','water_bodies','calwater22','usgsquads') {
	$nq->execute($t);
#	$nq->execute($t,2,0);
#	if ($nq->rows < 4 ) {
#	    $nq->execute($t,4,0);
#	    if ($nq->rows < 4) {
#		$nq->execute($t,1,1);
#		
#	    }
#	}

    $results{$to_lbl} = ();

    while (@row = $nq->fetchrow_array) {
      $id = $row[0];
      $labl = $row[1];
      $src = $row[2];
      
      push(@{$results{coverageLbl($src)}},"$id|$labl|$src");
    }

    }

    my $jsonText;
    my @extArray = split(/,/,$ext);
    my $east  = sprintf "%.5f", $extArray[0];
    my $west  = sprintf "%.5f", $extArray[1];
    my $north  = sprintf "%.5f", $extArray[2];
    my $south  = sprintf "%.5f", $extArray[3];

    $jsonText =  qq/{/;
    while (($key, $value) = each(%results)) {
	next if ((@{$value}) < 1);
	 $jsonText .= qq/"$key":[/; 
	 for $i (0..((@{$value}) - 1)) {
	     @pair = split(/\|/,$value->[$i]);
	     $jsonText .= "{\"id\":\"$pair[0]\",\"label\":\"$pair[1]\",\"src\":\"$pair[2]\"},"
	     }
         $jsonText =~ s/\,$//;
	 $jsonText .= qq/],/;
    }

    $jsonText =~ s/\,$//;
    $jsonText .= qq/}/;

    return $jsonText;
}

sub relatedTop {

    my $sql;
    my (@views,@terms,@types);
    my %results = (); # results in a hash of arrays

    $lbl = $name if (!($lbl));
    @views = ('county_view','city2k','hr_view', 'asmplan4','sen_law','cng_law');

    foreach $view (@views) {
	if ($view eq 'city2k') {
	    $to_auth = 'Census';
	    $to_coverage = 'city2k';
	    $to_lbl = 'PopulatedPlaces';	    
	}
	elsif ($view eq 'county_view') {
	    $to_auth = 'CMCC';
	    $to_coverage = 'counties';
	    $to_lbl = 'County';
	}
	elsif ($view eq 'hr_view') {
	    $to_auth = 'FRAP';
	    $to_coverage = 'calwater22';
	    $to_lbl = 'Watersheds';
	}
	elsif ($view eq 'asmplan4') {
	    $to_auth = 'CMCC';
	    $to_coverage = 'asmplan4';
	    $to_lbl = 'AssemblyDistricts';
	}
	elsif ($view eq 'sen_law') {
	    $to_auth = 'CMCC';
	    $to_coverage = 'sen_law';
	    $to_lbl = 'SenateDistricts';
	}
	elsif ($view eq 'cng_law') {
	    $to_auth = 'CMCC';
	    $to_coverage = 'cng_law';
	    $to_lbl = 'CongressionalDistricts';
	}

       if ($view eq 'city2k') {
	    $sql = qq/SELECT distinct name,label,'Census2000 Urban Areas' FROM city2k where type = 'City' and population > 200000 order by name/;
	    $nq = $dbh->prepare($sql);
	    $nq->execute();
	}
       elsif ($view eq 'hr_view') {
	    $sql = qq/SELECT idnum,hrname,'CalWater Hydrologic Regions' FROM hr order by hrname/;
	    $nq = $dbh->prepare($sql);
	    $nq->execute();
	}
       elsif ($view eq 'asmplan4') {
	    $sql = qq/SELECT distinct id, 'Assembly ' || id, 'California Assembly Districts' FROM asmplan4 order by id/;
	    $nq = $dbh->prepare($sql);
	    $nq->execute();
	}
       elsif ($view eq 'sen_law') {
	    $sql = qq/SELECT distinct district,('Senate ' || district),'California Senate Districts' FROM  sen_law order by district/;
	    $nq = $dbh->prepare($sql);
	    $nq->execute();
	}
       elsif ($view eq 'cng_law') {
	    $sql = qq/SELECT distinct district,('Congress ' || district),'California Congressional Districts' FROM cng_law order by district/;
	    $nq = $dbh->prepare($sql);
	    $nq->execute();
	}
	elsif ($view eq 'county_view')  {
	    $sql = qq/SELECT distinct description,description,'California Counties' FROM county_view order by description LIMIT 58/;
	    $nq = $dbh->prepare($sql);
	    $nq->execute();
	}

    $results{$to_lbl} = ();

    while (@row = $nq->fetchrow_array) {
      $id = $row[0];
      $labl = $row[1];
      $src = $row[2];
      
      push(@{$results{$to_lbl}},"$to_coverage:$id|$labl|$src");
   }

    }

    my $jsonText;

    $jsonText =  qq/{/;
    while (($key, $value) = each(%results)) {
	 $jsonText .= qq/"$key":[/; 
	 for $i (0..((@{$value}) - 1)) {
	     @pair = split(/\|/,$value->[$i]);
	     $jsonText .= "{\"id\":\"$pair[0]\",\"label\":\"$pair[1]\",\"src\":\"$pair[2]\"},"
	     }
         $jsonText =~ s/\,$//;
	 $jsonText .= qq/],/;
    }

    $jsonText =~ s/\,$//;
    $jsonText .= qq/}/;

    return $jsonText;
}


sub cnddb_query {
    @args = ("wget http://maps.dfg.ca.gov/webservices/cnddbquad/quad.asmx/GetQuadInfo?quadcode=3811911", $_);
    system qq[(@args)];
    print qq[$_];
}

sub address2ll {
 
    my $adr = shift;
    my ($ora) = Geo::Coder::US->geocode($adr);
    my @matches = ($ora->{long}, $ora->{lat});
    return @matches;
}

sub address2ll_old {

    my $adr = shift;
    my $response = `echo $adr | address2ll`;
    if ($response !~ /success/i) {
	return 0
	}
    else {
	$response =~ s/.*At/At/s;
	$response =~ s/End.*//is;
	$response =~ s/.*@//s;
	$response =~ s/\s//g;
	return split(',',$response);
    }
    
}

sub ceic_intersect {

    my $clip = shift;
    my $lbl = shift;
    my $topic = shift;
    my $content = shift;
    
    my $area;

    if ($ext) {
	$poly = getExtPolygon($ext,3310,\$area);
    }
    else {
	$poly = getPolygon($clip,3310,\$area);
    }

    $sql = qq[SELECT  d.id,d.title, area(difference(envelope(geom),envelope(GeometryFromText('$poly',3309)))) as fit ];
    $from = qq[FROM dataset_place p, dataset d, ceic_node c ];
    $where = qq[WHERE d.ceic_node_id = c.id and p.dataset_id = d.id ];
    if ($topic) {
        $from .= qq[, dataset_theme t ];
        $where .= qq[and t.dataset_id = d.id and upper(t.themekey) = UPPER('$topic') ];
    }
    if ($content) {
        $from .= qq[, dataset_content c ];
        $where .= qq[and c.dataset_id = d.id and upper(c.content) = UPPER('$content') ];
    }

    $where .= qq[and (area(difference(envelope(geom),envelope(GeometryFromText('$poly',3309)))) < 20000000000) 
	    order by fit limit 100];

    $sql .= $from . $where;
    $dq = $dbh3->prepare($sql);
#    $dq->execute();

#    print $sql;

    print qq[<span class="subtitle">Matched on Spatial Query</span><br>] if ($dq->execute()); 
    print qq[<table border=0>];
    while (@row = $dq->fetchrow_array) {
      push(@id,$row[0]);
      push(@title,$row[1]);
    }
    $dq->finish();

    ## now match on the place name occuring in the title or placekey fields
    $lbl =~ s/\(.*\)//;
    if ($topic) {
	$sql = qq/SELECT d.id,d.title 
	FROM dataset d, dataset_place p, ceic_node c, dataset_theme t  
	WHERE d.ceic_node_id = c.id AND p.dataset_id = d.id AND t.dataset_id = d.id AND 
	(UPPER(placekey) LIKE UPPER('$lbl%') OR UPPER(d.title) LIKE UPPER('$lbl%')) AND
	upper(t.themekey) = UPPER('$topic') AND  
	ORDER BY title LIMIT 100/;    
    }
    else {
	$sql = qq/SELECT d.id,d.title
	FROM dataset d, dataset_place p, ceic_node c 
	WHERE d.ceic_node_id = c.id AND p.dataset_id = d.id AND 
	(UPPER(placekey) LIKE UPPER('$lbl%') OR UPPER(d.title) LIKE UPPER('$lbl%')) 
	ORDER BY title LIMIT 100/;    
    }

    $nq = $dbh3->prepare($sql);
    $nq->execute();

    @row = [];
    while (@row = $nq->fetchrow_array) {
      push(@id,$row[0]);
      push(@$title,$row[1]);
      }

    $nq->finish();
}


1
