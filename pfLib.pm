sub sanitize {
#### Input Sanitation ####################################
foreach $param (param()) {
    if (param($param)) {
    unless ( param($param) =~ /^[\w .!-?:\/]+$/ ) {
        print    "You entered an invalid character. " .
               "You may only enter letters, numbers, " .
               "underscores, spaces, periods, exclamation " .
               "points, question marks, and hyphens.";
        exit;
    }
}
}
##########################################################
}

sub db_connect {
     $dbh = DBI->connect("dbi:Pg:dbname=gis");
     $dbh3 = DBI->connect("dbi:Pg:dbname=gforge");
     $dbh3->do("SET search_path TO ceic, public");
}

sub db_disconnect {
    $dbh->disconnect();
    $dbh3->disconnect();
}

sub gnis2shp {
    my $clip = shift;
    my $sql,$name;

    my @id = split(":",$clip); 
    $sql = qq[select name,type from geonamea where gid = '$id[1]'];
    my $st = $dbh->prepare($sql);    
    $st->execute();
    my @row = $st->fetchrow_array();

    if (($row[1] = 'civil') && ($row[0] =~ /county$/i)) {
	## it's a county
	$clip = "counties:$row[0]";
	$clip =~ s/county//i;
	$clip =~ s/\s+$//;
    }

    return uc($clip)
    }

sub clip2names {
    my $clip = shift;
    $$auth = shift;
    $$coverage = shift;
    $$name = shift;
    $$lbl = shift;
    $coverage_lbl = shift;

    my %Shplbl;
    $Shplbl{calwater22} = 'Watershed';
    $Shplbl{zip_codes} = 'Zip Code';
    $Shplbl{urban_areas} = 'Urban Area';
    $Shplbl{counties} = 'County';
    $Shplbl{bioregions} = 'Bioregion';
    $Shplbl{grid} = '7.5min Quads';
#    $Shplbl{geoname} = 'GNIS Placename';
    $Shplbl{gnis} = 'GNIS Placename';
    $Shplbl{city2k} = 'Populated Place';
    $Shplbl{asmplan4} = 'Assembly District';
    $Shplbl{sen_law} = 'Senate District';
    $Shplbl{cng_law} = 'Congressional District';
    $Shplbl{hydrogp} = 'Water Bodies';

    my @clipparse = split(':',$clip);
    $$coverage = lc($clipparse[0]);
    $$name = lc($clipparse[1]);
    if ($$coverage =~ /bioregion|calwater22|urban_areas/) {$auth = 'FRAP'}
    elsif ($$coverage =~ /counties|asmplan4|sen_law|cng_law|hydrogp/) {$auth = 'CMCC'}
    elsif ($$coverage =~ /geoname|gnis|calgnis/) {$auth = 'GNIS'}
    elsif ($$coverage =~ /zip_codes|city2k|school/) {$auth = 'Census'}
    elsif ($$coverage =~ /grid/) {$auth = 'DOQQ'}

    my ($sql,$st);

    my %view;
    $view{counties} = 'county_view';
    $view{bioregions} = 'bioregion_view';
    $view{grid} = 'grid_view';
    $view{urban_areas} = 'urbanarea_view';
    $view{zip_codes} = 'zip_view';
##    $view{geoname} = 'geoname';
    $view{gnis} = 'calgnis';
    $view{city2k} = 'city2k';
    $view{asmplan4} = 'asmplan4';
    $view{sen_law} = 'sen_law';
    $view{cng_law} = 'cng_law';
    $view{hydrogp} = 'hydrogp';

    if (length($$name) eq 2) {
	$view{calwater22} = 'hr'
	}
    elsif (length($$name) eq 5) {
	$view{calwater22} = 'hu'
	}
    elsif (length($$name) eq 7) {
	$view{calwater22} = 'ha'
	}
    elsif (length($$name) eq 8) {
	$view{calwater22} = 'hsa'
	}
    elsif (length($$name) eq 10) {
	$view{calwater22} = 'spws'
	}
    elsif (length($$name) eq 12) {
	$view{calwater22} = 'pws'
	}

    my $db = 'gis';
    if ($$coverage =~  /counties|bioregions|urban_areas|zip_codes|ca_outline/) {
	$sql = qq/SELECT description FROM $view{$$coverage} WHERE  upper(description) = upper('$$name')/;
    }
    elsif ($$coverage eq 'calwater22') {
	$sql = qq/SELECT $view{calwater22}name FROM $view{calwater22} WHERE idnum = '$$name'/;
    }
#    elsif ($$coverage eq 'geoname') {
#	$sql = qq/SELECT name FROM geonamea WHERE gid = $$name/;
#    }
    elsif ($$coverage =~ /gnis|calgnis/) {
	$sql = qq/SELECT feature_na FROM calgeonames2009 WHERE feature_id = $$name/;
    }
    elsif ($$coverage eq 'hydrogp') {
	$sql = qq/SELECT name FROM hydrogp WHERE upper(name) = upper('$$name')/;
    }
    elsif ($$coverage eq 'city2k') {
	$sql = qq/SELECT label FROM city2k WHERE upper(name) = upper('$$name')/;
    }
    elsif ($$coverage eq 'asmplan4') {
	$sql = qq/SELECT id FROM asmplan4 WHERE id = $$name/;
    }
    elsif ($$coverage eq 'sen_law') {
	$sql = qq/SELECT district FROM sen_law WHERE district = '$$name'/;
    }
    elsif ($$coverage eq 'cng_law') {
	$sql = qq/SELECT district FROM cng_law WHERE district = $$name/;
    }
    elsif ($$coverage eq 'school_dist_u') {
	$sql = qq/SELECT first_name FROM school_dist_u WHERE upper(first_name) = upper('$$name')/;
    }
    elsif ($$coverage eq 'grid') {
	$sql = qq/SELECT mapname FROM mapgrida WHERE upper(substr(ocode,2,7)) =  upper('$$name')/;
    }
    
    my $st = $dbh->prepare($sql);
    $st->execute();
    my @row = $st->fetchrow_array();
    $st->finish;
    
##    $lbl = "$row[0] ($Shplbl{$$coverage})";
    $lbl = "$row[0]";
    $coverage_lbl = $Shplbl{$$coverage};
}

sub display_location {

    my $clip = shift;
    my $lbl = shift;
    my $back = shift;
    my $linkto = shift;
    my $coverage_lbl = shift;
    my $exts = shift;

    $linkto = 'placefinder' if (!($linkto));
    $lbl = $clip if (!($lbl));

    if ($exts) {
	print qq[<a href="$linkto?clip=$clip"><img height=80 width=80 src="http://atlas.ca.gov/cgi-bin/test/spotshape?ext=$exts;size=80" alt="County Map" usemap="#locMap"></a>];
    }
    else {
	print qq[<a href="$linkto?clip=$clip"><img height=80 width=80 src="http://atlas.ca.gov/cgi-bin/test/spotshape?clip=$clip;size=80" alt="County Map" usemap="#locMap"></a>];

    }
   county_ismap();
}

sub smartName {

    my $name = shift;

    @parts = map ucfirst, split(' ',$name);
    $name = join(' ' , @parts);

    $name =~ s/^California //gi;
    $name =~ s/California/CA/gi;
    $name =~ s/Department Of //gi;
    $name =~ s/Town Of //gi;
    $name =~ s/County Of //gi;
    $name =~ s/City Of //gi;
    $name =~ s/Ca Dept. Of //gi;
    $name =~ s/County/Co/gi;
    $name =~ s/City/Ci/gi;
    $name =~ s/Catalog//gi;
    $name =~ s/Metropolitan/Metro/gi;
    $name =~ s/Association/Assoc/gi;
    $name =~ s/Department/Depart/gi;
    $name =~ s/District/Dist/gi;
    $name =~ s/Consortium/Consort/gi;
    $name =~ s/Commission/Comm/gi;
    $name =~ s/Division/Div/gi;
    $name =~ s/Environmental/Env/gi;
    $name =~ s/Program/Prog/gi;
    $name =~ s/Management/Man/gi;
    $name =~ s/Conservancy/Consrv/gi;
    $name =~ s/Government/Gov/gi;
    $name =~ s/Municipal/Muni/gi;
    $name =~ s/Transportation/Trans/gi;

    # Get rid of blanks and non word chars
    $name =~ s/[\s,\.]//gi;
    $name =~ s/\W//g;
 
    return $name;

}

sub display_intersect {

    my $auth = shift;
    my $coverage = shift;
    my $name = shift;
    my $lbl = shift;
    my $size = shift;
    my $toShp = shift;
    my $mapName = shift;

    $linkto = 'placefinder' if (!($linkto));
    $mapName = 'shpMap' if (!($mapName));

    print qq[<img height=$size width=$size name="mainImage" src="http://atlas.ca.gov/cgi-bin/test/showShpIntersect?clip=$clip&amp;toShp=$toShp" alt="Intersecting $toShp(s)" usemap="#$mapName"><br><br>];
    map_intersect($auth,$coverage,$name,$toShp,$size,$linkto,800,$mapName);
}

sub display_quadMap {

    my $auth = shift;
    my $coverage = shift;
    my $name = shift;
    my $lbl = shift;
    my $mapName = shift;
    my $linkto = shift;

    $linkto = 'placefinder' if (!($linkto));

    $mapName = 'quadMap' if (!($mapName));
    my $quad = substr($name,0,5) if ($name);
    my $cell = $name if (length($name) eq 7);

    $size = 120;
    print qq[<img height=$size width=$size name="mainImage" src="http://atlas.ca.gov/cgi-bin/test/spotquad?code=$quad&amp;cell=$cell&amp;spot=1&amp;zoom=1&amp;width=$size&amp;height=$size" alt="Quad Spotter" usemap="#$mapName"><br><br>];
    map_quad($quad,0,0,$size,$linkto);
}

sub display_general {

    my $auth = shift;
    my $coverage = shift;
    my $name = shift;
    my $lbl = shift;
    my $size = shift;
    my $ptBuffer = shift;
    my $linkto = shift;
    my $mode = shift;

    $linkto = 'placefinder' if (!($linkto));
    $ptBuffer = 0 if (!($ptBuffer));

    $size = 160 if (!($size));    
    $lbl = $name if (!($lbl));

    print qq[<img height=$size width=$size src="http://atlas.ca.gov/cgi-bin/test/spotshape?clip=$clip&amp;zoom=1&amp;size=$size&amp;ptBuffer=$ptBuffer;mode=$mode" alt="Location">];
}

sub display_quad {

    my $auth = shift;
    my $coverage = shift;
    my $name = shift;
    my $lbl = shift;
    my $mapName = shift;
    my $linkto = shift;

    $linkto = 'placefinder' if (!($linkto));

    $mapName = 'quadMap' if (!($mapName));
    $lbl = $name if (!($lbl));
    my ($stath,$sql);

    $name =~ s/^o//;
    my $quad = substr($name,0,5) if ($name);
    my $cell = $name if (length($name) eq 7);

    my $size;

    $size = 350; 

    my (@row,@geom);

    if ($cell) {
	$size = 350; 
	print qq[<img height=$size width=$size name="mainImage" src="http://atlas.ca.gov/cgi-bin/test/placeview?auth=DOQQ&amp;coverage=Grid&amp;name=$cell&amp;zoom=1&amp;size=$size" alt="Quad Map"><br><br>];

    }
    elsif ($quad) {
	$size = 350; 
	print qq[<img height=$size width=$size name="mainImage" src="http://atlas.ca.gov/cgi-bin/test/placeview?auth=DOQQ&amp;coverage=Grid&amp;name=$quad&amp;zoom=1&amp;size=$size"  alt="Quad Map" usemap="#$mapName"><br><br>];
	map_quad($quad,0,0,$size,$linkto,$mapName);
    }
    else {
	$size = 350;
	print qq[<img height=$size width=$size name="mappedImage" src="http://atlas.ca.gov/cgi-bin/test/spotquad" usemap="#cellpage"  alt="Quad Map" usemap="#degreeMap"><br><br>];
    }
}

sub display_ws {

    my $auth = shift;
    my $coverage = shift;
    my $idnum = shift;
    my $name = shift;
    my $linkto = shift;
    my $size = shift;

    $linkto = 'placefinder' if (!($linkto));

    my ($stath,$sql);

my $mode = 'sr';
my $size = 350 if (!($size));
my $emb = 0;

my ($imgwidth, $imgheight);
if ($size) {$imgwidth = size; $imgheight = size}
else {$size = 350; $imgwidth = 350; $imgheight = 350}

$mode = 'sr' if (!($mode));
my (@row,@geom,$up1,$up2,$up3,$up4);


$idnum = 0 if (!($idnum));
if (length($idnum) < 2) {
    $names = qq/SELECT 'California' from hr WHERE idnum  = '01'/;
}
elsif (length($idnum) eq 2) {
    $names = qq/SELECT (hrname || ' (hr)') from hr WHERE hr.idnum = '$idnum'/;
}
elsif (length($idnum) eq 5) {
    $up1 = substr($idnum,0,2);
    $names = qq/SELECT (huname || ' (hu)'), (hrname || ' (hr)') from hu,hr WHERE hr.idnum = '$up1' and hu.idnum  = '$idnum'/;
}
elsif (length($idnum) eq 7) {
    $up1 = substr($idnum,0,5);
    $up2 = substr($idnum,0,2);
    $names = qq/SELECT (haname || ' (ha)'), (huname || ' (hu)'), (hrname || ' (hr)') from ha,hu,hr WHERE hr.idnum = '$up2' and hu.idnum = '$up1' and ha.idnum  = '$idnum'/;
}
elsif (length($idnum) eq 8) {
    $up1 = substr($idnum,0,7);
    $up2 = substr($idnum,0,5);
    $up3 = substr($idnum,0,2);
    $names = qq/SELECT (hsaname || ' (hsa)'), (haname || ' (ha)'), (huname || ' (hu)'), (hrname || ' (hr)') from hsa,ha,hu,hr WHERE hr.idnum = '$up3' and hu.idnum = '$up2' and ha.idnum  = '$up1' and hsa.idnum = '$idnum'/;
}
elsif (length($idnum) eq 10) {
    $up1 = substr($idnum,0,8);
    $up2 = substr($idnum,0,7);
    $up3 = substr($idnum,0,5);
    $up4 = substr($idnum,0,2);
    $names = qq/SELECT (spwsname || ' (spws)'), (hsaname || ' (hsa)'), (haname || ' (ha)'), (huname || ' (hu)'),(hrname || ' (hr)') from ha,hu,hr,hsa,spws WHERE hr.idnum = '$up4' and hu.idnum = '$up3' and ha.idnum  = '$up2' and hsa.idnum = '$up1' and spws.idnum = '$idnum'/;
}
elsif (length($idnum) > 10) {
    $up1 = substr($idnum,0,10);
    $up2 = substr($idnum,0,8);
    $up3 = substr($idnum,0,7);
    $up4 = substr($idnum,0,5);
    $up5 = substr($idnum,0,2);
    $names = qq/SELECT (pwsname || ' (pws)'), (spwsname || ' (spws)'), (hsaname || ' (hsa)'), (haname || ' (ha)'), (huname || ' (hu)'),(hrname || ' (hr)') from ha,hu,hr,hsa,spws,pws WHERE hr.idnum = '$up5' and hu.idnum = '$up4' and ha.idnum  = '$up3' and hsa.idnum = '$up2' and  spws.idnum = '$up1' and pws.idnum = '$idnum'/;
}

$nq = $dbh->prepare($names);
$nq->execute();
@nameArray = $nq->fetchrow_array;
    $nq->finish;
$name = $nameArray[0];


print qq[<table width="100%">];
print qq[<tr><th width=140 class="blueLink">Location</th><th class="blueLink">$nameArray[0]</th></tr>];

print qq[<tr><td valign="top">];
print qq[<table width="100%"><tr><td valign="top">];

## examine the name trail to backup the tree to the last region that is not subdivided. 
@nameBase = @nameArray;

## first remove the level designation e.g. "name (hsa)"
for $i (0..((@nameBase) -1)) { $nameBase[$i] =~ s/\(.+\)// }

print qq[<tr><td align="center" class="content"><a href="#" onClick="navigate('$linkto','','$up5')"><img src="http://atlas.ca.gov/cgi-bin/test/spotshed?idnum=$up5&amp;zoom=idnum" alt="Watershed Map"><br>$nameArray[5]</a></td></tr>] if ($up5 > 0);

print qq[<tr><td align="center" class="content"><a href="#" onClick="navigate('$linkto','','$up4')"><img src="http://atlas.ca.gov/cgi-bin/test/spotshed?idnum=$up4&amp;zoom=idnum" alt="Watershed Map"><br>$nameArray[4]</a></td></tr>] if (($up4 >0) && ($nameBase[4] ne $nameBase[3]));

print qq[<tr><td align="center" class="content"><a href="#" onClick="navigate('$linkto','','$up3')"><img src="http://atlas.ca.gov/cgi-bin/test/spotshed?idnum=$up3&amp;zoom=idnum" alt="Watershed Map"><br>$nameArray[3]</a></td></tr>] if (($up3 > 0) && ($nameBase[3] ne $nameBase[2]));

print qq[<tr><td align="center" class="content"><a href="#" onClick="navigate('$linkto','','$up2')"><img src="http://atlas.ca.gov/cgi-bin/test/spotshed?idnum=$up2&amp;zoom=idnum" alt="Watershed Map"><br>$nameArray[2]</a></td></tr>] if (($up2 >0) && ($nameBase[2] ne $nameBase[1]));

print qq[<tr><td align="center" class="content"><a href="#" onClick="navigate('$linkto','','$up1')"><img src="http://atlas.ca.gov/cgi-bin/test/spotshed?idnum=$up1&amp;zoom=idnum" alt="Watershed Map"><br>$nameArray[1]</a></td></tr>] if (($up1 >0) && ($nameBase[1] ne $nameBase[0]));

print qq[<tr><td align="center" class="content"><a href="#" onClick="navigate('$linkto','','$up1')"><img src="http://atlas.ca.gov/cgi-bin/test/spotshed?idnum=$idnum&amp;zoom=idnum" alt="Watershed Map"><br>$nameArray[0]</a></td></tr>];

if ($mode eq 'drg') {
  print qq[</table></td><td width=$size valign="top" align="center"><table><tr><td align="center" class="content">];
  print qq[<img height=$size width=$size  name="mainImage" src="http://atlas.ca.gov/cgi-bin/test/showshed?idnum=$idnum&zoom=idnum&amp;width=$size&amp;height=$size&amp;mode=drg" alt="Watershed Map">];
}
elsif  ($mode eq 'ls'){
  print qq[</table></td><td width=$size valign="top" align="center"><table><tr><td align="center" class="content">];
  print qq[<img height=$size width=$size name="mainImage" src="http://atlas.ca.gov/cgi-bin/test/showshed?idnum=$idnum&amp;zoom=idnum&amp;width=$size&amp;height=$size&amp;mode=ls" alt="Watershed Map">];
}
else {
  print qq[</table></td><td width=$size valign="top" align="center"><table><tr><td align="center" class="content">];
  print qq[<img height=$size width=$size name="mainImage" src="http://atlas.ca.gov/cgi-bin/test/showshed?idnum=$idnum&amp;zoom=idnum&amp;width=$size&amp;height=$size&amp;mode=sr" usemap="#shedMap" alt="Watershed Map">];
}



print qq[<br>];

  print qq[<p>calwater22:$idnum];
  print qq[\n<!--lbl:$lbl-->];

print qq[</td>];
print qq[</tr>];

print qq[</td></tr>];
print qq[</tr></td></tr>];

print qq[</table>];
map_shed($idnum,$size) if (length($idnum) < 12);
print qq[<!--name:$nameArray[0]-->];
$nq->finish;

}

sub map_shed {

    my $idnum = shift;
    my $size = shift;
    my $linkto = shift;
    my ($name,$points,$coords,$st);

    $linkto = 'placefinder' if (!($linkto));

    $size = 350 if (!($size));

$idnum = 0 if (!($idnum));
if (length($idnum) < 2) {
    $sql = qq/SELECT idnum,hrname from hr order by hrname/;
    $ext_sql = qq/select xmax(extent(the_geom)), xmin(extent(the_geom)), ymax(extent(the_geom)), ymin(extent(the_geom)) FROM hr/;
}
elsif (length($idnum) eq 2) {
    $sql = qq/SELECT idnum,(huname || ' (hu)') from hu WHERE idnum  like '$idnum%' order by huname/;
#    $ext_sql = qq/select xmax(extent(the_geom)), xmin(extent(the_geom)), ymax(extent(the_geom)), ymin(extent(the_geom)) FROM hr WHERE idnum = '$idnum'/;
    $ext_sql = qq/select xmax(extent(the_geom)), xmin(extent(the_geom)), ymax(extent(the_geom)), ymin(extent(the_geom)) FROM hr WHERE idnum = '$idnum'/;
}
elsif (length($idnum) eq 5) {
    $sql = qq/SELECT idnum,(haname || ' (ha)') from ha WHERE idnum  like '$idnum%' order by haname/;
    $ext_sql = qq/select xmax(extent(the_geom)), xmin(extent(the_geom)), ymax(extent(the_geom)), ymin(extent(the_geom)) FROM hu WHERE idnum = '$idnum'/;
}
elsif (length($idnum) eq 7) {
    $sql = qq/SELECT idnum,(hsaname || ' (hsa)') from hsa WHERE idnum  like '$idnum%' order by hsaname/;
    $ext_sql = qq/select xmax(extent(the_geom)), xmin(extent(the_geom)), ymax(extent(the_geom)), ymin(extent(the_geom)) FROM ha WHERE idnum = '$idnum'/;
}
elsif (length($idnum) eq 8) {
    $sql = qq/SELECT idnum,(spwsname || ' (spws)') from spws WHERE idnum  like '$idnum%' order by spwsname/;
    $ext_sql = qq/select xmax(extent(the_geom)), xmin(extent(the_geom)), ymax(extent(the_geom)), ymin(extent(the_geom)) FROM hsa WHERE idnum = '$idnum'/;
}
elsif (length($idnum) eq 10) {
    $sql = qq/SELECT idnum,(pwsname || ' (pws)') from pws WHERE idnum  like '$idnum%' order by pwsname/;
    $ext_sql = qq/select xmax(extent(the_geom)), xmin(extent(the_geom)), ymax(extent(the_geom)), ymin(extent(the_geom)) FROM spws WHERE idnum = '$idnum'/;
}
elsif (length($idnum) > 10) {
    $sql = qq/SELECT idnum,(pwsname || ' (pws)') from pws WHERE idnum  like '$up1%' order by pwsname/;
    $ext_sql = qq/select xmax(extent(the_geom)), xmin(extent(the_geom)), ymax(extent(the_geom)), ymin(extent(the_geom)) FROM spws WHERE idnum = '$idnum'/;
}

    print qq[<MAP NAME="shedMap">];

    $st = $dbh->prepare($ext_sql);
    $st->execute();

    @ext = $st->fetchrow_array();
    $st->finish;

    $maxx = $ext[0];
    $minx = $ext[1];
    $maxy = $ext[2];
    $miny = $ext[3];
    
   # remove pesky quotes added to neg numbers 
    $maxx =~ s/\'//g;
    $minx =~ s/\'//g;
    $maxy =~ s/\'//g;
    $miny =~ s/\'//g;

    $st = $dbh->prepare($sql);
    $st->execute();
    $dat = $st->fetchall_arrayref();    
    $st->finish;

    foreach $row (@$dat) {
	$idnum = $row->[0];
	$name = $row->[1];
	$points = getPolygon('calwater22:' . $idnum,3309);
	$points =~ s/[\(|\)|MULTIPOLYGON]//g;
	$points =~ s/\s/,/g;
	@pt = split(',',$points);
        $coords = create_imagemap($maxx,$maxy,$minx,$miny,$size);
	if ($linkto eq 'calw_tree') {
	    $area = qq[<area shape=POLY title="$name" coords="$coords" href="$linkto?idnum=$idnum&name=$lbl&mode=$mode">\n]}
	else {
	    $area = qq[<area shape=POLY title="$name" coords="$coords" href="$linkto?auth=FRAP&coverage=calwater22&name=$idnum&lbl=$name" alt="$name">\n]}

	print "$area";
    }

    print qq[</MAP>];
}

sub transformBBox {
    my $eastbc = shift;
    my $westbc = shift;
    my $northbc = shift;
    my $southbc = shift;
    my $fromSRID = shift; 
    my $toSRID = shift; 
    
    my $sql = qq/select ST_Box2d(srid2srid($$westbc,$$southbc,$$eastbc,$$northbc,$fromSRID,$toSRID))/;
    $st = $dbh->prepare($sql);
    $st->execute();
    my @row = $st->fetchrow_array();
    $r = $row[0];
    $r =~ s/^BOX\(//i;
    $r =~ s/\)$//;
    $r =~ s/,/ /g;
    my @v = split(/ /,$r);
    $$westbc = $v[0];
    $$southbc = $v[1];
    $$eastbc = $v[2];
    $$northbc = $v[3];
    return;
}

sub getExtent {

    my $clip = shift;
    my $srid = shift;
    my $ptBuffer = shift;

    my ($coverage,$name) = split /:/,$clip;
    $ptBuffer = 800 if (!($ptBuffer));
    $ptBuffer = $ptBuffer/100000 if ($srid eq '4269');

    my ($sql,$st);

    my %view;
    $view{counties} = 'county_view';
    $view{bioregions} = 'bioregion_view';
    $view{grid} = 'grid_view';
    $view{urban_areas} = 'urbanarea_view';
    $view{zip_codes} = 'zip_view';
    $view{geoname} = 'geoname';
    $view{gnis} = 'calgnis';
    $view{city2k} = 'city2k';
    $view{asmplan4} = 'asmplan4';
    $view{sen_law} = 'sen_law';
    $view{cng_law} = 'cng_law';
    $view{school_dist_u} = 'school_dist_u';
    $view{hydrogp} = 'hydrogp';

    if (length($name) eq 2) {
	$view{calwater22} = 'hr'
	}
    elsif (length($name) eq 5) {
	$view{calwater22} = 'hu'
	}
    elsif (length($name) eq 7) {
	$view{calwater22} = 'ha'
	}
    elsif (length($name) eq 8) {
	$view{calwater22} = 'hsa'
	}
    elsif (length($name) eq 10) {
	$view{calwater22} = 'spws'
	}
    elsif (length($name) eq 12) {
	$view{calwater22} = 'pws'
	}

    if ($coverage =~  /counties|bioregions|urban_areas|zip_codes|grid/) {
	$sql = qq/SELECT xmax(extent(transform(footprint,$srid))), xmin(extent(transform(footprint,$srid))), ymax(extent(transform(footprint,$srid))), ymin(extent(transform(footprint,$srid))) FROM $view{$coverage} WHERE  upper(description) like upper('$name')/;
    }
    elsif ($coverage eq 'calwater22') {
#	$name = substr($name,0,5);
	$sql = qq/SELECT xmax(extent(transform(the_geom,$srid))), xmin(extent(transform(the_geom,$srid))), ymax(extent(transform(the_geom,$srid))), ymin(extent(transform(the_geom,$srid))) FROM $view{calwater22} WHERE idnum like '$name%'/;
    }
       elsif ($coverage eq 'city2k') {
	$sql = qq/SELECT xmax(extent(transform(the_geom,$srid))), xmin(extent(transform(the_geom,$srid))), ymax(extent(transform(the_geom,$srid))), ymin(extent(transform(the_geom,$srid))) FROM $view{city2k} WHERE upper(name) like upper('$name')/;
    }
       elsif ($coverage eq 'hydrogp') {
	$sql = qq/SELECT xmax(extent(transform(the_geom,$srid))), xmin(extent(transform(the_geom,$srid))), ymax(extent(transform(the_geom,$srid))), ymin(extent(transform(the_geom,$srid))) FROM $view{hydrogp} WHERE upper(name) like upper('$name')/;
    }
    elsif ($coverage eq 'school_dist_u') {
	$sql = qq/SELECT xmax(extent(transform(the_geom,$srid))), xmin(extent(transform(the_geom,$srid))), ymax(extent(transform(the_geom,$srid))), ymin(extent(transform(the_geom,$srid))) FROM $view{school_dist_u} WHERE upper(first_name) like upper('$name')/;
    }
    elsif ($coverage eq 'sen_law') {
	$sql = qq/SELECT xmax(extent(transform(the_geom,$srid))), xmin(extent(transform(the_geom,$srid))), ymax(extent(transform(the_geom,$srid))), ymin(extent(transform(the_geom,$srid))) FROM $view{sen_law} WHERE upper(district) like upper('$name')/;
    }
    elsif ($coverage eq 'cng_law') {
	$sql = qq/SELECT xmax(extent(transform(the_geom,$srid))), xmin(extent(transform(the_geom,$srid))), ymax(extent(transform(the_geom,$srid))), ymin(extent(transform(the_geom,$srid))) FROM $view{cng_law} WHERE upper(district) like upper('$name')/;
    }
    elsif ($coverage eq 'asmplan4') {
	$sql = qq/SELECT xmax(extent(transform(the_geom,$srid))), xmin(extent(transform(the_geom,$srid))), ymax(extent(transform(the_geom,$srid))), ymin(extent(transform(the_geom,$srid))) FROM $view{asmplan4} WHERE id = $name/;
    }
    elsif ($coverage eq 'geoname') {
	$sql = qq/SELECT xmax(extent(transform(the_geom,$srid))) + $ptBuffer, xmin(extent(transform(the_geom,$srid))) - $ptBuffer, ymax(extent(transform(the_geom,$srid))) + $ptBuffer, ymin(extent(transform(the_geom,$srid))) - $ptBuffer FROM geonamea WHERE gid = $name/;
    }
    elsif ($coverage =~ /gnis|calgnis/) {
#	$sql = qq/SELECT xmax(extent(transform(geom,$srid))) + $ptBuffer, xmin(extent(transform(geom,$srid))) - $ptBuffer, ymax(extent(transform(geom,$srid))) + $ptBuffer, ymin(extent(transform(geom,$srid))) - $ptBuffer FROM calgnis WHERE feature_id = $name/;
	$sql = qq/SELECT xmax(extent(transform(the_geom,$srid))) + $ptBuffer, xmin(extent(transform(the_geom,$srid))) - $ptBuffer, ymax(extent(transform(the_geom,$srid))) + $ptBuffer, ymin(extent(transform(the_geom,$srid))) - $ptBuffer FROM calgeonames2009 WHERE feature_id = $name/;
    }
     elsif ($coverage eq 'ca_outline') {
	$sql = qq/SELECT xmax(extent(transform(simplify(footprint,.05),$srid))), xmin(extent(transform(simplify(footprint,.05),$srid))), ymax(extent(transform(simplify(footprint,.05),$srid))), ymin(extent(transform(simplify(footprint,.05),$srid))) FROM ca_outline_view/;
    }
    
    $st = $dbh->prepare($sql);
    $st->execute();
    @ext = $st->fetchrow_array();
    $st->finish;
    
    return \@ext;

}

sub getPolygon {

    my $clip = shift;
    my $srid = shift;
    my $area = shift;
    $dbHandle = shift;

    my ($coverage,$name) = split(/:/,$clip);
    my ($sql,$poly);

    if ($clip){
	if ($clip =~ /urban_areas/i) {
	    $sql = qq/select AsText(MemGeomUnion(simplify(transform(footprint,$srid),200))) from urbanarea_view where upper(trim(description)) = upper('$name')/;
	}
	elsif ($clip =~ /bioregions/i) {
	    $sql = qq/select AsText(transform(simplify(footprint,.1),$srid)) from bioregion_view where upper(trim(description)) = upper('$name')/;
	}
	elsif ($clip =~ /geoname/i && (!($name =~ /\D/))) {
	    $sql = qq/select AsText(envelope(expand(box3d(envelope(transform(the_geom,$srid))),0.002))) from geonamea where gid = $name/;
#	    $sql = qq/select AsText(envelope(expand(box3d(envelope(transform(the_geom,$srid))),0.0002))) from geonamea where gid = $name/;
	}
	elsif ($clip =~ /gnis/i && (!($name =~ /\D/))) {
	    $sql = qq/select AsText(envelope(expand(box3d(envelope(transform(the_geom,$srid))),0.002))) from calgeonames2009 where feature_id = $name/;
	}
	elsif ($clip =~ /calwater22/i) {
#	elsif ($clip =~ /calwater22/i && (!($name =~ /\D/))) {
	    if (length($name) eq 2) {
		$sql = qq/select AsText(transform(simplify(the_geom,1500),$srid)) from hr where idnum = '$name'/;
	    }
	    elsif (length($name) eq 5) {
                $sql = qq/select AsText(transform(simplify(the_geom,800),$srid)) from hu where idnum = '$name'/;
            }
	    elsif (length($name) eq 7) {
                $sql = qq/select AsText(transform(simplify(the_geom,200),$srid)) from ha where idnum = '$name'/;
            }
	    elsif (length($name) eq 8) {
                $sql = qq/select AsText(transform(simplify(the_geom,50),$srid)) from hsa where idnum = '$name'/;
            }
	    elsif (length($name) eq 10) {
                $sql = qq/select AsText(transform(simplify(the_geom,15),$srid)) from spws where idnum = '$name'/;
            }
	    elsif (length($name) gt 10) {
                $sql = qq/select AsText(transform(simplify(the_geom,15),$srid)) from pws where idnum = '$name'/;
            }

	}
	elsif ($clip =~ /counties/i) {
#	    $sql = qq/select AsText(MemGeomUnion(transform(simplify(transform(footprint, 2249), 1000),$srid))) from gazetteer where coverage = 'county' and upper(trim(description)) = upper('$name')/;

	    $sql = qq/select AsText(MemGeomUnion(footprint)) from gazetteer2 where coverage = 'cnty24k97' and upper(trim(name)) = upper('$name')/;
#	    $sql = qq/select AsText(MemGeomUnion(transform(simplify(footprint,.001),$srid))) from county_view where upper(trim(description)) = upper('$name')/;
	}
	elsif ($clip =~ /zip_codes/i) {
	    $sql = qq/select AsText(MemGeomUnion(transform(simplify(footprint,.001),$srid))) from zip_view where upper(trim(description)) = upper('$name')/;
	}
	elsif ($clip =~ /city2k/i) {
	    $sql = qq/select AsText(MemGeomUnion(transform(simplify(the_geom,15),$srid))) from city2k where upper(trim(name)) = upper('$name')/;
	}
	elsif ($clip =~ /hydrogp/i) {
	    $sql = qq/select AsText(MemGeomUnion(transform(simplify(the_geom,50),$srid))) from hydrogp where upper(trim(name)) = upper('$name')/;
	}
	elsif ($clip =~ /asmplan4/i) {
	    $sql = qq/select AsText(MemGeomUnion(transform(simplify(the_geom,.01),$srid))) from asmplan4 where id = $name/;
	}
	elsif ($clip =~ /sen_law/i) {
	    $sql = qq/select AsText(MemGeomUnion(transform(simplify(the_geom,.01),$srid))) from sen_law where upper(trim(district)) = upper('$name')/;
	}
	elsif ($clip =~ /cng_law/i) {
	    $sql = qq/select AsText(MemGeomUnion(transform(simplify(the_geom,.01),$srid))) from cng_law where upper(trim(district)) = upper('$name')/;
	}
	elsif ($clip =~ /school_dist_u/i) {
	    $sql = qq/select AsText(MemGeomUnion(transform(simplify(the_geom,.005),$srid))) from school_dist_u where upper(trim(first_name)) = upper('$name')/;
	}
	elsif ($clip =~ /grid/i) {
	    $sql = qq/select AsText(transform(footprint,$srid)) = upper('$name')/;
	}
	elsif (($clip =~ /ca_outline/i) || (uc($clip) eq 'CALIFORNIA' )) {
	    $sql = qq/select AsText(transform(simplify(footprint,.005),$srid)) from ca_outline_view/;
	}
	else { 
	    return 0;
	}

	if ($dbHandle) {
	    $st = $dbHandle->prepare($sql) || return 0;
	}
	else {
	    $st = $dbh->prepare($sql) || return 0;
	}

	$st->execute() || return 0;
        return 0 if (!(@row = $st->fetchrow_array)); 
        $st->finish;
	$poly = $row[0];
	## sorry, area causes prob
	##$$area = $row[1];
	$$area = 0;
    }

    return $poly
}

sub getExtPolygon {

    my $ext = shift;
    my $srid = shift;
    $area = shift;
    my $inPrj = shift;

    $inPrj = 4269 if (!($inPrj));
    my ($sql,$poly);

    ## format the ext values for the BOX3D syntax
    @extArray = split(/,/,$ext);

    $sql = qq/select AsText(transform(envelope(SetSRID('BOX3D($extArray[1] $extArray[0],$extArray[3] $extArray[2])'::BOX3D,$inPrj)),$srid)), Area(envelope(transform(SetSRID('BOX3D($extArray[0] $extArray[1],$extArray[2] $extArray[3])'::box3d,$inPrj),$srid)))/;
#    $sql = qq/select AsText(transform(envelope(SetSRID('BOX3D($extArray[0] $extArray[2],$extArray[1] $extArray[3])'::BOX3D,$inPrj)),$srid)), Area(envelope(transform(SetSRID('BOX3D($extArray[0] $extArray[2],$extArray[1] $extArray[3])'::box3d,$inPrj),$srid)))/;
    $st = $dbh->prepare($sql);
    $st->execute();
    @row = $st->fetchrow_array; 
    $st->finish;
    $poly = $row[0];
    $$area = $row[1];

    return $poly
}

sub getCentroid {

    my $clip = shift;
    my $srid = shift;

    $srid = 4249 if (!($srid));

    my ($sql,$lat_long);
    my ($coverage,$name) = split /:/,$clip;

	if ($coverage eq 'urban_areas') {
	    $sql = qq/select astext(centroid(MemGeomUnion(footprint))) from urbanarea_view where upper(trim(description)) = upper('$name')/;
	}
	elsif ($coverage eq 'bioregions') {
	    $sql = qq/select astext(centroid(extent(footprint))) from bioregion_view where upper(trim(description)) = upper('$name')/;
	}
	elsif ($coverage =~ /gnis/) {
	    $sql = qq/select astext(centroid(extent(transform(the_geom,4269)))) from calgeonames2009 where feature_id = $name/;
	}
	elsif ($coverage eq 'calwater22') {
	    if (length($name) eq 2) {
		$sql = qq/select astext(centroid(extent(transform(the_geom,4269)))) from hr where idnum = '$name'/;
	    }
	    elsif (length($name) eq 5) {
                $sql = qq/select astext(centroid(extent(transform(the_geom,4269)))) from hu where idnum = '$name'/;
            }
	    elsif (length($name) eq 7) {
                $sql = qq/select astext(centroid(extent(transform(the_geom,4269)))) from ha where idnum = '$name'/;
            }
	    elsif (length($name) eq 8) {
                $sql = qq/select astext(centroid(extent(transform(the_geom,4269)))) from hsa where idnum = '$name'/;
            }
	    elsif (length($name) eq 10) {
                $sql = qq/select astext(centroid(extent(transform(the_geom,4269)))) from spws where idnum = '$name'/;
            }
	    elsif (length($name) gt 10) {
                $sql = qq/select astext(centroid(extent(transform(the_geom,4269)))) from pws where idnum = '$name'/;
            }

	}
	elsif ($coverage eq 'counties') {
	    $sql = qq/select astext(centroid(extent(footprint))) from county_view where upper(trim(description)) = upper('$name')/;
	}
	elsif ($coverage eq 'zip_codes') {
	    $sql = qq/select astext(centroid(extent(footprint))) from zip_view where upper(trim(description)) = upper('$name')/;
	}
	elsif ($coverage eq 'city2k') {
	    $sql = qq/select astext(centroid(extent(transform(the_geom,4269)))) from city2k where upper(trim(name)) = upper('$name')/;
	}
	elsif ($coverage eq 'hydrogp') {
	    $sql = qq/select astext(centroid(extent(transform(the_geom,4269)))) from hydrogp where upper(trim(name)) = upper('$name')/;
	}
	elsif ($coverage eq 'school_dist_u') {
	    $sql = qq/select astext(centroid(extent(transform(the_geom,4269)))) from school_dist_u where upper(trim(first_name)) = upper('$name')/;
	}
	elsif ($coverage eq 'asmplan4') {
	    $sql = qq/select astext(centroid(extent(transform(the_geom,4269)))) from asmplan4 where id = $name/;
	}
	elsif ($coverage eq 'sen_law') {
	    $sql = qq/select astext(centroid(extent(transform(the_geom,4269)))) from sen_law where district = '$name'/;
	}
	elsif ($coverage eq 'cng_law') {
	    $sql = qq/select astext(centroid(extent(transform(the_geom,4269)))) from cng_law where district = '$name'/;
	}
	elsif (lc($coverage) eq 'grid') {
	    $sql = qq/select astext(centroid(extent(footprint))) from grid_view where upper(trim(description)) = upper('$name')/;
	}
	elsif (lc($coverage) eq 'ca_outline') {
	    $sql = qq/select astext(centroid(extent(footprint))) from ca_outline_view/;
	}
	else  {
	    $sql = qq/select astext(centroid(extent(footprint))) from ca_outline_view/;
	}

##    print "\n$sql\n";

	$st = $dbh->prepare($sql);
	$st->execute();
        @row = $st->fetchrow_array; 
        $st->finish;
	$lat_long = $row[0];
    $lat_long =~ s/^POINT\(//i;
	$lat_long =~ s/\)$//;
	@ll = split(/ /,$lat_long);
			  return \@ll;
}

sub gaz_intersect {

    my $clip = shift;
    my $lbl = shift;

    my ($sql,$poly);
    my (@views,@matchList);
    my $area;

    $lbl = $name if (!($lbl));
    $poly = getPolygon($clip,3310,\$area);

    $sql = qq[select coverage, description from gazetteer where (transform(simplify(footprint,.2),3310) && GeometryFromText('$poly',3310)) and (coverage IN ('zip_codes','urban_areas','counties')) and intersects(transform(footprint,3310),GeometryFromText('$poly',3310)) and area(difference(transform(footprint,3310),GeometryFromText('$poly',3310))) < (area(GeometryFromText('$poly',3310)) * .8) and description <> '' order by coverage];
#    $sql = qq[select coverage, description from gazetteer where (simplify(footprint,.2) && GeometryFromText('$poly',4269)) and (coverage IN ('zip_codes','urban_areas','calwater22','counties')) and intersects(footprint,GeometryFromText('$poly',4269)) and description <> '' order by coverage];

    my $nq = $dbh->prepare($sql);
    $nq->execute();

    my ($shpname,$coverage);
    while (@row = $nq->fetchrow_array) {
      $coverage = $row[0];
      $shpname = $row[1];
      push(@matchList,"$coverage:$shpname");
   }

    return \@matchList;
}

sub ext_intersect {

    my $toShp = shift;
    
    my ($sql,$poly);
    my (@views,@matchList);
    my $area;

    $lbl = $name if (!($lbl));

    $poly = '';

    if ($toShp) {
	# only map to the requested shp type.
	@views = ($toShp)
    }
    elsif ($area lt .04){
	@views = ('bioregion_view','county_view','urbanarea_view','hu_view','grid_view','zip_view','geoname')
	}
    else {
	@views = ('bioregion_view','county_view','urbanarea_view','hu_view','grid_view')
	}

    print qq[<table border=0 width="100%"><tr>] if ($printing);
    foreach $view (@views) {
	if ($view eq 'bioregion_view') {
	    $to_auth = 'FRAP';
	    $to_coverage = 'bioregions';
	    $to_lbl = 'California Bioregions';
	}
	elsif ($view eq 'urbanarea_view') {
	    $to_auth = 'FRAP';
	    $to_coverage = 'urban_areas';
	    $to_lbl = 'Urban Areas';	    
	}
	elsif ($view eq 'county_view') {
	    $to_auth = 'CMCC';
	    $to_coverage = 'counties';
	    $to_lbl = 'California Counties';
	}
	elsif ($view eq 'grid_view') {
	    $to_auth = 'DOQQ';
	    $to_coverage = 'Grid';
	    $to_lbl = 'Degree Grid/USGS 7.5 Minute Quads';
	}
	elsif ($view eq 'hu_view') {
	    $to_auth = 'FRAP';
	    $to_coverage = 'calwater22';
	    $to_lbl = 'Watersheds (CalWater Hydrologic Units)';
	}
	elsif ($view eq 'zip_view') {
	    $to_auth = 'Census';
	    $to_coverage = 'zip_codes';
	    $to_lbl = 'Zip Codes';
	}
	elsif ($view eq 'geoname') {
	    $to_auth = 'GNIS';
	    $to_coverage = 'geoname';
	    $to_lbl = 'Other Places (GNIS)';
	}
	elsif ($view eq 'spatialdomain') {
	    $to_auth = 'CERES';
	    $to_coverage = 'spatialdomain';
	    $to_lbl = 'CEIC Defined Regions';
	}

	print qq[<tr><th class="blueLink">$to_lbl</th></tr><tr><td align="left">]  if ($printing);

	if ($view eq 'grid_view') {
	    if ($area lt .2) { 
 		$res = 7;
		$sql = qq/SELECT 'x', 'o' || description FROM $view WHERE length(trim(description)) = $res and (footprint && GeometryFromText('$poly',4269)) and intersects(footprint,GeometryFromText('$poly',4269)) order by description/;
		$nq = $dbh->prepare($sql);
		$nq->execute();
		while (@row = $nq->fetchrow_array) { push(@ocode,$row[1]) }
		$ocodeStr = join("','",@ocode);
		$sql = qq/SELECT substr(ocode,2,7),mapname || ' (' || substr(ocode,2,7) || ')' FROM mapgrida WHERE ocode IN ('$ocodeStr') order by mapname/;
		$nq = $dbh->prepare($sql);
		$nq->execute();
	    }
	    else { 
 		$res = 5;
		$sql = qq/SELECT distinct description,description FROM $view WHERE length(trim(description)) = $res and (footprint && GeometryFromText('$poly',4269)) and intersects(footprint,GeometryFromText('$poly',4269)) order by description/;
		$nq = $dbh->prepare($sql);
		$nq->execute();
	    }
	}
	elsif ($view eq 'hu_view') {
	    if ($area < .1 || ($toShp)){ 
		$sql = qq/SELECT distinct idnum,pwsname FROM pws WHERE (the_geom && transform(GeometryFromText('$poly',4269),3309)) and intersects(the_geom,transform(GeometryFromText('$poly',4269),3309)) order by pwsname/;
	    }
	    if ($area < .2 || ($toShp)){ 
		$sql = qq/SELECT distinct idnum,haname FROM ha WHERE (the_geom && transform(GeometryFromText('$poly',4269),3309)) and intersects(the_geom,transform(GeometryFromText('$poly',4269),3309)) order by haname/;
	    }
	    elsif ($area < .3 || ($toShp)){ 
		$sql = qq/SELECT distinct idnum,huname FROM hu WHERE (the_geom && transform(GeometryFromText('$poly',4269),3309)) and intersects(the_geom,transform(GeometryFromText('$poly',4269),3309)) order by huname/;
	    }
	    else {
		$sql = qq/SELECT distinct idnum,hrname FROM hr WHERE (the_geom && transform(GeometryFromText('$poly',4269),3309)) and intersects(the_geom,transform(GeometryFromText('$poly',4269),3309)) order by hrname/;
	    }
	    $nq = $dbh->prepare($sql);
	    $nq->execute();
	}
	elsif ($view eq 'urbanarea_view') {
	    $sql = qq/SELECT distinct description,description FROM $view WHERE (footprint && GeometryFromText('$poly',4269)) and intersects(footprint,GeometryFromText('$poly',4269)) and upper(description) not like '%(%)' order by description/;
	    $nq = $dbh->prepare($sql);
	    $nq->execute();
	}
	elsif ($view eq 'geoname') {
#	    $sql = qq/SELECT gid,name || ' (' || type || ')'  FROM geonamea WHERE type not IN ('church','school','building','po','hospital','tower') and (the_geom && transform(GeometryFromText('$poly',4269),3309)) and intersects(the_geom,transform(GeometryFromText('$poly',4269),3309)) order by type,name/;
	    $sql = qq/SELECT gid,name || ' (' || type || ')'  FROM geonamea WHERE (the_geom && transform(GeometryFromText('$poly',4269),3309)) and intersects(the_geom,transform(GeometryFromText('$poly',4269),3309)) order by type,name/;
	    $nq = $dbh->prepare($sql);
	    $nq->execute();
	}
	elsif ($view eq 'spatialdomain') {
	    $sql = qq/SELECT trim(name),trim(name),abs(area(the_geom) - area(transform(GeometryFromText('$poly',4269),3309))) as fit FROM spatialdomain WHERE (the_geom && transform(GeometryFromText('$poly',4269),3309)) and intersects(the_geom,transform(GeometryFromText('$poly',4269),3309)) and (area(the_geom) < 90000000000) order by fit/;
	    $nq = $dbh3->prepare($sql);
	    $nq->execute();
	}
	else {
#	    $sql = qq/SELECT distinct description,description FROM $view WHERE (footprint && GeometryFromText('$poly',4269)) and intersects(footprint,GeometryFromText('$poly',4269)) and (area(difference(footprint,GeometryFromText('$poly',4269))) < 1) order by description/;
	    $sql = qq/SELECT distinct description,description FROM $view WHERE (footprint && GeometryFromText('$poly',4269)) and intersects(footprint,GeometryFromText('$poly',4269)) order by description/;
	    $nq = $dbh->prepare($sql);
	    $nq->execute();
	}


    my ($shpname,$shpname_);
    while (@row = $nq->fetchrow_array) {
      $shpname = $row[0];
      $lbl = $row[1];
      $shpname_ = $shpname;
      $shpname_ =~ s/\s/_/g;
#      print qq[<a class="blueLink" href="$linkto?auth=$to_auth&amp;coverage=$to_coverage&amp;name=$shpname&amp;lbl=$lbl&amp;size=350&amp;ptBuffer=800">$lbl</a><br>] if ($printing);
      print qq[<a class="blueLink" href="$linkto?auth=$to_auth&amp;coverage=$to_coverage&amp;name=$shpname&amp;lbl=$lbl&amp;ptBuffer=800">$lbl</a><br>] if ($printing);

      push(@matchList,"$to_coverage:$shpname"
);
   }
    print qq[</td></tr>] if ($printing);
}
    print qq[</table>] if ($printing);

    return \@matchList;
}

sub shp_intersect {


    my $clip = shift;
    my $lbl = shift;
    my $linkto = shift;
    my $toShp = shift;
    my $mode = shift;
    
    $linkto = 'placefinder' if (!($linkto));
    # if called with a toShp only return a list otherwise print the results
    my $printing = 1 unless ($toShp || ($mode eq 'quiet') );
    my ($sql,$poly);
    my (@views,@matchList);
    my $area;

    $lbl = $name if (!($lbl));

    $poly = getPolygon($clip,4269,\$area);

    if ($toShp) {
	# only map to the requested shp type.
	@views = ($toShp)
    }
    @views = ('county_view','city2k','zip_view','school_dist_u');

    print qq[<table border=0 width="120"><tr>] if ($printing);
    foreach $view (@views) {
	if ($view eq 'bioregion_view') {
	    $to_auth = 'FRAP';
	    $to_coverage = 'bioregions';
	    $to_lbl = 'California Bioregions';
	}
	elsif ($view eq 'urbanarea_view') {
	    $to_auth = 'FRAP';
	    $to_coverage = 'urban_areas';
	    $to_lbl = 'Urban Area';	    
	}
	elsif ($view eq 'city2k') {
	    $to_auth = 'Census';
	    $to_coverage = 'city2k';
	    $to_lbl = 'Populated Places';	    
	}
	elsif ($view eq 'school_dist_u') {
	    $to_auth = 'Census';
	    $to_coverage = 'school_dist_u';
	    $to_lbl = 'School Districts';	    
	}
	elsif ($view eq 'county_view') {
	    $to_auth = 'CMCC';
	    $to_coverage = 'counties';
	    $to_lbl = 'County';
	}
	elsif ($view eq 'grid_view') {
	    $to_auth = 'DOQQ';
	    $to_coverage = 'Grid';
	    $to_lbl = 'Degree Grid/USGS 7.5 Minute Quads';
	}
	elsif ($view eq 'hu_view') {
	    $to_auth = 'FRAP';
	    $to_coverage = 'calwater22';
	    $to_lbl = 'Watersheds (CalWater Hydrologic Units)';
	}
	elsif ($view eq 'zip_view') {
	    $to_auth = 'Census';
	    $to_coverage = 'zip_codes';
	    $to_lbl = 'Zip Codes';
	}
	elsif ($view eq 'geoname') {
	    $to_auth = 'GNIS';
	    $to_coverage = 'geoname';
	    $to_lbl = 'Other Places (GNIS)';
	}

	print qq[<tr><td class="blueLink" align="left">$to_lbl:<br>]  if ($printing);

       if ($view eq 'city2k') {
	    $sql = qq/SELECT distinct name,label FROM city2k WHERE (the_geom && transform(GeometryFromText('$poly',4269),3310)) and intersects(the_geom,transform(GeometryFromText('$poly',4269),3310)) and upper(name) not like '%(%)' order by name/;
	    $nq = $dbh->prepare($sql);
	    $nq->execute();
	}
       elsif ($view eq 'school_dist_u') {
	    $sql = qq/SELECT distinct first_name,first_name FROM school_dist_u WHERE (the_geom && transform(GeometryFromText('$poly',4269),3309)) and intersects(the_geom,transform(GeometryFromText('$poly',4269),3309)) order by first_name/;
	    $nq = $dbh->prepare($sql);
	    $nq->execute();
	}
	elsif ($view eq 'geoname') {
	    $sql = qq/SELECT gid,name || ' (' || type || ')'  FROM geonamea WHERE (the_geom && transform(GeometryFromText('$poly',4269),3309)) and intersects(the_geom,transform(GeometryFromText('$poly',4269),3309)) order by type,name/;
	    $nq = $dbh->prepare($sql);
	    $nq->execute();
	}
	elsif ($view eq 'zip_view') {
	    $sql = qq/SELECT distinct description,description FROM zip_view WHERE description NOT LIKE '%XX' and description NOT LIKE '%HH' and (footprint && GeometryFromText('$poly',4269)) and intersects(footprint,GeometryFromText('$poly',4269)) order by description/;
	    $nq = $dbh->prepare($sql);
	    $nq->execute();
	}
	else {
	    $sql = qq/SELECT distinct description,description FROM $view WHERE (footprint && GeometryFromText('$poly',4269)) and intersects(footprint,GeometryFromText('$poly',4269)) order by description/;
	    $nq = $dbh->prepare($sql);
	    $nq->execute();
	}


    my ($shpname,$shpname_);
    while (@row = $nq->fetchrow_array) {
      $shpname = $row[0];
      $lbl = $row[1];
      $shpname_ = $shpname;
      $shpname_ =~ s/\s/_/g;

      print qq[<a href="$linkto?clip=$to_coverage:$shpname&amp;lbl=$lbl&amp;ptBuffer=800">$lbl</a><br>] if ($printing);

      push(@matchList,"$to_coverage:$shpname"
);
   }
    print qq[</td></tr>] if ($printing);
}
    print qq[</table>] if ($printing);

    return \@matchList;
}

sub related {

    my $clip = shift;
    my $lbl = shift;
    my $toShp = shift;
    my $mode = shift;
    my $ext = shift;
    
    my ($sql,$poly);
    my (@views,@terms,@types);
    my $area;
    my %results = (); # results in a hash of arrays


    $lbl = $name if (!($lbl));

    if ($ext) {
	$poly = getExtPolygon($ext,4269,\$area);
    }
    else {
#        $ext = join(", ",@{getExtent($clip,4269)});
	$poly = getPolygon($clip,4269,\$area);
    }

    if ($toShp) {
	# only map to the requested shp type.
	@views = ($toShp)
    }
    else {
#	@views = ('county_view','city2k','asmplan4','sen_law','cng_law');
	@views = ('bioregion_view','county_view','city2k','asmplan4','sen_law','cng_law');
        if ($area < 1){
	    push(@views,'zip_view');
	    push(@views,'hu_view');
	    push(@views,'hydrogp');
#	    push(@views,'school_dist_u');
	}
        if ($area < .02){
#        if (0){
	    push(@views,'gnis');
	    push(@views,'calgnis');
	}
    }

    foreach $view (@views) {
	if ($view eq 'city2k') {
	    $to_auth = 'Census';
	    $to_coverage = 'city2k';
	    $to_lbl = 'Populated Places';	    
	}
	elsif ($view eq 'school_dist_u') {
	    $to_auth = 'Census';
	    $to_coverage = 'school_dist_u';
	    $to_lbl = 'School Districts';	    
	}
	elsif ($view eq 'county_view') {
	    $to_auth = 'CMCC';
	    $to_coverage = 'counties';
	    $to_lbl = 'County';
	}
	elsif ($view eq 'bioregion_view') {
	    $to_auth = 'CMCC';
	    $to_coverage = 'bioregions';
	    $to_lbl = 'Bioregions';
	}
	elsif ($view eq 'grid_view') {
	    $to_auth = 'DOQQ';
	    $to_coverage = 'Grid';
	    $to_lbl = 'USGS 7.5 Minute Quads';
	}
	elsif ($view eq 'hu_view') {
	    $to_auth = 'FRAP';
	    $to_coverage = 'calwater22';
#	    $to_lbl = 'Watersheds (CalWater Hydrologic Units)';
	    $to_lbl = 'Watersheds';
	}
	elsif ($view eq 'zip_view') {
	    $to_auth = 'Census';
	    $to_coverage = 'zip_codes';
	    $to_lbl = 'Zip Codes';
	}
	elsif ($view eq 'gnis') {
	    $to_auth = 'GNIS';
	    $to_coverage = 'calgnis';
	    $to_lbl = 'Other Places';
	}
	elsif ($view eq 'calgnis') {
	    $to_auth = 'GNIS';
	    $to_coverage = 'calgnis';
	    $to_lbl = 'Other Places';
	}
	elsif ($view eq 'asmplan4') {
	    $to_auth = 'CMCC';
	    $to_coverage = 'asmplan4';
	    $to_lbl = 'Assembly Districts';
	}
	elsif ($view eq 'sen_law') {
	    $to_auth = 'CMCC';
	    $to_coverage = 'sen_law';
	    $to_lbl = 'Senate Districts';
	}
	elsif ($view eq 'cng_law') {
	    $to_auth = 'CMCC';
	    $to_coverage = 'cng_law';
	    $to_lbl = 'Congressional Districts';
	}
	elsif ($view eq 'hydrogp') {
	    $to_auth = 'CMCC';
	    $to_coverage = 'hydrogp';
	    $to_lbl = 'Water Bodies';
	}

       if ($view eq 'city2k') {
	    $sql = qq/SELECT name,label,'Census2000 Urban Areas' FROM city2k WHERE (the_geom && transform(GeometryFromText('$poly',4269),3310)) and intersects(the_geom,transform(GeometryFromText('$poly',4269),3310)) and upper(name) not like '%(%)' order by population DESC LIMIT 40/;
	    $nq = $dbh->prepare($sql);
	    $nq->execute();
	}
       elsif ($view eq 'hu_view') {
	    $sql = qq/SELECT idnum,huname,'CalWater Hydrologic Units' FROM hu WHERE (the_geom && transform(GeometryFromText('$poly',4269),3309)) and intersects(the_geom,transform(GeometryFromText('$poly',4269),3309)) order by huname LIMIT 40/;
	    $nq = $dbh->prepare($sql);
	    $nq->execute();
	}
       elsif ($view eq 'school_dist_u') {
	    $sql = qq/SELECT first_name,first_name,'California School Districts' FROM school_dist_u WHERE (the_geom && transform(GeometryFromText('$poly',4269),3309)) and intersects(the_geom,transform(GeometryFromText('$poly',4269),3309)) order by first_name LIMIT 40/;
	    $nq = $dbh->prepare($sql);
	    $nq->execute();
	}
       elsif ($view eq 'hydrogp') {
	    $sql = qq/SELECT name,name,'California Hydrology Polygons' FROM hydrogp WHERE (the_geom && transform(GeometryFromText('$poly',4269),3309)) and intersects(the_geom,transform(GeometryFromText('$poly',4269),3309)) order by name LIMIT 40/;
	    $nq = $dbh->prepare($sql);
	    $nq->execute();
	}
       elsif ($view eq 'hydrogl') {
	    $sql = qq/SELECT name,name,'California Hydrology Lines' FROM hydrogl WHERE (the_geom && transform(GeometryFromText('$poly',4269),3309)) and intersects(the_geom,transform(GeometryFromText('$poly',4269),3309)) order by name LIMIT 40/;
	    $nq = $dbh->prepare($sql);
	    $nq->execute();
	}
       elsif ($view eq 'asmplan4') {
	    $sql = qq/SELECT distinct id, 'Assembly ' || id, 'California Assembly Districts' FROM asmplan4 WHERE (the_geom && GeometryFromText('$poly',4269)) and intersects(the_geom,GeometryFromText('$poly',4269)) order by id/;
	    $nq = $dbh->prepare($sql);
	    $nq->execute();
	}
       elsif ($view eq 'sen_law') {
	    $sql = qq/SELECT distinct district,('Senate ' || district),'California Senate Districts' FROM  sen_law WHERE (the_geom && GeometryFromText('$poly',4269)) and intersects(the_geom,GeometryFromText('$poly',4269)) order by district/;
	    $nq = $dbh->prepare($sql);
	    $nq->execute();
	}
       elsif ($view eq 'cng_law') {
	    $sql = qq/SELECT distinct district,('Congress ' || district),'California Congressional Districts' FROM cng_law WHERE (the_geom && GeometryFromText('$poly',4269)) and intersects(the_geom,GeometryFromText('$poly',4269)) order by district/;
	    $nq = $dbh->prepare($sql);
	    $nq->execute();
	}
	elsif ($view =~ /gnis|calgnis/) {
	    $sql = qq/SELECT feature_id,feature_na || ' (' || feature_cl || ')','GNIS Places'   FROM calgeonames2009 WHERE (the_geom && transform(GeometryFromText('$poly',4269),3310)) and intersects(the_geom,transform(GeometryFromText('$poly',4269),3310)) and feature_cl not IN ('church','building') order by feature_na  LIMIT 40/;
	    $nq = $dbh->prepare($sql);
	    $nq->execute();
#	    print "\n$sql\n";
	}
	elsif ($view eq 'zip_view') {
	    $sql = qq/SELECT distinct description,description,'California Postal Codes' FROM zip_view WHERE description NOT LIKE '%XX' and description NOT LIKE '%HH' and (footprint && GeometryFromText('$poly',4269)) and intersects(footprint,GeometryFromText('$poly',4269)) order by description  LIMIT 40/;
	    $nq = $dbh->prepare($sql);
	    $nq->execute();
	}
	else {
	    $sql = qq/SELECT distinct description,description,'$view' FROM $view WHERE (footprint && GeometryFromText('$poly',4269)) and intersects(footprint,GeometryFromText('$poly',4269)) order by description  LIMIT 58/;
	    $nq = $dbh->prepare($sql);
	    $nq->execute();
	}

    $results{$to_lbl} = ();

    while (@row = $nq->fetchrow_array) {
      $id = $row[0];
      $labl = $row[1];
      $src = $row[2];
      
      push(@{$results{$to_lbl}},"$to_coverage:$id|$labl|$src");
   }

}
    my $jsonText;
    my @extArray = split(/,/,$ext);
    my $east  = sprintf "%.5f", $extArray[0];
    my $west  = sprintf "%.5f", $extArray[1];
    my $north  = sprintf "%.5f", $extArray[2];
    my $south  = sprintf "%.5f", $extArray[3];

    $jsonText =  qq/{/;
    while (($key, $value) = each(%results)) {
	next if ((@{$value}) < 1);
	 $jsonText .= qq/"$key":[/; 
	 for $i (0..((@{$value}) - 1)) {
	     @pair = split(/\|/,$value->[$i]);
	     $jsonText .= "{\"id\":\"$pair[0]\",\"label\":\"$pair[1]\",\"src\":\"$pair[2]\"},"
	     }
         $jsonText =~ s/\,$//;
	 $jsonText .= qq/],/;
    }

    $jsonText =~ s/\,$//;
    $jsonText .= qq/}/;

    return $jsonText;
}

sub relatedTop {

    my $sql;
    my (@views,@terms,@types);
    my %results = (); # results in a hash of arrays

    $lbl = $name if (!($lbl));
    @views = ('county_view','city2k','hr_view', 'asmplan4','sen_law','cng_law');

    foreach $view (@views) {
	if ($view eq 'city2k') {
	    $to_auth = 'Census';
	    $to_coverage = 'city2k';
	    $to_lbl = 'PopulatedPlaces';	    
	}
	elsif ($view eq 'county_view') {
	    $to_auth = 'CMCC';
	    $to_coverage = 'counties';
	    $to_lbl = 'County';
	}
	elsif ($view eq 'hr_view') {
	    $to_auth = 'FRAP';
	    $to_coverage = 'calwater22';
	    $to_lbl = 'Watersheds';
	}
	elsif ($view eq 'asmplan4') {
	    $to_auth = 'CMCC';
	    $to_coverage = 'asmplan4';
	    $to_lbl = 'AssemblyDistricts';
	}
	elsif ($view eq 'sen_law') {
	    $to_auth = 'CMCC';
	    $to_coverage = 'sen_law';
	    $to_lbl = 'SenateDistricts';
	}
	elsif ($view eq 'cng_law') {
	    $to_auth = 'CMCC';
	    $to_coverage = 'cng_law';
	    $to_lbl = 'CongressionalDistricts';
	}

       if ($view eq 'city2k') {
	    $sql = qq/SELECT distinct name,label,'Census2000 Urban Areas' FROM city2k where type = 'City' and population > 200000 order by name/;
	    $nq = $dbh->prepare($sql);
	    $nq->execute();
	}
       elsif ($view eq 'hr_view') {
	    $sql = qq/SELECT idnum,hrname,'CalWater Hydrologic Regions' FROM hr order by hrname/;
	    $nq = $dbh->prepare($sql);
	    $nq->execute();
	}
       elsif ($view eq 'asmplan4') {
	    $sql = qq/SELECT distinct id, 'Assembly ' || id, 'California Assembly Districts' FROM asmplan4 order by id/;
	    $nq = $dbh->prepare($sql);
	    $nq->execute();
	}
       elsif ($view eq 'sen_law') {
	    $sql = qq/SELECT distinct district,('Senate ' || district),'California Senate Districts' FROM  sen_law order by district/;
	    $nq = $dbh->prepare($sql);
	    $nq->execute();
	}
       elsif ($view eq 'cng_law') {
	    $sql = qq/SELECT distinct district,('Congress ' || district),'California Congressional Districts' FROM cng_law order by district/;
	    $nq = $dbh->prepare($sql);
	    $nq->execute();
	}
	elsif ($view eq 'county_view')  {
	    $sql = qq/SELECT distinct description,description,'California Counties' FROM county_view order by description LIMIT 58/;
	    $nq = $dbh->prepare($sql);
	    $nq->execute();
	}

    $results{$to_lbl} = ();

    while (@row = $nq->fetchrow_array) {
      $id = $row[0];
      $labl = $row[1];
      $src = $row[2];
      
      push(@{$results{$to_lbl}},"$to_coverage:$id|$labl|$src");
   }

    }

    my $jsonText;

    $jsonText =  qq/{/;
    while (($key, $value) = each(%results)) {
	 $jsonText .= qq/"$key":[/; 
	 for $i (0..((@{$value}) - 1)) {
	     @pair = split(/\|/,$value->[$i]);
	     $jsonText .= "{\"id\":\"$pair[0]\",\"label\":\"$pair[1]\",\"src\":\"$pair[2]\"},"
	     }
         $jsonText =~ s/\,$//;
	 $jsonText .= qq/],/;
    }

    $jsonText =~ s/\,$//;
    $jsonText .= qq/}/;

    return $jsonText;
}

sub place_info {

    my $clip = shift;
    my $auth = shift;
    my $coverage = shift;
    my $name = shift;
    my $lbl = shift;
    my $ext = shift;
    
    my ($sql,$poly);
    my (@views,@terms,@types);
    my $area;
    my %results = (); # results in a hash of arrays

    $lbl = $name if (!($lbl));

    if ($ext) {
	$poly = getExtPolygon($ext,4269,\$area);
    }
    else {
	$poly = getPolygon($clip,4269,\$area);
    }

    @views = ('county_view','city2k','asmplan4','sen_law','cng_law');


    foreach $view (@views) {
	if ($view eq 'city2k') {
	    $to_auth = 'Census';
	    $to_coverage = 'city2k';
	    $to_lbl = 'Populated Places';	    
	}
	elsif ($view eq 'county_view') {
	    $to_auth = 'CMCC';
	    $to_coverage = 'counties';
	    $to_lbl = 'County';
	}
	elsif ($view eq 'asmplan4') {
	    $to_auth = 'CMCC';
	    $to_coverage = 'asmplan4';
	    $to_lbl = 'Assembly Districts';
	}
	elsif ($view eq 'sen_law') {
	    $to_auth = 'CMCC';
	    $to_coverage = 'sen_law';
	    $to_lbl = 'Senate Districts';
	}
	elsif ($view eq 'cng_law') {
	    $to_auth = 'CMCC';
	    $to_coverage = 'cng_law';
	    $to_lbl = 'Congressional Districts';
	}

       if ($view eq 'city2k') {
	    $sql = qq/SELECT distinct name,label FROM city2k WHERE (the_geom && transform(GeometryFromText('$poly',4269),3310)) and intersects(the_geom,transform(GeometryFromText('$poly',4269),3310)) and upper(name) not like '%(%)' order by name LIMIT 40/;
	    $nq = $dbh->prepare($sql);
	    $nq->execute();
	}
       elsif ($view eq 'school_dist_u') {
	    $sql = qq/SELECT first_name,first_name FROM school_dist_u WHERE (the_geom && transform(GeometryFromText('$poly',4269),3309)) and intersects(the_geom,transform(GeometryFromText('$poly',4269),3309)) order by first_name LIMIT 40/;
	    $nq = $dbh->prepare($sql);
	    $nq->execute();
	}
       elsif ($view eq 'asmplan4') {
	    $sql = qq/SELECT distinct id, 'Assembly ' || id FROM asmplan4 WHERE (the_geom && GeometryFromText('$poly',4269)) and intersects(the_geom,GeometryFromText('$poly',4269)) order by id/;
	    $nq = $dbh->prepare($sql);
	    $nq->execute();
	}
       elsif ($view eq 'sen_law') {
	    $sql = qq/SELECT distinct district,('Senate ' || district) FROM  sen_law WHERE (the_geom && GeometryFromText('$poly',4269)) and intersects(the_geom,GeometryFromText('$poly',4269)) order by district/;
	    $nq = $dbh->prepare($sql);
	    $nq->execute();
	}
       elsif ($view eq 'cng_law') {
	    $sql = qq/SELECT distinct district,('Congress ' || district) FROM cng_law WHERE (the_geom && GeometryFromText('$poly',4269)) and intersects(the_geom,GeometryFromText('$poly',4269)) order by district/;
	    $nq = $dbh->prepare($sql);
	    $nq->execute();
	}
	elsif ($view eq 'geoname') {
	    $sql = qq/SELECT gid,name || ' (' || type || ')'  FROM geonamea WHERE (the_geom && transform(GeometryFromText('$poly',4269),3309)) and intersects(the_geom,transform(GeometryFromText('$poly',4269),3309)) order by type,name  LIMIT 40/;
	    $nq = $dbh->prepare($sql);
	    $nq->execute();
	}
	elsif ($view eq 'zip_view') {
	    $sql = qq/SELECT distinct description,description FROM zip_view WHERE description NOT LIKE '%XX' and description NOT LIKE '%HH' and (footprint && GeometryFromText('$poly',4269)) and intersects(footprint,GeometryFromText('$poly',4269)) order by description  LIMIT 40/;
	    $nq = $dbh->prepare($sql);
	    $nq->execute();
	}
	else {
	    $sql = qq/SELECT distinct description,description FROM $view WHERE (footprint && GeometryFromText('$poly',4269)) and intersects(footprint,GeometryFromText('$poly',4269)) order by description  LIMIT 58/;
	    $nq = $dbh->prepare($sql);
	    $nq->execute();
	}

    $results{$to_coverage} = ();

    while (@row = $nq->fetchrow_array) {
      $id = $row[0];
      $lbl = $row[1];
      
      push(@{$results{$to_coverage}},"$to_coverage:$id|$lbl");
   }

}
    my $jsonText;
    $jsonText =  qq/{"ResultSet":{/;
    while (($key, $value) = each(%results)) {
	 $jsonText .= qq/"$key":[/; 
	 for $i (0..((@{$value}) - 1)) {
	     @pair = split(/\|/,$value->[$i]);
	     $jsonText .= "{\"id\":\"$pair[0]\",\"label\":\"$pair[1]\"},"
	     }
	 $jsonText .= qq/],/;
    }

    $jsonText =~ s/\,$//;
    $jsonText .= qq/}}/;

    return $jsonText;
}

sub cnddb_query {
    @args = ("wget http://maps.dfg.ca.gov/webservices/cnddbquad/quad.asmx/GetQuadInfo?quadcode=3811911", $_);
    system qq[(@args)];
    print qq[$_];
}

sub address2ll {
 
    my $adr = shift;
    my ($ora) = Geo::Coder::US->geocode($adr);
    my @matches = ($ora->{long}, $ora->{lat});
    return @matches;
}

sub address2ll_old {

    my $adr = shift;
    my $response = `echo $adr | address2ll`;
    if ($response !~ /success/i) {
	return 0
	}
    else {
	$response =~ s/.*At/At/s;
	$response =~ s/End.*//is;
	$response =~ s/.*@//s;
	$response =~ s/\s//g;
	return split(',',$response);
    }
    
}

sub ceic_intersect {

    my $clip = shift;
    my $lbl = shift;
    my $topic = shift;
    my $content = shift;
    
    my $area;

    if ($ext) {
	$poly = getExtPolygon($ext,3310,\$area);
    }
    else {
	$poly = getPolygon($clip,3310,\$area);
    }

    $sql = qq[SELECT  d.id,d.title, area(difference(envelope(geom),envelope(GeometryFromText('$poly',3309)))) as fit ];
    $from = qq[FROM dataset_place p, dataset d, ceic_node c ];
    $where = qq[WHERE d.ceic_node_id = c.id and p.dataset_id = d.id ];
    if ($topic) {
        $from .= qq[, dataset_theme t ];
        $where .= qq[and t.dataset_id = d.id and upper(t.themekey) = UPPER('$topic') ];
    }
    if ($content) {
        $from .= qq[, dataset_content c ];
        $where .= qq[and c.dataset_id = d.id and upper(c.content) = UPPER('$content') ];
    }

    $where .= qq[and (area(difference(envelope(geom),envelope(GeometryFromText('$poly',3309)))) < 20000000000) 
	    order by fit limit 100];

    $sql .= $from . $where;
    $dq = $dbh3->prepare($sql);
#    $dq->execute();

#    print $sql;

    print qq[<span class="subtitle">Matched on Spatial Query</span><br>] if ($dq->execute()); 
    print qq[<table border=0>];
    while (@row = $dq->fetchrow_array) {
      push(@id,$row[0]);
      push(@title,$row[1]);
    }
    $dq->finish();

    ## now match on the place name occuring in the title or placekey fields
    $lbl =~ s/\(.*\)//;
    if ($topic) {
	$sql = qq/SELECT d.id,d.title 
	FROM dataset d, dataset_place p, ceic_node c, dataset_theme t  
	WHERE d.ceic_node_id = c.id AND p.dataset_id = d.id AND t.dataset_id = d.id AND 
	(UPPER(placekey) LIKE UPPER('$lbl%') OR UPPER(d.title) LIKE UPPER('$lbl%')) AND
	upper(t.themekey) = UPPER('$topic') AND  
	ORDER BY title LIMIT 100/;    
    }
    else {
	$sql = qq/SELECT d.id,d.title
	FROM dataset d, dataset_place p, ceic_node c 
	WHERE d.ceic_node_id = c.id AND p.dataset_id = d.id AND 
	(UPPER(placekey) LIKE UPPER('$lbl%') OR UPPER(d.title) LIKE UPPER('$lbl%')) 
	ORDER BY title LIMIT 100/;    
    }

    $nq = $dbh3->prepare($sql);
    $nq->execute();

    @row = [];
    while (@row = $nq->fetchrow_array) {
      push(@id,$row[0]);
      push(@$title,$row[1]);
      }

    $nq->finish();
}

sub nrpi_intersect {

    my $clip = shift;
    my $lbl = shift;
    
    $linkto = 'placefinder' if (!($linkto));
    # if called with a toShp only return a list otherwise print the results
    my $printing = 1 if (!($toShp));
    my ($sql,$poly);
    my (@views,@matchList);
    my $area;

    $lbl = $name if (!($lbl));

    $poly = getPolygon($clip,4269,\$area);

    $to_auth = 'CERES';
    $to_coverage = 'spatialdomain';
    $to_lbl = 'CEIC Defined Regions';

    print qq[<div align="left">]  if ($printing);

    $sql = qq/SELECT title,online_link FROM nrpi_link WHERE (the_geom && transform(GeometryFromText('$poly',4269),3309)) and intersects(the_geom,transform(GeometryFromText('$poly',4269),3309)) order by title/;
    $nq = $dbh->prepare($sql);
    $nq->execute();

    my ($shpname,$link,$cnt);
    $cnt = 0;
    while (@row = $nq->fetchrow_array) {
      $shpname = $row[0];
      $link = $row[1];
      $cnt++;
      print qq[<a href="$link" class="blueLink">$shpname</a><br>] if ($printing);

   }

    print qq[<span class="blueLink">No projects found for this locale</span>] if ($cnt lt 1);
    print qq[</div>] if ($printing);
}

sub shp2quadMap {

    my $clip = shift;
    my $lbl = shift;
    my $size = shift;
    my $linkto = shift;
    my $mapName = shift;

    $mapName = 'quadMap' if (!($mapName));
    my ($sql,$poly,$cell,$ocodeStr);

    $poly = getPolygon($clip,4269);

    $size = 350 if (!($size));
    $linkto = 'drg' if (!($linkto));

## prepare the quad query
    $sql = qq/SELECT distinct description,description FROM grid_view WHERE length(description) = 7 and footprint && GeometryFromText(?,4269) and intersects(footprint,GeometryFromText(?,4269)) order by description/;


    $quadQuery = $dbh->prepare($sql);
    $quadQuery->execute($poly,$poly) || die "Could Not Execute quadQuery";
    my (@row,@ocode);
    while (@row = $quadQuery->fetchrow_array) {
      $cell = $row[1];
      if (!(join(' ',@ocode) =~ m/$cell/i)) {
	  push(@ocode,$cell);
      }
  }

$ocodeStr = join(',',@ocode);

    if ($coverage eq 'calwater22') {
	print qq[<img height=$size width=$size name="mappedImage" src="http://atlas.ca.gov/cgi-bin/test/spotquad?idnum=$name&amp;code=$quad&amp;cell=$ocodeStr&amp;width=$size&amp;height=$size&amp;mode=sr&amp;zoom=1&amp;spot=$spot" usemap="#$mapName" alt="$lbl"><br><br>];
    }
    elsif ($coverage eq 'counties') {
	print qq[<img height=$size width=$size name="mappedImage" src="http://atlas.ca.gov/cgi-bin/test/spotquad?cnty=$name&amp;code=$quad&amp;cell=$ocodeStr&amp;width=$size&amp;height=$size&amp;mode=sr&amp;zoom=1&amp;spot=$spot" usemap="#$mapName"  alt="$lbl"><br><br>];
    }
    else {
	print qq[<img height=$size width=$size name="mappedImage" src="http://atlas.ca.gov/cgi-bin/test/spotquad?code=$quad&amp;cell=$ocodeStr&amp;width=$size&amp;height=$size&amp;mode=sr&amp;zoom=1&amp;spot=$spot" usemap="#$mapName"  alt="$lbl"><br><br>];
    }

# print qq[<b>Select a Quad Intersecting $lbl for $linkto Download</b>];

map_quad('',$ocodeStr,'',$size,$linkto,$mapName);

}

sub place_form {

    my $auth = shift;
    my $coverage = shift;
    my $name = shift;
    my $lbl = shift;
    my $clip = shift;

print qq[

    <form name="ajxForm" method="post" action="placeNameQuery" id="ajxForm">
    <table  cellpadding="1" cellspacing="0" border="0" style="background-color:#FFFFFF;font-family:Arial;font-size:8pt;width:620px;border-collapse:collapse;">
        <tr style="height:25px;">
            <td colspan="2" valign="top" align="left">
	        <b>&nbsp;Place Name:</b>
                <input name="name" type="text" id="term" AUTOCOMPLETE = "off" onkeyup="EventKeyUp(event)" style="color:Navy;background-color:#FFFFFF;border-style:Groove;width:240px;">
		<input type="button" value="clear" onClick="this.form.term.value = '';document.getElementById('term:2').innerHTML = '';" /> 
		<input type="button" value="Match Sub Strings" onClick="subStrMatch(event);" />
                <input type="hidden" value="" name="clip" /><br>

            </td>
        </tr>
<!--	<tr><td align="center">Hint: Begin typing a place name and pause for name completion. Click on Match for substring matches.</td></tr> -->
	<tr><td colspan=2 valign="top" height=100>
                <div id="term:2" style="position:absolute;z-index:97;visibility:visible;height:90px;width:592px;">
                </div>
	</td>
	</tr>
	<tr><td colspan=2 align="left">
	</tr>
    </table>
</form>

];

}

sub place_form_gnis {

    my $auth = shift;
    my $coverage = shift;
    my $name = shift;
    my $lbl = shift;
    my $clip = shift;

print qq[

    <form name="ajxForm" method="post" action="placeNameQuery" id="ajxForm">
    <table  cellpadding="1" cellspacing="0" border="0" style="background-color:#FFFFFF;font-family:Arial;font-size:8pt;width:620px;border-collapse:collapse;">
        <tr style="height:25px;">
            <td colspan="2" valign="top" align="left">
	        <b>&nbsp;Place Name:</b>
                <input name="name" type="text" id="term" AUTOCOMPLETE = "off" onkeyup="EventKeyUp(event)" style="color:Navy;background-color:#FFFFFF;border-style:Groove;width:240px;">
		<input type="button" value="clear" onClick="this.form.term.value = '';document.getElementById('term:2').innerHTML = '';" /> 
		<input type="button" value="Match Sub Strings" onClick="subStrMatch(event);" />
		<input type="button" value="Filter" onClick="toggleFilter()" />
                <div id="filter_div" style="position:absolute;z-index:97;visibility:hidden;left:30;top:30;height:90px;width:592px;">
	 <br><b>County:</b>&nbsp;<select name="county"><option>All Counties</option>];

    my $sql = "select distinct name from cnty24k97 order by name";
    my $st = $dbh->prepare($sql);
    $st->execute();
    while (@row = $st->fetchrow_array) {
	print qq[<option>$row[0]</option>];
    }
print qq[      </select>
	        &nbsp;<b>Place Type:</b>&nbsp;<select name="placeType"><option>All Types</option>
	            <option>Populated Places</option>
	            <option>Buildings, Structures, other Infrastructure</option>
	            <option>Locales and Natural Areas</option>
	        </select>
	        </div>
                <input type="hidden" value="" name="clip" /><br>

            </td>
        </tr>
<!--	<tr><td align="center">Hint: Begin typing a place name and pause for name completion. Click on Match for substring matches.</td></tr> -->
	<tr><td colspan=2 valign="top" height=100>
                <div id="term:2" style="position:absolute;z-index:97;visibility:visible;height:90px;width:592px;">
                </div>
	</td>
	</tr>
	<tr><td colspan=2 align="left">
	</tr>
    </table>
</form>

];

}

sub map_quad {

    my $quad = shift;
    my $cell = shift;
    my $ocodeStr = shift;
    my $spot = shift;
    my $size = shift;
    my $linkto = shift;
    my $mapName = shift;
    my $callback = shift;

    $mapName = 'quadMap' if (!($mapName));
    my ($name,$points,$sql,$coords,$st);

    print qq[<MAP NAME="$mapName">];

    $ocodeStr =~ s/,/','/g;

    if ($ocodeStr) {
	$ext_sql = qq/select xmax(extent(the_geom)), xmin(extent(the_geom)), ymax(extent(the_geom)), ymin(extent(the_geom)) FROM mapgrida WHERE substr(ocode,2,7) IN ('$ocodeStr')/;
	$sql = qq/SELECT substr(ocode,2,7) as cell, AsText(THE_GEOM), mapname FROM mapgrida WHERE substr(ocode,2,7) IN ('$ocodeStr')/;
	$st = $dbh->prepare($ext_sql);
    }
    else {
	$ext_sql = qq/select xmax(extent(transform(polygon,3309))), xmin(extent(transform(polygon,3309))), ymax(extent(transform(polygon,3309))), ymin(extent(transform(polygon,3309))) FROM degreeGridView where cell = '$quad'/;
#	$ext_sql = qq/select xmax(extent(the_geom)), xmin(extent(the_geom)), ymax(extent(the_geom)), ymin(extent(the_geom)) FROM mapgrida WHERE substr(ocode,2,7) like '$quad%'/;
	$sql = qq/SELECT substr(ocode,2,7) as cell, AsText(THE_GEOM), mapname FROM mapgrida WHERE substr(ocode,2,7) like '$quad%'/;
	$st = $dbh->prepare($ext_sql);
    }

    # Execute extend query
    $st->execute();
    @ext = $st->fetchrow_array();
    $st->finish;

    $maxx = $ext[0];
    $minx = $ext[1];
    $maxy = $ext[2];
    $miny = $ext[3];

   # remove pesky quotes added to neg numbers 
    $maxx =~ s/\'//g;
    $minx =~ s/\'//g;
    $maxy =~ s/\'//g;
    $miny =~ s/\'//g;

    # Execute cell query
    my $st = $dbh->prepare($sql);
    $st->execute();
    $dat = $st->fetchall_arrayref();    
    $st->finish;

    foreach $row (@$dat) {
	$cell = $row->[0];
	$points = $row->[1];
	$name = $row->[2]; ## . ' (' . $cell . ')';
	$points =~ s/[\(|\)|MULTIPOLYGON]//g;
	$points =~ s/\s/,/g;
	@pt = split(',',$points);
        $coords = create_imagemap($maxx,$maxy,$minx,$miny,$size);
	if ($linkto eq 'drg') {
	    $area = qq[<area shape="poly" title="$name" coords="$coords" href="http://atlas.ca.gov/cgi-bin/test/drg?cell=$cell;name=$name" target="_blank" alt="$name">\n];
	}
	elsif ($linkto eq 'doqq') {
	    $cell =~ s/^o//;
	    $area = qq[<area shape="poly" title="$name" coords="$coords" href="http://atlas.ca.gov/cgi-bin/test/doqq?cell=$cell;name=$name" target="_blank" alt="$name">\n];
	}
	elsif ($linkto eq 'callback') {
	    $cell =~ s/^o//;
	    $area = qq[<area shape="poly" title="$name" coords="$coords" href="#" onClick="$callback('$cell','$name');" alt="$name">\n];
	}
	elsif ($linkto eq 'cnddb') {
	    $a = substr($cell,5,1);
	    my $quadcode = substr($cell,0,5) . (ord(lc($a)) - 96) . substr($cell,6,1);
#	    $area = qq[<area shape="poly" title="$name" coords="$coords" href="http://cwp.resources.ca.gov/browser/cnddbQuery.epl?quadcode=$quadcode;quadname=$name" target="_blank" alt="$name">\n];
	    $area = qq[<area shape="poly" title="$name" coords="$coords" href="http://cwp.resources.ca.gov/assets/php/speciesQuery?quadcode=$quadcode;quadname=$name" target="_blank" alt="$name">\n];
	}
        else {
	    $area = qq[<area shape="poly" title="$name" coords="$coords" href="$linkto?auth=DOQQ;coverage=Grid;name=$cell;lbl=$name" alt="$name">\n];
	}
	print "$area";
    }

    print qq[</MAP>];
}


sub map_degree {

    my $quad = shift;
    my $size = shift;
    my $linkto = shift;
    my $mapName = shift;

    $mapName = 'degreeMap' if (!($mapName));

    my ($points,$sql,$minx,$maxx,$miny,$maxy,$dat,$name,$points,$coords);

    print qq[<MAP NAME="$mapName">];

#    $sql = qq/select xmax(extent(transform(polygon,3310))), xmin(extent(transform(polygon,3310))), ymax(extent(transform(polygon,3310))), ymin(extent(transform(polygon,3310))) FROM degreeGridView/;
#    $sql = qq/select xmax(extent(polygon)), xmin(extent(polygon)), ymax(extent(polygon)), ymin(extent(polygon)) FROM degreeGridView/;
    $sql = qq/select xmax(extent(footprint)), xmin(extent(footprint)), ymax(extent(footprint)), ymin(extent(footprint)) FROM ca_outline_view/;

    my $st = $dbh->prepare($sql);
    $st->execute();
    @ext = $st->fetchrow_array();
    $st->finish();

    $maxx = int($ext[0]);
    $minx = int($ext[1]);
    $maxy = int($ext[2]);
    $miny = int($ext[3]);

   # remove pesky quotes added to neg numbers 
    $maxx =~ s/\'//g;
    $minx =~ s/\'//g;
    $maxy =~ s/\'//g;
    $miny =~ s/\'//g;

    $sql = qq/SELECT cell, AsText(transform(POLYGON,4269)) FROM degreeGridView order by cell/;
    $st = $dbh->prepare($sql);
    $st->execute();
    $dat = $st->fetchall_arrayref();    
    $st->finish();

    foreach $row (@$dat) {
	$name = $row->[0];
	$quad = $name;
	$points = $row->[1];
	$points =~ s/[\(|\)|POLYGON]//g;
	$points =~ s/\s/,/g;
	@pt = split(',',$points);
        $coords = create_imagemap($maxx,$maxy,$minx,$miny,$size);
	$area = qq[<area shape="poly" title="$name" coords="$coords" href="$linkto?auth=DOQQ;coverage=Grid;name=$quad;lbl=$name" alt="$name">\n];
	print "$area";
    }
    print qq[</MAP>];
}


sub map_intersect {
    my $auth = shift;
    my $coverage = shift;
    my $name = shift;
    my $toShp = shift;
    my $size = shift;
    my $linkto = shift;
    my $ptBuffer = shift;
    my $mapName = shift;

    $mapName = 'shpMap' if (!($mapName));
    my ($ext,$matches,$points,$coords,$area,$maxx,$maxy,$minx,$miny);

    my %view;
    $view{counties} = 'county_view';
    $view{bioregions} = 'bioregion_view';
    $view{grid} = 'grid_view';
    $view{urban_areas} = 'urbanarea_view';
    $view{zip_codes} = 'zip_view';
    $view{calwater22} = 'hu_view';
    $view{geoname} = 'geoname';

    my %authFor;
    $authFor{counties} = 'CMCC';
    $authFor{bioregions} = 'FRAP';
    $authFor{grid} = 'DOQQ';
    $authFor{urban_areas} = 'FRAP';
    $authFor{zip_codes} = 'Census';
    $authFor{calwater22} = 'FRAP';
    $authFor{geoname} = 'GNIS';

    $matches = shp_intersect($clip,'','',$view{$toShp});
    $ext = getExtent($clip,3309);
    $maxx = $ext[0];
    $minx = $ext[1];
    $maxy = $ext[2];
    $miny = $ext[3];

   # remove pesky quotes added to neg numbers 
    $maxx =~ s/\'//g;
    $minx =~ s/\'//g;
    $maxy =~ s/\'//g;
    $miny =~ s/\'//g;

    print qq[<MAP NAME="$mapName">];
    foreach $match (@{$matches}) {
	$match =~ s/^.*://;
	$points = getPolygon($clip,3309);
	$points =~ s/[\(|\)|MULTIPOLYGON]//g;
	$points =~ s/\s/,/g;
	@pt = split(',',$points);
        $coords = create_imagemap($maxx,$maxy,$minx,$miny,$size);
	$area = qq[<area shape="poly" title="$match" coords="$coords" href="$linkto?auth=$authFor{$toShp}&amp;coverage=$toShp&amp;name=$match&amp;lbl=$match">\n];
	print "$area";
    }
    print qq[</MAP>];
}

sub map_coverage {
    my $coverage = shift;
    my $size = shift;
    my $linkto = shift;
    my $mapName = shift;

    $mapName = 'locMap' if (!($mapName));
    my ($ext,$matches,$points,$coords,$area,$maxx,$maxy,$minx,$miny);

    my %view;
    $view{counties} = 'county_view';
    $view{bioregions} = 'bioregion_view';

    $view{grid} = 'grid_view';
    $view{urban_areas} = 'urbanarea_view';
    $view{zip_codes} = 'zip_view';
    $view{calwater22} = 'hu_view';
    $view{geoname} = 'geoname';

    my %authFor;
    $authFor{counties} = 'CMCC';
    $authFor{bioregions} = 'FRAP';
    $authFor{grid} = 'DOQQ';
    $authFor{urban_areas} = 'FRAP';
    $authFor{zip_codes} = 'Census';
    $authFor{calwater22} = 'FRAP';
    $authFor{geoname} = 'GNIS';

    my $sql = qq[select description, max(area(footprint)) from $view{$coverage} group by description];
    my $st = $dbh->prepare($sql);
    $st->execute();
    my $matches = $st->fetchall_arrayref();    
    $st->finish();

    $ext = getExtent('ca_outline:california',3309);
    $maxx = $ext[0];
    $minx = $ext[1];
    $maxy = $ext[2];
    $miny = $ext[3];

   # remove pesky quotes added to neg numbers 
    $maxx =~ s/\'//g;
    $minx =~ s/\'//g;
    $maxy =~ s/\'//g;
    $miny =~ s/\'//g;

    print qq[<MAP NAME="$mapName">];
    foreach $row (@{$matches}) {
	$match = $row->[0];
	$points = getPolygon($clip,3309);
	$points =~ s/[\(|\)|MULTIPOLYGON]//g;
	$points =~ s/\s/,/g;
	@pt = split(',',$points);
        $coords = create_imagemap($maxx,$maxy,$minx,$miny,$size);
	$area = qq[<area shape="poly" title="$match" coords="$coords" href="$linkto?auth=$authFor{$coverage}&amp;coverage=$coverage&amp;name=$match&amp;lbl=$match">\n];
	print "$area";
    }
    print qq[</MAP>];
}

sub create_imagemap {

    #@polygon is a simple list that stores the real world coordinates of the object pulled from the database
    my @vertices = @pt;
    my $maxx = shift;
    my $maxy = shift;
    my $minx = shift;
    my $miny = shift;
    my $size = shift;

    my @imgshape;

    ## We need to square up our extent so it's proportional to the image
    my $sizex = abs($maxx - $minx);
    my $sizey = abs($maxy - $miny);
    if ($sizey > $sizex) {
	$minx -= ($sizey - $sizex)/2; 
	$maxx += ($sizey - $sizex)/2;
    }
    elsif ($sizex > $sizey) {
	$miny -= ($sizex - $sizey)/2; 
	$maxy += ($sizex - $sizey)/2;
    }

    #loop through the list grabbing each x and y coordinate and converting the geo-coordinate to image coordinates
    while (@vertices) {

         #grab first coordinate, remove any parentheses
        $xvert = shift(@vertices);
        $xvert =~ s/[\(\)]//g;

         #convert x map coordinate to image coordinates
        $cellsizex = ($maxx - $minx)/($size - 1);
        $ximg = int(($xvert - $minx)/$cellsizex);

        #add image coord. to the new list
        push (@imgshape,$ximg);

        #grab second coordinate, remove any parentheses
        $yvert = shift(@vertices);
        $yvert =~ s/[\(\)]//g;

        #convert y map coordinate to image coordinates
        $cellsizey = ($maxy -$miny)/($size - 1);
        $yimg = int(($maxy - $yvert)/$cellsizey);

        #add image coord. to the new list
        push (@imgshape,$yimg);

       }

    #form the <area> tag for html output
    $coords = join(",",@imgshape);
    return $coords;
}

sub navbar {

    my $clip = shift;
    my $lbl = shift;
    my $action = shift;
    my $mode = shift;

    my $zoomStr;
    my $intersectStr = "placefinder?clip=$clip&amp;lbl=$lbl&amp;action=crossTo";
    my $relatedStr = "placefinder?auth=$clip=$clip&amp;lbl=$lbl&amp;action=intersect";
    my $imgLoc = "http://ceic.atlas.ca.gov/images";

    my $ext = getExtent($clip,3310,300);
    my $maxx = $ext[0];
    my $minx = $ext[1];
    my $maxy = $ext[2];
    my $miny = $ext[3];

    my $mapsurferStr = qq[http://atlas.ca.gov/mapsurfer/index.phtml?extent=$minx,$miny,$maxx,$maxy];
    if ($coverage eq 'calwater22') {
	$mapZoomStr = qq[http://atlas.ca.gov/cgi-bin/test/showshed?idnum=$name&amp;name=$lbl&amp;size=800&amp;spot=1&amp;zoom=1&amp;mode=drg];
	$imgZoomStr = qq[http://atlas.ca.gov/cgi-bin/test/showshed?idnum=$name&amp;name=$lbl&amp;size=800&amp;spot=1&amp;zoom=1&amp;mode=ls];
    }
    else {
	$mapZoomStr = qq[http://atlas.ca.gov/cgi-bin/test/placeview?clip=$clip&amp;size=800&amp;spot=1&amp;zoom=1];
	$imgZoomStr = qq[http://atlas.ca.gov/cgi-bin/test/placeview?clip=$clip&amp;size=800&amp;spot=1&amp;zoom=1&amp;mode=image];
    }

print qq[
<table border="0" cellpadding="0" cellspacing="0" width="160">
<!-- fwtable fwsrc="navbar.png" fwbase="navbar.gif" fwstyle="Dreamweaver" fwdocid = "742308039" fwnested="0" -->
  <tr>
   <td><img src="$imgLoc/spacer.gif" width="160" height="1" border="0"></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="1" border="0"></td>
	 </tr>
  <tr>
   <td><img name="navbar_r1_c1" src="$imgLoc/navbar_r1_c1.gif" width="160" height="55" border="0"></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="55" border="0"></td>
	 </tr>];

 if ($mode ne 'pick') {
  print qq[ <tr>];

    if (1) {
	print qq[
		 <td><a href="#" onClick="ceic_opt()" onMouseOut="MM_swapImgRestore();"  onMouseOver="MM_swapImage('navbar_r2_c1','','navbar_r2_c1_f2.gif',1);" ><img name="navbar_r2_c1" src="$imgLoc/navbar_r2_c1.gif" width="160" height="16" border="0"></a></td>]}
	      else {
	print qq[
		 <td><a href="#"><img name="navbar_r2_c1" src="$imgLoc/navbar_r2_c1.gif" width="160" height="16" border="0"></a></td>]}

print qq[
   <td><img src="$imgLoc/spacer.gif" width="1" height="16" border="0"></td>
  </tr>
  <tr>
   <td><img name="navbar_r3_c1" src="$imgLoc/navbar_r3_c1.gif" width="160" height="2" border="0"></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="2" border="0"></td>
	 </tr>];


    if ($coverage && $name) {
	print qq[
	 <tr>
		 <td><a href="#" onClick="nrpi_opt()" onMouseOut="MM_swapImgRestore();"  onMouseOver="MM_swapImage('navbar_r4_c1','','navbar_r4_c1_f2.gif',1);" ><img name="navbar_r4_c1" src="$imgLoc/navbar_r4_c1.gif" width="160" height="16" border="0"></a></td>]}
    else {
	print qq[
	  <tr>	 
		 <td><a href="#"><img name="navbar_r4_c1" src="$imgLoc/navbar_r4_c1.gif" width="160" height="16" border="0"></a></td>]}

print qq[
   <td><img src="$imgLoc/spacer.gif" width="1" height="16" border="0"></td>
  </tr>
  <tr>
   <td><img name="navbar_r5_c1" src="$imgLoc/navbar_r5_c1.gif" width="160" height="2" border="0"></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="2" border="0"></td>
  </tr>
  <tr>
	 ];
## Species Data
if ($clip =~ /DOQQ/i && length($name) eq 7) {
   my $a = substr($name,5,1);
   my $quadcode = substr($name,0,5) . (ord(lc($a)) - 96) . substr($name,6,1);
    print qq[<td><a href="#" onClick="window.location = 'http://cwp.resources.ca.gov/cnddbQuery.epl?quadcode=$quadcode&amp;quadname=$lbl'" onMouseOut="MM_swapImgRestore();"  onMouseOver="MM_swapImage('navbar_r6_c1','','navbar_r6_c1_f2.gif',1);" ><img name="navbar_r6_c1" src="$imgLoc/navbar_r6_c1.gif" width="160" height="16" border="0"></a></td>];
}
else {
    print qq[<td><a href="#" onClick="shp2quads_opt('cnddb')" onMouseOut="MM_swapImgRestore();"  onMouseOver="MM_swapImage('navbar_r6_c1','','navbar_r6_c1_f2.gif',1);" ><img name="navbar_r6_c1" src="$imgLoc/navbar_r6_c1.gif" width="160" height="16" border="0"></a></td>];
}

print qq[
   <td><img src="$imgLoc/spacer.gif" width="1" height="16" border="0"></td>
	 </tr>];

    print qq[<!-- End Query Section -->];

print qq[
  <tr>
   <td><img name="navbar_r7_c1" src="$imgLoc/navbar_r7_c1.gif" width="160" height="21" border="0"></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="21" border="0"></td>
  </tr>
	 <tr>];
    if ($coverage && $name) {
	print qq[
   <td><a href="#" onClick="mode='map';base_view(mode)" onMouseOut="MM_swapImgRestore();"  onMouseOver="MM_swapImage('navbar_r8_c1','','navbar_r8_c1_f2.gif',1);" ><img name="navbar_r8_c1" src="$imgLoc/navbar_r8_c1.gif" width="160" height="16" border="0"></a></td>]
}
    else {
	print qq[
   <td><a href="#" ><img name="navbar_r8_c1" src="$imgLoc/navbar_r8_c1.gif" width="160" height="16" border="0"></a></td>]    }
print qq[
   <td><img src="$imgLoc/spacer.gif" width="1" height="16" border="0"></td>
  </tr>
  <tr>
   <td><img name="navbar_r9_c1" src="$imgLoc/navbar_r9_c1.gif" width="160" height="2" border="0"></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="2" border="0"></td>
  </tr>
  <tr>
   <td><a href="#" onClick="mode='image';base_view(mode)" id="menu_image_opt" onMouseOut="MM_swapImgRestore();"  onMouseOver="MM_swapImage('navbar_r10_c1','','navbar_r10_c1_f2.gif',1);" ><img name="navbar_r10_c1" src="$imgLoc/navbar_r10_c1.gif" width="160" height="16" border="0"></a></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="16" border="0"></td>
  </tr>
  <tr>
   <td><img name="navbar_r11_c1" src="$imgLoc/navbar_r11_c1.gif" width="160" height="2" border="0"></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="2" border="0"></td>
  </tr>
  <tr>
	 ];
if ($auth eq 'DOQQ' && length($name) eq 7) {
  print qq[<td><a href="#" onClick="window.location = 'http://atlas.ca.gov/cgi-bin/test/drg?cell=$name&amp;name=$lbl'" onMouseOut="MM_swapImgRestore();"  onMouseOver="MM_swapImage('navbar_r12_c1','','navbar_r12_c1_f2.gif',1);" ><img name="navbar_r12_c1" src="$imgLoc/navbar_r12_c1.gif" width="160" height="16" border="0"></a></td>];
}
else {
    print qq[<td><a href="#" onClick="shp2quads_opt('drg')" onMouseOut="MM_swapImgRestore();"  onMouseOver="MM_swapImage('navbar_r12_c1','','navbar_r12_c1_f2.gif',1);" ><img name="navbar_r12_c1" src="$imgLoc/navbar_r12_c1.gif" width="160" height="16" border="0"></a></td>];
}
print qq[
   <td><img src="$imgLoc/spacer.gif" width="1" height="16" border="0"></td>
  </tr>
  <tr>
   <td><img name="navbar_r13_c1" src="$imgLoc/navbar_r13_c1.gif" width="160" height="1" border="0"></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="1" border="0"></td>
  </tr>

  <tr>
	 ];
if ($auth eq 'DOQQ' && length($name) eq 7) {
    print qq[<td><a href="#" onClick="window.location = 'http://atlas.ca.gov/cgi-bin/test/doqq?cell=$name&amp;name=$lbl'" onMouseOut="MM_swapImgRestore();"  onMouseOver="MM_swapImage('navbar_r14_c1','','navbar_r14_c1_f2.gif',1);" ><img name="navbar_r14_c1" src="$imgLoc/navbar_r14_c1.gif" width="160" height="16" border="0"></a></td>];
}
else {
    print qq[<td><a href="#" onClick="shp2quads_opt('doqq')" onMouseOut="MM_swapImgRestore();"  onMouseOver="MM_swapImage('navbar_r14_c1','','navbar_r14_c1_f2.gif',1);" ><img name="navbar_r14_c1" src="$imgLoc/navbar_r14_c1.gif" width="160" height="16" border="0"></a></td>];
}

print qq[
   <td><img src="$imgLoc/spacer.gif" width="1" height="16" border="0"></td>
  </tr>
  <tr>
   <td><img name="navbar_r15_c1" src="$imgLoc/navbar_r15_c1.gif" width="160" height="1" border="0"></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="1" border="0"></td>
  </tr>
	 <tr>];
    if (($coverage && $name) || ($action eq 'address')) {
	print qq[
		 <td><a href="#" onClick="window.open('$mapsurferStr','','width=800,height=600,status=yes,resizable=yes,scrollbars=yes')" onMouseOut="MM_swapImgRestore();"  onMouseOver="MM_swapImage('navbar_r16_c1','','navbar_r16_c1_f2.gif',1);" ><img name="navbar_r16_c1" src="$imgLoc/navbar_r16_c1.gif" width="160" height="16" border="0"></a></td> ]
	     }
    else {
	print qq[<td><a href="#" ><img name="navbar_r16_c1" src="$imgLoc/navbar_r16_c1.gif" width="160" height="16" border="0"></a></td>   <td><img src="$imgLoc/spacer.gif" width="1" height="16" border="0"></td>
  </tr>
]
	}
} ## if not $mode = 'pick'

print qq[
  <tr>
   <td><img name="navbar_r17_c1" src="$imgLoc/navbar_r17_c1.gif" width="160" height="19" border="0"></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="19" border="0"></td>
  </tr>
  <tr>
   <td><a href="#" onClick="shp_intersect_opt('bioregions')" onMouseOut="MM_swapImgRestore();"  onMouseOver="MM_swapImage('navbar_r18_c1','','navbar_r18_c1_f2.gif',1);" ><img name="navbar_r18_c1" src="$imgLoc/navbar_r18_c1.gif" width="160" height="16" border="0"></a></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="16" border="0"></td>
  </tr>
  <tr>
   <td><img name="navbar_r19_c1" src="$imgLoc/navbar_r19_c1.gif" width="160" height="2" border="0"></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="2" border="0"></td>
  </tr>
  <tr>
   <td><a href="#" onClick="shp_intersect_opt('calwater22')" onMouseOut="MM_swapImgRestore();"  onMouseOver="MM_swapImage('navbar_r20_c1','','navbar_r20_c1_f2.gif',1);" ><img name="navbar_r20_c1" src="$imgLoc/navbar_r20_c1.gif" width="160" height="16" border="0"></a></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="16" border="0"></td>
  </tr>
  <tr>
   <td><img name="navbar_r21_c1" src="$imgLoc/navbar_r21_c1.gif" width="160" height="2" border="0"></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="2" border="0"></td>
  </tr>
  <tr>
   <td><a href="#" onClick="shp_intersect_opt('counties')" onMouseOut="MM_swapImgRestore();"  onMouseOver="MM_swapImage('navbar_r22_c1','','navbar_r22_c1_f2.gif',1);" ><img name="navbar_r22_c1" src="$imgLoc/navbar_r22_c1.gif" width="160" height="16" border="0"></a></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="16" border="0"></td>
  </tr>
  <tr>
   <td><img name="navbar_r23_c1" src="$imgLoc/navbar_r23_c1.gif" width="160" height="1" border="0"></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="1" border="0"></td>
  </tr>
  <tr>
   <td><a href="#" onClick="shp_intersect_opt('urban_areas')" onMouseOut="MM_swapImgRestore();"  onMouseOver="MM_swapImage('navbar_r24_c1','','navbar_r24_c1_f2.gif',1);" ><img name="navbar_r24_c1" src="$imgLoc/navbar_r24_c1.gif" width="160" height="16" border="0"></a></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="16" border="0"></td>
  </tr>
  <tr>
   <td><img name="navbar_r25_c1" src="$imgLoc/navbar_r25_c1.gif" width="160" height="1" border="0"></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="1" border="0"></td>
  </tr>
  <tr>
   <td><a href="#" onClick="shp_intersect_opt('zip_codes')" onMouseOut="MM_swapImgRestore();"  onMouseOver="MM_swapImage('navbar_r26_c1','','navbar_r26_c1_f2.gif',1);" ><img name="navbar_r26_c1" src="$imgLoc/navbar_r26_c1.gif" width="160" height="16" border="0"></a></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="16" border="0"></td>
  </tr>

  <tr>
   <td><img src="$imgLoc/navbar_r25_c1.gif" width="160" height="1" border="0"></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="1" border="0"></td>
  </tr>
  <tr>
   <td><a href="#" onClick="shp_intersect_opt('grid')" onMouseOut="MM_swapImgRestore();"  onMouseOver="MM_swapImage('navbar_quad1','','navbar_quad2.gif',1);" ><img name="navbar_quad1" src="$imgLoc/navbar_quad1.gif" width="160" height="16" border="0"></a></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="16" border="0"></td>
  </tr>


  <tr>
   <td><img src="$imgLoc/navbar_r25_c1.gif" width="160" height="1" border="0"></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="1" border="0"></td>
  </tr>
  <tr>
   <td><a href="#" onClick="related_opt()" onMouseOut="MM_swapImgRestore();"  onMouseOver="MM_swapImage('navbar_related1','','navbar_related2.gif',1);" ><img name="navbar_related1" src="$imgLoc/navbar_related1.gif" width="160" height="16" border="0"></a></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="16" border="0"></td>
  </tr>
  <tr>
   <td><img name="navbar_r27_c1" src="$imgLoc/navbar_r27_c1.gif" width="160" height="20" border="0"></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="20" border="0"></td>
  </tr>
  <tr>
   <td><a href="#" onClick="overview_opt()" onMouseOut="MM_swapImgRestore();"  onMouseOver="MM_swapImage('navbar_r28_c1','','navbar_r28_c1_f2.gif',1);" ><img name="navbar_r28_c1" src="$imgLoc/navbar_r28_c1.gif" width="160" height="16" border="0"></a></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="16" border="0"></td>
  </tr>
  <tr>
   <td><img name="navbar_r29_c1" src="$imgLoc/navbar_r29_c1.gif" width="160" height="2" border="0"></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="2" border="0"></td>
  </tr>
  <tr>
   <td><a href="#" onClick="contacts_opt()" onMouseOut="MM_swapImgRestore();"  onMouseOver="MM_swapImage('navbar_r30_c1','','navbar_r30_c1_f2.gif',1);" ><img name="navbar_r30_c1" src="$imgLoc/navbar_r30_c1.gif" width="160" height="16" border="0"></a></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="16" border="0"></td>
  </tr>
  <tr>
   <td><img name="navbar_r31_c1" src="$imgLoc/navbar_r31_c1.gif" width="160" height="17" border="0"></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="16" border="0"></td>
  </tr>
  <tr>
   <td class="content"><a href="http://ceres.ca.gov"><img src="$imgLoc/ceres_icon_gif.jpg"  border="0" alt="CERES Website"></a></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="16" border="0"></td>
  </tr>
</table>
];

}

sub shpNameQuery {

my ($auth,$coverage);
my $name = shift;

## prepare the box query
my $sql = qq/SELECT distinct auth,coverage,description FROM county_view where upper(description) like upper('%$name%') order by description/;

my $st = $dbh->prepare($sql);
$st->execute();

print qq[<p><table width="100%"><tr><th colspan=2 class="content">Matching Counties</th></tr><tr><td valign="top">];
while (@row = $st->fetchrow_array) {
    $auth = $row[0];
    $coverage = $row[1];
    print qq[<a class="blueLink" href="placefinder?auth=$auth&amp;coverage=counties&amp;name=$row[2]">$row[2]</a><br>];
}

print qq[</td><td valign="top">];

print qq[</td></tr></table>];

## prepare the box query
$sql = qq/SELECT distinct auth,coverage,description FROM urbanarea_view where upper(description) like upper('%$name%') and upper(description) not like '%(%)' order by description/;

$st = $dbh->prepare($sql);
$st->execute();

print qq[<p><table width="100%"><tr><th colspan=2 bgcolor="#FFFFFF" class="content">Matching Urban Areas</th></tr><tr><td valign="top">];
while (@row = $st->fetchrow_array) {
    $auth = $row[0];
    $coverage = $row[1];
    print qq[<a class="blueLink" href="placefinder?auth=$auth&amp;coverage=urban_areas&amp;name=$row[2]">$row[2]</a><br>];
}

print qq[</td><td valign="top">];

print qq[</td></tr></table>];

## prepare the box query
$sql = qq/SELECT distinct ocode,mapname FROM mapgrida where upper(mapname) like upper('%$name%') order by ocode desc/;

$st = $dbh->prepare($sql);
$st->execute();

print qq[<p><table width="100%"><tr><th colspan=2 bgcolor="#FFFFFF" class="content">Matching USGS 7.5 Min Quads</th></tr><tr><td valign="top">];
while (@row = $st->fetchrow_array) {
    $quad = substr($row[0],1,5);
    $cell = substr($row[0],1,7);
    $lbl = $row[1] . ' (' . $cell . ')';
    push (@codes,$cell);
    print qq[<a class="blueLink" href="placefinder?auth=DOQQ&amp;coverage=Grid&amp;name=$cell&amp;lbl=$lbl">$row[1] ($cell)</a><br>];
}

print qq[</td><td valign="top">];

my $ocodes = join(',',@codes);

print qq[</td></tr></table>];

print qq[<p><table width="100%"><tr><th colspan=2 bgcolor="#FFFFFF" class="content">Matching Calwater Watersheds</th></tr><tr><td valign="top">];
## prepare the calw query
$sql = qq/SELECT distinct hrname FROM hr where upper(hrname) like upper('%$name%')/;

$st = $dbh->prepare($sql);
$st->execute();

while (@row = $st->fetchrow_array) {
    print "$row[0] (Region)\n";
}

## prepare the box query
$sql = qq/SELECT distinct hu.idnum,huname,hrname FROM hu,hr where hr.idnum = substr(hu.idnum,1,2) and upper(huname) like upper('%$name%')/;

$st = $dbh->prepare($sql);
$st->execute();

while (@row = $st->fetchrow_array) {
    print qq[<a class="blueLink" href="placefinder?auth=FRAP&amp;coverage=calwater22&amp;name=$row[0]">$row[2](HR) > $row[1](Unit)</a><br>];
}

## prepare the box query
$sql = qq/SELECT distinct ha.idnum,haname,huname,hrname FROM ha,hu,hr where hr.idnum = substr(ha.idnum,1,2) and hu.idnum = substr(ha.idnum,1,5) and upper(haname) like upper('%$name%')/;

$st = $dbh->prepare($sql);
$st->execute();

while (@row = $st->fetchrow_array) {
    print qq[<a class="blueLink" href="placefinder?auth=FRAP&amp;coverage=calwater22&amp;name=$row[0]">$row[3](HR) > $row[2](HU) > $row[1](HA)</a><br>];
}

## prepare the box query
$sql = qq/SELECT distinct hsa.idnum,hsaname,haname,huname,hrname FROM hsa,ha,hu,hr where ha.idnum = substr(hsa.idnum,1,7) and hu.idnum = substr(hsa.idnum,1,5) and hr.idnum = substr(hsa.idnum,1,2) and upper(hsaname) like upper('%$name%')/;

$st = $dbh->prepare($sql);
$st->execute();

while (@row = $st->fetchrow_array) {
    print qq[<a class="blueLink" href="placefinder?auth=FRAP&amp;coverage=calwater22&amp;name=$row[0]">$row[4](HR) > $row[3](HU) > $row[2](HA) > $row[1](HSA)</a><br>];
}

## prepare the box query
$sql = qq/SELECT distinct  spws.idnum,spwsname,huname,hrname FROM spws,hu,hr where hu.idnum = substr(spws.idnum,1,5) and hr.idnum = substr(spws.idnum,1,2) and upper(spwsname) like upper('%$name%')/;

$st = $dbh->prepare($sql);
$st->execute();

while (@row = $st->fetchrow_array) {
    print qq[<a class="blueLink" href="placefinder?auth=FRAP&amp;coverage=calwater22&amp;name=$row[0]">$row[3](HR) > $row[2](HU) > $row[1](SPWS)</a><br>];
}

## prepare the box query
$sql = qq/SELECT distinct pws.idnum,pwsname,huname,hrname FROM pws,hu,hr where hu.idnum = substr(pws.idnum,1,5) and hr.idnum = substr(pws.idnum,1,2) and upper(pwsname) like upper('%$name%')/;

$st = $dbh->prepare($sql);
$st->execute();

while (@row = $st->fetchrow_array) {
    print qq[<a class="blueLink" href="placefinder?auth=FRAP&amp;coverage=calwater22&amp;name=$row[0]">$row[3](HR) > $row[2](HU) > $row[1](PWS)</a><br>];
}

print qq[</td></tr></table>];

## prepare the box query
$sql = qq/SELECT gid,name,type FROM geonamea where upper(name) like upper('%$name%') order by type,name/;

$st = $dbh->prepare($sql);
$st->execute();

print qq[<p><table width="100%"><tr><th colspan=2 bgcolor="#FFFFFF" class="content">Other Places (GNIS)</th></tr><tr><td valign="top">];
while (@row = $st->fetchrow_array) {
    $gid = $row[0];
    $name = $row[1];
    $type = ucfirst($row[2]);
    if ($type =~ /Building|School|Church/i) {
	$ptBuffer = 800
    }
    else {
    $ptBuffer = 1600
    }
    print qq[<a class="blueLink" href="placefinder?auth=GNIS&amp;coverage=geoname&amp;name=$gid&amp;lbl=$name&amp;size=300&amp;ptBuffer=$ptBuffer">$type&nbsp;-&nbsp;$name</a><br>];
}

print qq[</td></tr></table>];

}

sub overview {

    my $imgLoc = "http://ceic.atlas.ca.gov/images";
    return qq[
<table border=0><tr><td class="content" width=500 height=250 valign="middle">
<p><b>The CERES Placefinder</b>

<p>Placefinder is a tool that helps find and retrieve geographic and environmental data from the California Resources Agency, its consituent departments, and other partner organizations.  Placefinder lets users drill down through a map of California using predefined geographies like zip code, watershed, bioregion, county, urban area or by place name.  Once an area of interest is identified, the user can easily locate and retrieve digital orthophoto quads and scanned USGS map sheets for the area.  A user can also ask for a list of rare species and natural communities inventoried by Fish and Game.s Natural Diversity Data Base.  Placefinder also lets users easily crosswalk between these geographies. 
</td></tr>
<tr><td align="center"><a href="http://ceres.ca.gov/"><img src="$imgLoc/ceres_icon_gif.jpg"  border="0" alt="CERES Website"></a></td>
</table>
	 ];
}

sub links {

    my $imgLoc = "http://ceic.atlas.ca.gov/images";
    return qq[
<table border=0><tr><td width=500 height=250 valign="middle">
<p class="subtitle">The CERES Placefinder Contacts

<p><a href="http://resources.ca.gov/" class="blueLink">California Resources Agency</a>
<p><a href="http://gis.resources.ca.gov/" class="blueLink">California Spatial Information Library</a>
<p><a href="http://ceres.ca.gov" class="blueLink">California Environmental Resources Evaluation System (CERES)</a>
<p><a href="http://gis.resources.ca.gov/catalog" class="blueLink">California Environmental Information Catalog (CEIC)</a>
</td></tr>
<tr><td align="center"><a href="http://ceres.ca.gov/"><img name="ceres_icon" src="$imgLoc/ceres_icon_gif.jpg"  border="0" alt="CERES Website"></a></td>
</table>
	 ];

}

sub display_error {

    my $error = shift;

    print qq[

	     <hr>
	     <table><tr><td width=500>
	     <p class="content"><b>A Problem Occured:</b></p>
	     <p class="content"> $error</p>
	     </td></tr></table>
	 ];
}

sub county_ismap {

  print qq[

<MAP NAME="locMap"><area shape="poly" title="Alameda" coords="40,62,40,66,40,66,41,67,36,66,36,67,35,67,34,67,33,66,31,63,30,60,32,60,32,61,33,61,34,62,35,62,35,63,35,63,40,62" href="placefinder?clip=counties:Alameda&lbl=Alameda">
<area shape="poly" title="Alpine" coords="59,45,63,49,62,49,62,50,63,50,63,52,63,53,63,53,62,53,62,54,62,54,62,54,61,54,61,53,61,53,60,53,59,54,57,52,57,53,57,49,59,48,59,47,59,46,59,46,59,45" href="placefinder?clip=counties:Alpine&lbl=Alpine">
<area shape="poly" title="Amador" coords="57,49,57,52,57,52,55,52,54,52,53,52,53,53,52,52,52,53,51,53,51,54,49,55,49,55,49,55,48,56,46,56,46,55,46,52,46,51,47,51,47,51,49,51,50,51,51,52,54,51,55,50,55,50,56,50,57,49,57,49" href="placefinder?clip=counties:Amador&lbl=Amador">
<area shape="poly" title="Butte" coords="42,27,42,27,42,28,42,28,43,28,42,29,42,31,42,31,42,31,42,31,42,32,43,32,43,32,43,32,43,32,44,33,44,33,44,34,44,34,44,34,45,35,45,35,46,35,46,36,45,36,45,36,45,36,45,37,43,37,43,37,43,38,43,38,42,39,40,40,40,40,39,40,36,40,37,38,37,37,37,36,35,36,35,36,36,35,35,35,36,35,35,34,36,34,36,33,36,34,36,33,36,33,35,33,35,32,35,32,35,32,35,32,35,31,38,31,39,30,39,30,40,29,40,29,40,28,41,28,41,28,42,28,42,27" href="placefinder?clip=counties:Butte&lbl=Butte">
<area shape="poly" title="Calaveras" coords="57,52,57,53,57,52,58,53,56,54,53,58,53,59,52,59,52,59,52,59,52,60,52,60,51,61,50,61,47,58,46,56,48,56,49,55,49,55,49,55,51,54,51,53,52,53,52,52,53,53,53,52,54,52,55,52,57,52,57,52" href="placefinder?clip=counties:Calaveras&lbl=Calaveras">
<area shape="poly" title="Colusa" coords="36,39,36,40,36,41,36,41,36,42,36,42,36,42,36,42,36,43,37,43,37,44,37,44,37,45,37,45,31,45,30,45,30,44,30,43,30,43,29,42,30,42,30,42,30,41,29,41,29,41,27,41,27,40,27,40,26,39,27,39,26,38,34,38,34,38,35,38,35,39,36,39" href="placefinder?clip=counties:Colusa&lbl=Colusa">
<area shape="poly" title="Contra Costa" coords="31,58,32,58,33,58,34,58,36,58,36,58,37,58,37,59,38,58,39,58,40,57,40,58,40,58,40,58,40,59,40,59,40,59,40,59,40,60,40,61,40,61,40,62,35,63,35,63,35,62,34,62,33,61,33,61,32,60,30,60,30,60,30,59,31,58" href="placefinder?clip=counties:Contra Costa&lbl=Contra Costa">
<area shape="poly" title="Del Norte" coords="12,1,12,1,12,0,19,0,19,0,19,0,19,1,19,1,18,1,18,1,18,2,17,2,18,3,17,3,18,4,17,5,17,5,17,5,17,6,17,6,17,6,18,7,18,7,18,8,18,8,18,8,18,9,18,9,16,9,16,8,13,7,13,6,13,6,13,5,12,5,12,4,12,3,12,3,12,3,11,3,12,2,12,0,12,1,12,1" href="placefinder?clip=counties:Del Norte&lbl=Del Norte">
<area shape="poly" title="El Dorado" coords="51,46,52,45,53,44,55,44,56,44,56,43,58,43,58,44,59,46,59,46,59,47,59,48,57,49,57,49,57,49,56,50,55,50,55,50,55,51,52,52,51,52,50,51,49,51,48,51,48,51,48,51,48,51,47,51,47,51,46,52,45,48,45,49,45,48,45,47,45,47,46,47,45,47,46,47,46,46,46,46,46,46,46,46,47,45,47,45,48,45,48,45,48,45,49,44,49,44,50,45,50,45,50,45,50,45,51,45,51,46" href="placefinder?clip=counties:El Dorado&lbl=El Dorado">
<area shape="poly" title="Fresno" coords="69,65,71,66,71,66,71,67,72,66,72,67,72,67,72,68,72,68,72,69,73,69,73,69,74,70,73,70,74,72,74,72,76,73,76,74,77,75,77,76,77,76,77,77,75,77,70,77,70,79,66,79,66,80,64,80,62,82,62,82,61,82,61,82,58,82,58,86,54,90,54,89,53,89,52,88,52,88,52,88,51,87,51,87,50,87,50,86,50,86,51,85,50,85,50,84,50,84,51,84,51,81,47,77,52,73,52,73,52,74,53,75,52,75,53,76,54,76,54,77,54,77,54,77,54,77,55,77,55,77,56,77,56,77,57,76,58,76,58,76,59,76,59,76,60,76,61,75,61,74,61,74,61,74,62,74,62,73,62,73,62,73,62,73,63,73,63,73,63,72,63,72,63,72,63,72,64,71,64,72,64,72,65,72,65,71,65,72,65,71,66,71,66,70,66,69,66,69,66,69,66,69,69,65" href="placefinder?clip=counties:Fresno&lbl=Fresno">
<area shape="poly" title="Glenn" coords="35,32,35,32,35,33,36,33,36,33,36,34,36,33,36,34,35,34,36,35,35,35,36,35,35,36,35,36,37,36,37,37,37,38,36,39,35,39,35,38,34,38,34,38,27,38,27,35,25,35,25,34,25,34,25,33,25,33,25,32,35,32" href="placefinder?clip=counties:Glenn&lbl=Glenn">
<area shape="poly" title="Humboldt" coords="13,7,16,8,16,9,19,9,20,9,20,10,20,10,20,10,20,10,20,11,20,12,20,12,20,13,20,13,20,13,20,14,20,14,19,16,18,15,18,15,18,15,18,16,18,16,18,16,18,16,18,17,18,17,18,17,18,18,18,29,13,29,12,28,12,28,12,27,11,27,9,25,9,24,9,23,9,22,9,21,10,19,10,20,10,19,10,19,10,19,10,19,10,19,11,18,11,18,10,18,11,19,11,18,11,18,11,18,11,18,11,17,11,18,11,17,12,17,13,17,12,16,12,16,11,17,11,18,11,18,12,16,12,14,12,14,12,13,12,13,12,12,12,12,13,11,13,7" href="placefinder?clip=counties:Humboldt&lbl=Humboldt">
<area shape="poly" title="Imperial" coords="124,124,124,124,123,125,123,125,123,126,123,127,123,127,123,127,123,128,123,129,123,129,124,130,124,130,124,130,125,130,126,131,126,131,126,131,126,132,126,133,126,133,126,134,125,134,124,134,123,134,123,135,106,137,106,130,106,130,106,125,124,124" href="placefinder?clip=counties:Imperial&lbl=Imperial">
<area shape="poly" title="Inyo" coords="83,67,97,79,110,90,109,90,109,90,83,91,83,91,82,91,82,90,82,90,82,89,81,89,82,88,81,88,81,87,81,87,81,86,81,86,80,85,80,84,80,83,80,82,80,82,80,82,79,82,79,81,79,81,79,81,79,81,78,80,78,80,78,79,78,79,77,79,78,78,77,78,78,78,77,78,77,77,77,76,77,76,77,75,76,74,76,73,74,72,74,72,73,70,74,70,73,69,73,69,72,69,72,68,72,68,72,67,72,67,83,67" href="placefinder?clip=counties:Inyo&lbl=Inyo">
<area shape="poly" title="Kern" coords="82,91,86,91,86,92,86,92,86,93,86,93,86,93,86,93,86,105,71,106,71,106,70,106,70,106,67,106,67,105,67,105,67,105,65,105,65,104,64,104,64,102,64,102,64,102,63,102,63,100,62,100,62,99,60,99,60,98,59,98,59,97,58,97,58,96,57,95,57,94,56,94,56,91,82,91" href="placefinder?clip=counties:Kern&lbl=Kern">
<area shape="poly" title="Kings" coords="63,81,63,81,63,82,64,82,64,84,63,84,63,91,55,91,55,91,55,91,55,90,55,90,54,90,58,86,58,82,61,82,61,82,62,82,62,82,63,81" href="placefinder?clip=counties:Kings&lbl=Kings">
<area shape="poly" title="Lake" coords="25,35,27,35,27,38,26,38,27,39,26,39,28,41,29,41,30,42,30,42,30,42,29,42,30,43,30,44,30,45,31,45,30,45,30,45,31,46,30,46,31,47,30,47,30,47,30,48,28,49,28,48,27,48,27,47,26,46,26,46,25,46,25,45,24,45,24,44,23,44,23,44,23,43,23,43,23,41,23,41,24,40,24,40,24,40,24,40,23,39,23,38,23,38,23,38,23,36,25,36,25,35" href="placefinder?clip=counties:Lake&lbl=Lake">
<area shape="poly" title="Lassen" coords="58,12,58,30,58,34,58,34,56,34,56,34,56,34,57,33,57,31,57,31,56,30,56,30,56,30,56,28,55,29,55,28,54,28,53,27,53,27,53,27,53,27,52,26,51,26,51,26,51,26,51,25,49,25,49,25,49,26,48,26,48,27,48,27,47,26,47,26,47,26,46,26,46,26,46,26,46,23,43,23,43,16,43,16,43,12,58,12" href="placefinder?clip=counties:Lassen&lbl=Lassen">
<area shape="poly" title="Los Angeles" coords="86,105,86,109,86,113,86,117,85,117,85,117,85,117,85,118,85,118,83,118,83,119,82,120,81,121,81,121,81,121,81,121,81,121,81,121,81,121,80,121,80,121,80,121,80,121,80,121,80,121,79,121,79,121,79,121,79,121,79,121,79,121,79,122,79,122,78,121,77,121,77,121,78,120,78,120,77,118,77,118,77,118,77,118,76,117,73,117,72,118,72,117,71,117,71,116,73,115,74,115,74,114,75,114,74,113,71,106,86,105" href="placefinder?clip=counties:Los Angeles&lbl=Los Angeles">
<area shape="poly" title="Madera" coords="66,63,67,63,67,63,68,63,69,64,69,65,66,69,66,69,66,69,66,69,66,70,66,71,65,71,65,71,65,71,64,72,64,71,63,72,63,72,63,72,63,72,63,73,63,73,62,73,62,73,62,73,62,73,62,74,61,74,61,74,61,74,61,75,60,75,60,76,59,76,59,76,58,76,58,76,57,76,56,77,56,77,55,77,55,77,55,77,55,77,54,77,54,77,54,77,54,76,53,76,52,75,53,75,52,74,52,73,52,72,54,72,55,71,57,71,61,68,62,68,62,67,62,67,62,66,63,66,63,66,66,62,66,63" href="placefinder?clip=counties:Madera&lbl=Madera">
<area shape="poly" title="Marin" coords="24,55,23,54,24,54,24,54,24,54,26,56,28,56,28,56,29,57,31,58,30,59,30,61,29,61,28,61,27,60,27,60,27,60,27,60,27,60,27,60,26,60,25,59,24,58,25,58,25,58,24,58,24,58,24,58,24,58,24,58,24,57,24,58,24,57,24,58,24,57,24,58,24,58,24,58,23,58,24,59,23,59,24,56,23,55,25,58,25,57,25,57,24,56,24,55,24,55,24,55" href="placefinder?clip=counties:Marin&lbl=Marin">
<area shape="poly" title="Mariposa" coords="65,61,65,61,66,62,63,66,63,66,62,66,62,67,62,67,62,68,61,68,57,71,57,70,56,70,56,69,55,67,55,67,53,64,54,64,53,64,53,64,54,64,54,64,54,64,54,64,54,63,55,63,55,63,55,63,55,62,56,62,56,62,56,62,57,62,59,63,59,63,59,62,60,63,62,62,62,62,62,61,63,61,63,61,63,60,63,60,64,61,65,61,65,61" href="placefinder?clip=counties:Mariposa&lbl=Mariposa">
<area shape="poly" title="Mendocino" coords="18,29,18,29,25,30,25,30,25,30,25,31,25,32,25,32,25,33,25,33,25,34,25,34,25,36,23,36,23,38,23,38,23,38,23,39,24,40,24,40,24,40,24,40,23,41,23,41,23,43,23,43,23,44,23,44,24,44,24,45,25,45,25,46,26,46,23,46,23,46,22,46,22,47,19,47,19,47,18,47,18,47,17,46,16,45,15,45,15,44,15,44,16,44,16,43,16,42,15,41,15,40,15,39,15,39,15,39,15,39,14,38,15,37,15,37,15,37,15,36,15,35,15,36,15,35,15,34,15,33,15,31,14,31,14,30,13,29,18,29" href="placefinder?clip=counties:Mendocino&lbl=Mendocino">
<area shape="poly" title="Merced" coords="53,64,55,67,55,67,56,69,56,70,57,70,57,71,55,71,54,72,52,72,47,77,45,76,44,75,44,75,43,75,44,74,43,74,43,74,43,73,44,73,43,72,44,72,44,72,47,69,46,68,53,64" href="placefinder?clip=counties:Merced&lbl=Merced">
<area shape="poly" title="Modoc" coords="42,0,58,1,58,12,42,12,42,0" href="placefinder?clip=counties:Modoc&lbl=Modoc">
<area shape="poly" title="Mono" coords="63,49,83,67,72,67,72,66,71,67,71,66,69,65,69,64,68,63,67,63,67,63,67,63,66,63,67,62,67,62,67,61,67,61,67,61,66,60,66,59,66,59,66,58,65,57,64,58,64,58,64,57,64,57,63,57,63,57,63,56,62,56,62,56,62,56,62,55,62,55,62,55,62,54,62,54,62,53,63,53,63,53,63,52,63,50,62,50,62,49,63,49,63,49" href="placefinder?clip=counties:Mono&lbl=Mono">
<area shape="poly" title="Monterey" coords="39,75,39,76,40,77,41,77,41,77,41,78,40,78,41,78,42,79,42,79,42,80,43,80,42,81,43,81,43,81,46,84,46,84,46,84,46,84,46,84,46,84,47,84,47,84,48,84,49,85,49,85,50,85,49,84,50,84,51,85,50,86,50,86,50,87,51,87,51,87,52,88,52,88,52,88,53,89,54,89,54,90,55,90,55,90,55,91,55,91,55,91,42,91,42,91,41,90,40,88,39,88,38,86,36,85,36,84,35,84,36,83,35,83,35,82,35,80,35,80,35,80,35,80,35,79,36,79,36,79,37,76,37,76,37,75,38,75,38,75,38,75,39,75,39,75" href="placefinder?clip=counties:Monterey&lbl=Monterey">
<area shape="poly" title="Napa" coords="31,46,31,46,32,46,32,49,33,49,33,50,33,50,33,50,34,51,33,53,34,54,34,54,33,54,33,55,32,55,33,55,32,55,33,56,32,56,33,56,30,56,31,56,31,56,31,56,31,55,30,55,30,54,29,52,29,52,29,52,29,51,28,50,28,50,28,50,28,49,30,48,30,47,30,47,31,47,30,46,31,46" href="placefinder?clip=counties:Napa&lbl=Napa">
<area shape="poly" title="Nevada" coords="50,37,51,37,51,37,52,38,58,38,58,40,50,40,48,41,48,42,48,42,47,43,47,43,46,44,45,44,45,44,44,44,43,44,43,41,44,41,44,40,44,40,44,39,45,39,45,39,46,39,46,39,47,38,49,38,50,37" href="placefinder?clip=counties:Nevada&lbl=Nevada">
<area shape="poly" title="Orange" coords="85,118,86,119,87,121,88,121,88,121,89,122,90,122,88,124,88,125,88,125,87,126,87,125,86,125,86,125,85,124,84,123,84,123,84,122,84,123,83,123,84,123,83,123,83,123,83,123,83,123,82,122,81,121,82,122,82,121,81,121,81,121,81,121,81,121,81,121,81,121,81,121,81,121,82,120,83,119,83,118,85,118" href="placefinder?clip=counties:Orange&lbl=Orange">
<area shape="poly" title="Placer" coords="49,41,50,40,51,40,58,40,58,43,56,43,56,44,55,44,53,44,52,45,51,46,50,45,50,45,50,45,49,44,49,44,48,45,48,45,48,45,47,45,47,45,46,46,46,46,46,46,46,46,46,47,45,47,46,47,45,47,45,47,45,48,45,49,41,48,41,48,41,48,41,45,42,45,42,44,43,44,44,44,45,44,45,44,46,44,47,43,47,43,49,41" href="placefinder?clip=counties:Placer&lbl=Placer">
<area shape="poly" title="Plumas" coords="43,23,46,23,46,26,46,26,46,26,47,26,47,26,47,26,48,27,48,27,48,26,49,26,49,25,49,25,51,25,51,26,51,26,51,26,52,26,53,27,53,27,53,27,53,27,54,28,55,28,55,29,56,28,56,30,56,30,56,30,57,31,57,31,57,33,56,34,56,34,56,34,51,34,50,34,49,34,49,34,49,33,48,33,48,33,47,34,47,34,47,35,46,36,46,35,45,35,45,35,44,34,44,34,43,33,43,32,43,32,43,32,42,32,42,31,42,31,42,31,42,31,42,29,43,28,42,28,42,28,42,27,42,27,42,27,43,27,43,26,43,25,43,25,42,25,41,24,41,24,41,23,41,23,41,23,43,23" href="placefinder?clip=counties:Plumas&lbl=Plumas">
<area shape="poly" title="Riverside" coords="126,114,126,115,125,115,125,116,124,117,125,117,125,117,125,118,125,118,125,118,125,120,125,120,125,121,125,121,125,121,125,122,125,122,124,123,124,124,92,125,92,125,90,125,90,124,88,124,88,124,90,122,89,122,88,121,88,121,87,121,86,119,86,119,87,119,86,118,87,118,87,118,88,117,88,117,90,117,90,117,92,117,92,117,95,117,95,117,115,116,115,115,126,114" href="placefinder?clip=counties:Riverside&lbl=Riverside">
<area shape="poly" title="Sacramento" coords="41,48,45,49,45,48,46,52,46,55,46,55,44,55,43,55,42,56,42,56,41,55,41,55,40,56,40,57,40,57,40,57,39,58,38,58,37,59,36,58,37,58,38,58,38,58,38,57,39,56,39,55,40,54,40,54,41,53,40,53,41,52,41,52,40,52,40,52,41,51,40,51,41,50,40,50,40,49,40,49,39,49,40,48,41,48" href="placefinder?clip=counties:Sacramento&lbl=Sacramento">
<area shape="poly" title="San Benito" coords="44,74,43,75,44,75,44,75,45,76,47,77,51,81,51,84,50,84,50,84,50,84,49,84,49,85,49,85,49,85,48,84,47,84,47,84,46,84,46,84,46,84,46,84,46,84,46,84,43,81,43,81,42,81,43,80,42,80,42,79,42,79,41,78,40,78,41,78,41,77,40,77,39,75,39,75,40,75,40,74,41,74,41,74,44,74" href="placefinder?clip=counties:San Benito&lbl=San Benito">
<area shape="poly" title="San Bernardino" coords="110,90,122,101,123,103,123,104,124,105,125,105,125,106,125,107,125,107,126,108,126,109,126,109,128,110,128,110,129,111,129,112,129,112,128,113,126,114,126,114,115,115,115,116,95,117,95,117,92,117,92,117,90,117,90,117,88,117,88,117,87,118,87,118,86,118,87,119,86,119,86,119,85,118,85,118,85,117,85,117,85,117,86,117,86,113,86,109,86,105,86,105,86,93,86,93,86,93,86,93,86,92,86,92,86,91,109,90,109,90,110,90" href="placefinder?clip=counties:San Bernardino&lbl=San Bernardino">
<area shape="poly" title="San Diego" coords="" href="placefinder?clip=counties:San Diego&lbl=San Diego">
<area shape="poly" title="San Francisco" coords="30,60,30,60,31,63,29,63,29,62,29,62,29,61,30,61,30,60" href="placefinder?clip=counties:San Francisco&lbl=San Francisco">
<area shape="poly" title="San Joaquin" coords="46,55,47,58,47,63,46,63,46,63,45,63,45,63,44,63,44,64,44,63,44,64,44,64,41,67,40,66,40,66,40,62,40,61,40,61,40,60,40,59,40,59,40,59,40,59,40,58,40,58,40,58,40,57,40,57,40,57,40,57,40,56,41,55,41,55,42,56,42,56,43,55,44,55,46,55,46,55" href="placefinder?clip=counties:San Joaquin&lbl=San Joaquin">
<area shape="poly" title="San Luis Obispo" coords="55,91,56,91,56,94,57,94,57,95,58,96,58,97,59,97,59,98,60,98,60,99,62,99,62,100,63,100,63,102,64,102,64,102,64,102,64,104,64,104,62,103,60,103,60,103,59,102,59,102,58,102,57,101,56,102,56,102,56,102,56,102,55,103,55,103,55,103,54,103,54,103,54,104,54,104,53,103,52,103,50,104,50,103,50,102,50,101,49,100,49,101,48,100,47,99,48,98,48,98,48,98,48,98,48,98,48,97,47,97,46,96,45,95,44,94,43,93,43,93,42,92,42,92,42,91,55,91" href="placefinder?clip=counties:San Luis Obispo&lbl=San Luis Obispo">
<area shape="poly" title="San Mateo" coords="29,63,31,63,34,66,32,67,32,68,33,69,32,69,33,69,33,70,32,70,32,71,31,71,31,72,31,72,30,71,30,70,30,70,30,68,29,66,29,66,29,66,29,66,29,65,29,65,29,64,29,63" href="placefinder?clip=counties:San Mateo&lbl=San Mateo">
<area shape="poly" title="Santa Barbara" coords="64,104,65,104,65,111,65,111,65,112,64,112,63,112,62,112,61,112,60,112,59,112,58,111,56,111,54,111,52,111,52,111,52,110,51,109,50,110,50,109,51,107,50,107,51,105,50,104,50,104,50,103,50,104,52,103,53,103,54,104,54,104,54,103,54,103,55,103,55,103,55,103,56,102,56,102,56,102,56,102,57,101,58,102,59,102,59,102,60,103,60,103,62,103,64,104,64,104" href="placefinder?clip=counties:Santa Barbara&lbl=Santa Barbara">
<area shape="poly" title="Santa Clara" coords="41,67,41,67,41,67,41,67,41,68,42,68,41,68,42,69,41,69,41,70,41,70,42,71,42,71,42,71,43,71,43,71,44,72,43,72,44,73,44,73,43,73,43,74,43,74,44,74,41,74,41,74,40,74,39,75,39,75,39,74,39,75,38,74,38,74,38,73,37,73,34,71,34,70,33,70,33,69,33,69,32,69,33,69,32,68,32,67,34,66,34,67,35,67,36,67,36,66,41,67" href="placefinder?clip=counties:Santa Clara&lbl=Santa Clara">
<area shape="poly" title="Santa Cruz" coords="33,69,33,70,34,71,34,71,37,73,38,73,38,74,38,74,39,75,39,74,39,75,39,75,39,75,39,75,38,75,38,75,38,75,38,75,38,75,37,75,37,76,36,74,35,74,35,74,34,74,34,74,34,74,33,74,32,73,31,72,31,71,32,71,32,70,33,70,33,69" href="placefinder?clip=counties:Santa Cruz&lbl=Santa Cruz">
<area shape="poly" title="Shasta" coords="42,12,43,12,43,16,43,16,43,23,40,23,40,23,39,23,37,23,37,23,35,23,33,24,34,24,33,24,33,24,33,24,32,24,31,24,30,24,28,25,27,24,27,24,26,24,25,25,25,25,24,25,24,24,24,24,24,23,25,23,26,22,27,22,27,21,28,21,28,20,27,20,28,20,27,19,28,19,28,19,28,18,28,18,28,17,29,16,29,16,29,15,30,15,30,14,30,14,31,13,31,12,30,12,42,12" href="placefinder?clip=counties:Shasta&lbl=Shasta">
<area shape="poly" title="Sierra" coords="58,34,58,38,52,38,52,38,52,37,51,37,49,38,47,38,46,39,46,37,46,36,46,36,47,35,47,34,47,34,48,33,48,33,49,33,49,34,49,34,50,34,51,34,58,34,58,34" href="placefinder?clip=counties:Sierra&lbl=Sierra">
<area shape="poly" title="Siskiyou" coords="19,0,32,0,42,0,42,12,30,12,30,12,30,11,30,11,30,10,31,10,30,9,29,10,29,10,29,10,28,11,28,11,27,11,27,12,26,12,26,12,25,12,25,12,25,13,26,14,26,14,26,15,25,14,24,15,24,14,23,13,22,13,21,12,21,12,20,12,20,11,20,10,20,10,20,10,19,9,18,9,18,8,18,7,18,7,17,6,17,6,17,6,17,5,17,5,17,5,18,4,17,3,18,3,17,2,18,2,18,1,18,1,19,1,19,1,19,0,19,0,19,0" href="placefinder?clip=counties:Siskiyou&lbl=Siskiyou">
<area shape="poly" title="Solano" coords="35,51,36,51,36,51,37,51,38,51,38,51,38,54,40,54,39,56,38,57,38,58,38,58,37,58,37,58,36,58,36,58,34,58,33,58,32,58,31,58,30,56,33,56,32,56,33,56,32,55,33,55,32,55,33,55,33,54,34,54,34,54,33,53,34,51,34,51,35,52,35,51" href="placefinder?clip=counties:Solano&lbl=Solano">
<area shape="poly" title="Sonoma" coords="26,46,27,47,27,48,28,48,28,49,28,50,28,50,29,51,29,52,29,52,29,52,30,54,30,54,30,55,31,55,31,56,31,56,31,56,30,56,31,58,29,57,28,56,28,56,26,56,24,54,24,54,24,54,23,54,23,54,23,54,23,54,23,54,23,54,22,54,23,53,22,52,20,50,19,48,18,47,18,47,18,47,19,47,19,47,22,47,22,46,23,46,23,46,26,46" href="placefinder?clip=counties:Sonoma&lbl=Sonoma">
<area shape="poly" title="Stanislaus" coords="47,58,50,61,53,64,46,68,46,68,47,68,46,68,47,69,44,72,43,71,43,71,42,71,42,71,42,71,41,70,41,70,41,69,42,69,41,68,42,68,41,68,41,67,41,67,41,67,44,64,44,64,44,63,44,64,44,63,45,63,45,63,46,63,46,63,47,63,47,58" href="placefinder?clip=counties:Stanislaus&lbl=Stanislaus">
<area shape="poly" title="Sutter" coords="39,40,39,40,39,40,39,40,39,41,40,41,39,41,40,41,40,43,40,43,40,43,40,44,40,44,40,45,40,45,42,44,42,45,41,45,41,48,41,48,41,48,40,48,40,48,39,47,39,48,39,48,39,48,39,48,39,48,39,48,39,47,38,47,38,46,38,46,38,46,37,46,37,46,37,45,37,45,37,44,37,44,37,44,37,43,36,43,36,42,36,42,36,42,36,42,36,41,36,41,36,40,39,40" href="placefinder?clip=counties:Sutter&lbl=Sutter">
<area shape="poly" title="Tehama" coords="41,23,41,23,41,23,41,24,41,24,42,25,43,25,43,25,43,26,43,27,42,27,42,27,42,27,42,28,40,28,40,29,40,29,39,30,39,30,38,31,35,31,35,32,35,32,25,32,25,32,25,31,25,30,25,29,25,29,25,28,25,28,25,27,24,27,25,27,25,26,24,25,25,25,25,25,26,24,27,24,27,24,28,25,30,24,31,24,32,24,33,24,33,24,33,24,34,24,33,24,35,23,37,23,37,23,39,23,40,23,40,23,41,23" href="placefinder?clip=counties:Tehama&lbl=Tehama">
<area shape="poly" title="Trinity" coords="30,12,31,12,31,13,30,14,30,14,30,15,29,15,29,16,29,16,28,17,28,18,28,18,28,19,28,19,27,19,28,20,27,20,28,20,28,21,27,21,27,22,26,22,25,23,24,23,24,24,24,24,24,25,25,26,25,27,24,27,25,27,25,28,25,28,25,28,25,29,25,30,18,29,18,18,18,17,18,17,18,17,18,16,18,16,18,16,18,16,18,15,18,15,18,15,19,16,20,15,20,15,20,14,20,14,20,13,20,13,20,13,20,13,20,12,20,12,20,12,21,12,21,12,22,13,23,13,24,14,24,15,25,14,26,15,26,14,26,14,25,13,25,12,25,12,26,12,26,12,27,12,27,11,28,11,28,11,29,10,29,10,29,10,30,9,31,10,30,10,30,11,30,11,30,12,30,12" href="placefinder?clip=counties:Trinity&lbl=Trinity">
<area shape="poly" title="Tulare" coords="77,77,78,78,77,78,78,78,77,79,78,79,78,79,78,80,78,80,79,81,79,81,79,81,79,82,80,82,80,82,80,82,80,83,80,84,80,85,81,86,81,86,81,87,81,87,81,88,82,88,81,89,82,89,82,90,82,90,82,91,63,91,63,84,64,84,64,82,63,82,63,81,63,81,63,81,64,80,66,80,66,79,70,79,70,77,75,77,77,77" href="placefinder?clip=counties:Tulare&lbl=Tulare">
<area shape="poly" title="Tuolumne" coords="58,53,59,54,60,53,61,53,61,54,62,54,62,55,62,55,62,55,62,56,62,56,62,56,63,56,63,57,63,57,64,57,64,57,64,58,64,58,65,57,66,58,66,59,66,59,66,60,67,61,67,61,67,61,67,62,67,62,66,63,66,62,65,62,65,61,65,61,65,61,64,61,64,61,63,60,63,60,63,61,63,61,62,61,62,62,62,62,60,63,59,62,59,63,59,63,57,62,56,62,56,62,56,62,55,62,55,63,55,63,55,63,54,63,54,64,54,64,54,64,53,64,54,64,53,64,50,61,51,61,52,60,52,60,52,59,52,59,52,59,53,59,53,58,56,54,58,53" href="placefinder?clip=counties:Tuolumne&lbl=Tuolumne">
<area shape="poly" title="Ventura" coords="71,106,74,113,75,114,74,114,74,115,73,115,71,116,71,117,69,116,69,116,68,115,67,115,67,115,67,115,67,115,67,115,67,114,67,114,67,114,67,114,64,112,65,112,65,111,65,111,65,104,65,104,65,105,67,105,67,105,67,105,67,106,70,106,70,106,71,106" href="placefinder?clip=counties:Ventura&lbl=Ventura">
<area shape="poly" title="Yolo" coords="37,45,37,46,37,46,37,46,38,46,38,46,38,46,38,47,39,47,39,48,39,48,39,48,39,48,39,48,39,47,40,48,40,48,39,49,40,49,40,49,40,50,41,50,40,51,41,51,40,52,40,52,41,52,41,52,40,53,41,53,40,54,40,54,40,54,38,54,38,51,38,51,37,51,37,51,36,51,35,52,34,51,34,51,33,50,33,50,33,50,33,49,32,49,32,46,31,46,30,45,30,45,37,45" href="placefinder?clip=counties:Yolo&lbl=Yolo">
<area shape="poly" title="Yuba" coords="46,36,47,35,46,36,46,36,46,37,46,39,45,39,45,39,44,39,44,40,44,40,44,41,43,41,43,44,43,44,42,44,40,45,40,45,40,44,40,44,40,44,40,43,40,43,40,43,40,41,39,41,40,41,39,41,40,40,40,40,42,39,43,38,43,38,43,37,43,37,45,37,45,36,45,36,45,36,46,36,46,36" href="placefinder?clip=counties:Yuba&lbl=Yuba">
</MAP>
	   ];
}

1
