#!/usr/bin/perl
##
## Return CEIC dataset Obj information in JSON format.
##
use CGI qw/:standard/;
use DBI;
use lib '/usr/local/share/ceic/perllib';
use CERES::CEIC::CalEdlnObj;

print header;
sanitize;

my $id = param('id');
my $src = param('src');
my $name = param('name');
my $mode = param('mode');
my $callback = param('callback');
my $st,$sql,$cnt,$where,$jsonText,$ds,$metadataLink,$linkTerm,$term,@theme,@org;

$id =~ s/\*/%/g;
$id =~ s/\?/_/g;

if ($src eq 'place') {
    $html = qq[<html><body><table cellspacing="30"><tr><td valign="top">];
    $html .= qq[<table>
		<tr><td><a href="#" onClick="ceicQuery('clip','$id','gis')">GIS Datasets</a></td><tr>
		<tr><td><a href="#" onClick="ceicQuery('clip','$id','database')">Databases</a></td><tr>
		<tr><td><a href="#" onClick="ceicQuery('clip','$id','project')">Projects</a></td><tr>
		<tr><td><a href="#" onClick="ceicQuery('clip','$id','other')">Other Resources</a></td><tr>
		<tr><td>Organizations</td><tr>
		<tr><td>Related Areas</td><tr>
		</table>];
    $html .= qq[</td><td valign="top">];
    $html .= qq[<img src="http://atlas.ca.gov/cgi-bin/test/spotpoint?clip=$id&size=120">];
    $html .= qq[<img src="http://atlas.ca.gov/cgi-bin/test/spotshape?clip=$id&zoom=1&size=120">];
    $html .= qq[</td></tr></body></html>];
    print $html;
}
else {
   ## prepare the name query
   $dbh = DBI->connect("dbi:Pg:dbname=gforge");
   $dbh->do("SET search_path TO ceic, public");
   
   $ds = CERES::CEIC::CalEdlnObj->new(dbh => $dbh);
   if ($id) {
      $ds->load(id => $id);
   }
   elsif ($name || $src) {
      $ds->load(catName => $src, dsName => $name)
      }   

   ## Some botched fields contain "HASH..."
   foreach $key (keys %{$ds}) {
      $ds->{$key} = '' if ($ds->{$key} =~ /^HASH/);
   }

   my $desc = $ds->{abstract} . $ds->{purpose};
   $ds->{abstract} = substr($desc,0,256) . ' ...' if (length($desc) gt 256);
   $metadataLink = 'http://atlas.ca.gov/catalog/' . $ds->{nodeshortname} . '/' . $ds->{shortname} . '.html';
   $linkTerm = 'More Information';

   $ds->{title} =~ s/(['"])/\\$1/g;
   $ds->{abstract} =~ s/(['"])/\\$1/g;

   my @theme = @{$ds->{theme}};
   my @themeArray;
   my @org = @{$ds->{org}};
   my @orgArray;
   my @place = @{$ds->{place}};
   my @placeArray;
   my @content = @{$ds->{resourcetype}};
   my @contentArray;

   foreach $term (@theme) { push (@themeArray,$term->{content}) }
   my $themeStr = join(',',@themeArray);
   foreach $term (@org) { push (@orgArray,$term->{content}) }
   my $orgStr = join(',',@orgArray);
   foreach $term (@place) { push (@placeArray,$term->{content}) }
   my $placeStr = join(',',@placeArray);
   foreach $term (@content) { push (@contentArray,$term->{content}) }
   my $contentStr = join(',',@contentArray);


   if ($mode eq 'json') {
    my $jsonText = '';
    if ($callback) { $jsonText = qq/$callback(/ }
    $jsonText .=  qq/{"ResultSet":{"Result":{/;
    $jsonText .= qq/\"id\":\"$ds->{id}\",/;
    $jsonText .= qq/\"label\":\"$ds->{title}\",/;
    $jsonText .= qq/\"category\":\"$contentStr\",/;
    $jsonText .= qq/\"abstr\":\"$ds->{abstract}\",/;
    $jsonText .= qq/\"theme\":\"$themeStr\",/;
    $jsonText .= qq/\"org\":\"$orgStr\",/;
    $jsonText .= qq/\"place\":\"$placeStr\",/;
    $jsonText .= qq/\"src\":\"$ds->{nodeName}\",/;
    $jsonText .= qq/\"link_url\":\"$ds->{link_url}\",/;
    $jsonText .= qq/\"link_url_resp\":\"$ds->{link_url_resp}\",/;
    $jsonText .= qq/\"dist_url_resp\":\"$ds->{dist_url_resp}\",/;
    $jsonText .= qq/\"dist_url\":\"$ds->{dist_url}\",/;
    $jsonText .= qq/\"meta_url\":\"$metadataLink\",/;
    if ($metadataLink =~ /ceic\.resources\.ca\.gov/) {
       $htmlLink = $metadataLink;
       $htmlLink =~ s/\.xml/.html/i;
       $htmlLink =~ s/\/xml/\/catalog/i;
       $jsonText .= qq/\"html_url\":\"$htmlLink\",/
    }
    else {
       $jsonText .= qq/\"html_url\":\"$ds->{link_url}\",/
    }
    $jsonText .= qq/\"last_update\":\"$ds->{last_update}\",/;

    $jsonText =~ s/\,$//;
    $jsonText .= qq/}}}/;

    if ($callback) { $jsonText .= qq/)/ }
    print $jsonText;
  }
  else {
    $html = qq[<html><body>
               <p>Source:&nbsp;<b>$ds->{nodeName}</b><br>
               <p>Title:&nbsp;$ds->{title}<br>
               <p>Resource Type:&nbsp;$ds->{category}<br>
               <p>Abstract:<br>] . substr($ds->{abstract},0,200) . qq[...<br>
	       <p>Keywords:<br>
	       ];
     foreach $term (@theme) {
	 $html .= qq[<a href="#" onClick="MWJ_findObj('term').value = '$term->{content}';ceicQuery();">$term->{content}</a>&nbsp;|&nbsp;];
     }
     $html .= qq[<p>Organizations:<br>];
     foreach $term (@org) {
	 $html .= qq[<a href="#" onClick="MWJ_findObj('orgname').value = '$term->{content}';ceicQuery();">$term->{content}</a>&nbsp;|&nbsp;];
     }
     $html .= qq[<p>Places:<br>];
     foreach $term (@place) {
	 if ($term->{content} =~ /.*:.*/) {
	     $html .= qq[<b>$term->{content}&nbsp;|&nbsp;];
	 }
	 elsif (!($term->{content} =~ /bounding|california/i)) {
	     $html .= qq[<b>$term->{content}&nbsp;|&nbsp;];
	 }
     }
     $html .= qq[<p>$linkTerm:<br><a href="$metadataLink" target="blank">$metadataLink<a><br>] if ($metadataLink);

    if ($ds->{dist_url} =~ /http|ftp/) {
     $html .= qq[<p>Distribution Link:<br><a href="$ds->{dist_url}" target="blank">$ds->{dist_url}<a><br>] if ($ds->{dist_url});
 }
     $html .= qq[</body></html>];
     print $html;
}
}

