sub sanitize {
#### Input Sanitation ####################################
foreach $param (param()) {
    if (param($param)) {
    unless ( param($param) =~ /^[\w .!-?:\/]+$/ ) {
        print    "You entered an invalid character. " .
               "You may only enter letters, numbers, " .
               "underscores, spaces, periods, exclamation " .
               "points, question marks, and hyphens.";
        exit;
    }
}
}
##########################################################
}

sub testMirrors {
##    use Apache;
    use LWP::UserAgent;
    use HTTP::Status;
    use HTTP::Response;
    $ceres_code = shift;
    $ucdavis_code = shift;

 # Create a user agent object
    $ua = new LWP::UserAgent;
    $ua->agent("ceres-casil/0.1 " . $ua->agent);
    $ua->timeout(5);

    $host = $ENV{SERVER_NAME};
    $path = $ENV{REQUEST_URI};
    $ucdavis = "http://atlas.ca.gov/casil/";
    $ceres = "http://casil-mirror1.ceres.ca.gov/casil/";

 # Create a request
    $req = new HTTP::Request HEAD => "$ucdavis";
 # Pass request to the user agent and get a response back
    $res = $ua->request($req);
    $$ucdavis_code = $res->code;

 # Create a request
    $req = new HTTP::Request HEAD => "$ceres";
 # Pass request to the user agent and get a response back
    $res = $ua->request($req);
    $$ceres_code = $res->code;

}

sub clip2names {
    my $clip = shift;
#    $lbl = shift;

    my @clipparse = split(':',$clip);
    $coverage = lc($clipparse[0]);
    $name = lc($clipparse[1]);

    if ($coverage =~ /bioregion|calwater22|city2k/) {$auth = 'FRAP'}
    elsif ($coverage =~ /counties|ca_outline/) {$auth = 'CMCC'}
    elsif ($coverage =~ /geoname|gnis|calgnis/) {$auth = 'GNIS'}
    elsif ($coverage =~ /zip_codes/) {$auth = 'Census'}
    elsif ($coverage =~ /grid/) {$auth = 'DOQQ'}

    my ($sql,$dbh,$st);

    my %view;
    $view{counties} = 'county_view';
    $view{bioregions} = 'bioregion_view';
    $view{grid} = 'grid_view';
    $view{city2k} = 'city2k';
    $view{zip_codes} = 'zip_view';
    $view{geoname} = 'geoname';
    $view{gnis} = 'calgnis';
    $view{ca_outline} = 'ca_outline_view';
    if (length($name) eq 2) {
	$view{calwater22} = 'hr'
	}
    elsif (length($name) eq 5) {
	$view{calwater22} = 'hu'
	}
    elsif (length($name) eq 7) {
	$view{calwater22} = 'ha'
	}
    elsif (length($name) eq 8) {
	$view{calwater22} = 'hsa'
	}
    elsif (length($name) eq 10) {
	$view{calwater22} = 'spws'
	}
    elsif (length($name) eq 12) {
	$view{calwater22} = 'pws'
	}

    my $db = 'gis';
    if ($coverage =~  /counties|bioregions|zip_codes|ca_outline/) {
	$sql = qq/SELECT description FROM $view{$coverage} WHERE  upper(description) = upper('$name')/;
    }
    elsif ($coverage =~  /city2k/) {
	$sql = qq/SELECT name FROM $view{$coverage} WHERE  upper(name) = upper('$name')/;
    }
    elsif ($coverage eq 'calwater22') {
	$sql = qq/SELECT $view{calwater22}name FROM $view{calwater22} WHERE idnum = '$name'/;
    }
    elsif ($coverage eq 'geoname') {
	$sql = qq/SELECT name FROM geonamea WHERE gid = $clipparse[1]/;
    } 
    elsif ($coverage eq 'calgnis') {
	$sql = qq/SELECT name FROM geonamea WHERE gid = $clipparse[1]/;
    } 
    elsif ($coverage eq 'gnis') {
	$sql = qq/SELECT feature_name FROM calgnis WHERE feature_id = $clipparse[1]/;
    }
    elsif ($coverage eq 'grid') {
	$sql = qq/SELECT mapname FROM mapgrida WHERE upper(substr(ocode,2,7)) =  upper('$name')/;
##	$db = 'shapes';
    }
    
    my $dbh = DBI->connect("dbi:Pg:dbname=$db"); 
    my $st = $dbh->prepare($sql);
    $st->execute();
    my @row = $st->fetchrow_array();
    $st->finish;
    $dbh->disconnect;
    
    $lbl = $row[0];

}

sub display_location {

    my $clip = shift;
    my $lbl = shift;
    my $back = shift;
    my $linkto = shift;

    $linkto = 'geofinder' if (!($linkto));
    $lbl = $clip if (!($lbl));
    $size = 140;

##    $name = substr($name,0,5) if ($coverage eq 'Grid');

    if ($back) {
	print qq[<a href="geofinder?clip=$clip"><img height=$size width=$size src="/cgi-bin/api/spotshape?clip=$clip&amp;size=$size" alt="Location Map" border="0"></a><br><br>];    }
    else {
	print qq[<a href="$linkto?clip=$clip"><img height=$size width=$size src="/cgi-bin/api/spotshape?clip=$clip&amp;size=$size" alt="County Map" usemap="#locMap" border="0"></a><br><br>];
    }
 }

sub display_intersect {

    my $auth = shift;
    my $coverage = shift;
    my $name = shift;
    my $lbl = shift;
    my $size = shift;
    my $toShp = shift;
    my $mapName = shift;

    $linkto = 'geofinder' if (!($linkto));
    $mapName = 'shpMap' if (!($mapName));

    print qq[<img height=350 width=350 name="mainImage" src="/cgi-bin/api/showShpIntersect?auth=$auth&amp;coverage=$coverage&amp;name=$name&amp;toShp=$toShp" alt="Intersecting $toShp(s)" usemap="#$mapName"><br><br>];
    map_intersect($auth,$coverage,$name,$toShp,$size,$linkto);
}

sub display_quadMap {

    my $auth = shift;
    my $coverage = shift;
    my $name = shift;
    my $lbl = shift;
    my $mapName = shift;
    my $linkto = shift;

    $linkto = 'geofinder' if (!($linkto));

    $mapName = 'quadMap' if (!($mapName));
    my $quad = substr($name,0,5) if ($name);
    my $cell = $name if (length($name) eq 7);

    $size = 140;
    print qq[<img height=$size width=$size name="mainImage" src="/cgi-bin/api/spotquad?code=$quad&amp;cell=$cell&amp;spot=1&amp;zoom=1&amp;width=$size&amp;height=$size" alt="Quad Spotter" usemap="#$mapName"><br><br>];
    map_quad($quad,0,0,1,$size,$linkto);
}

sub display_general {

    my $clip = shift;
    my $lbl = shift;
    my $size = shift;
    my $ptBuffer = shift;
    my $linkto = shift;

    $linkto = 'geofinder' if (!($linkto));
    $ptBuffer = 1200 if (!($ptBuffer));

    $size = 350 if (!($size));    
    $lbl = $clip if (!($lbl));

    print qq[<img height=$size width=$size src="/cgi-bin/api/spotshape?clip=$clip&amp;zoom=1&amp;size=$size&amp;ptBuffer=$ptBuffer" alt="Location">];
}

sub display_quad {

    my $auth = shift;
    my $coverage = shift;
    my $name = shift;
    my $lbl = shift;
    my $mapName = shift;
    my $linkto = shift;

    $linkto = 'geofinder' if (!($linkto));

    $mapName = 'quadMap' if (!($mapName));
    $lbl = $name if (!($lbl));
    my ($dbh,$stath,$sql);

    $name =~ s/^o//;
    my $quad = substr($name,0,5) if ($name);
    my $cell = $name if (length($name) eq 7);

    my $size;

    $size = 350; 

    my (@row,@geom);

    if ($cell) {
	$size = 350; 
	print qq[<img height=$size width=$size name="mainImage" src="/cgi-bin/api/spotshape?auth=DOQQ&amp;coverage=Grid&amp;name=$cell&amp;zoom=1&amp;size=$size" alt="Quad Map"><br><br>];

    }
    elsif ($quad) {
	$size = 350; 
	print qq[<img height=$size width=$size name="mainImage" src="/cgi-bin/api/spotshape?auth=DOQQ&amp;coverage=Grid&amp;name=$quad&amp;zoom=1&amp;size=$size"  alt="Quad Map" usemap="#$mapName"><br><br>];
	map_quad($quad,0,0,1,$size,$linkto,$mapName);
    }
    else {
	$size = 350;
	print qq[<img height=$size width=$size name="mappedImage" src="/cgi-bin/api/spotquad" usemap="#cellpage"  alt="Quad Map" usemap="#degreeMap"><br><br>];
    }
}

sub display_ws {

    my $auth = shift;
    my $coverage = shift;
    my $idnum = shift;
    my $name = shift;
    my $linkto = shift;
    my $size = shift;

    $linkto = 'geofinder' if (!($linkto));

    my ($dbh,$stath,$sql);

my $mode = 'sr';
my $size = 350 if (!($size));
my $emb = 0;

my ($imgwidth, $imgheight);
if ($size) {$imgwidth = size; $imgheight = size}
else {$size = 350; $imgwidth = 350; $imgheight = 350}

$mode = 'sr' if (!($mode));
my (@row,@geom,$up1,$up2,$up3,$up4);

$dbh= DBI->connect("dbi:Pg:dbname=gis");

$idnum = 0 if (!($idnum));
if (length($idnum) < 2) {
    $names = qq/SELECT 'California' from hr WHERE idnum  = '01'/;
}
elsif (length($idnum) eq 2) {
    $names = qq/SELECT (hrname || ' (hr)') from hr WHERE hr.idnum = '$idnum'/;
}
elsif (length($idnum) eq 5) {
    $up1 = substr($idnum,0,2);
    $names = qq/SELECT (huname || ' (hu)'), (hrname || ' (hr)') from hu,hr WHERE hr.idnum = '$up1' and hu.idnum  = '$idnum'/;
}
elsif (length($idnum) eq 7) {
    $up1 = substr($idnum,0,5);
    $up2 = substr($idnum,0,2);
    $names = qq/SELECT (haname || ' (ha)'), (huname || ' (hu)'), (hrname || ' (hr)') from ha,hu,hr WHERE hr.idnum = '$up2' and hu.idnum = '$up1' and ha.idnum  = '$idnum'/;
}
elsif (length($idnum) eq 8) {
    $up1 = substr($idnum,0,7);
    $up2 = substr($idnum,0,5);
    $up3 = substr($idnum,0,2);
    $names = qq/SELECT (hsaname || ' (hsa)'), (haname || ' (ha)'), (huname || ' (hu)'), (hrname || ' (hr)') from hsa,ha,hu,hr WHERE hr.idnum = '$up3' and hu.idnum = '$up2' and ha.idnum  = '$up1' and hsa.idnum = '$idnum'/;
}
elsif (length($idnum) eq 10) {
    $up1 = substr($idnum,0,8);
    $up2 = substr($idnum,0,7);
    $up3 = substr($idnum,0,5);
    $up4 = substr($idnum,0,2);
    $names = qq/SELECT (spwsname || ' (spws)'), (hsaname || ' (hsa)'), (haname || ' (ha)'), (huname || ' (hu)'),(hrname || ' (hr)') from ha,hu,hr,hsa,spws WHERE hr.idnum = '$up4' and hu.idnum = '$up3' and ha.idnum  = '$up2' and hsa.idnum = '$up1' and spws.idnum = '$idnum'/;
}
elsif (length($idnum) > 10) {
    $up1 = substr($idnum,0,10);
    $up2 = substr($idnum,0,8);
    $up3 = substr($idnum,0,7);
    $up4 = substr($idnum,0,5);
    $up5 = substr($idnum,0,2);
    $names = qq/SELECT (pwsname || ' (pws)'), (spwsname || ' (spws)'), (hsaname || ' (hsa)'), (haname || ' (ha)'), (huname || ' (hu)'),(hrname || ' (hr)') from ha,hu,hr,hsa,spws,pws WHERE hr.idnum = '$up5' and hu.idnum = '$up4' and ha.idnum  = '$up3' and hsa.idnum = '$up2' and  spws.idnum = '$up1' and pws.idnum = '$idnum'/;
}

$nq = $dbh->prepare($names);
$nq->execute();
@nameArray = $nq->fetchrow_array;
    $nq->finish;
$name = $nameArray[0];

print qq[
<SCRIPT LANGUAGE="JavaScript"><!--

var base = './';
var mode = '$mode';
var script = '$linkto';
var idnum = '$idnum';
var name = '$name';


function navigate(_script,_mode,_idnum,_name,size) {
    if (_mode) { mode = _mode }
    if (_script) { script = _script }
    idnum = _idnum;
    if (_name) { name = _name }

    var url = base + script + '?' + 'auth=FRAP&amp;coverage=calwater22&amp;name=' + idnum + '&amp;lbl=' + name;
//alert(url);
    window.location = url
}

function change(_script,_mode,_idnum,name,size) {
    if (_mode != '') { mode = _mode }
    if (_script != '') { script = _script }
    if (_idnum != '') { idnum = _idnum }

    var url = '' + script + '?' + 'idnum=' + idnum + '&amp;mode=' + mode + '&amp;name=' + name + '&amp;height=' + size + '&amp;width=' + size;
        if (document.images)
	document.images['mainImage'].src = url;
}

//--></SCRIPT>
 ];


print qq[<table width="100%">];
print qq[<tr><th bgcolor="#6699CC" width=175 class="blueLink">Location</th><th bgcolor="#6699CC" class="blueLink">$nameArray[0]</th></tr>];

print qq[<tr><td valign="top" width=175>];
print qq[<table width="100%"><tr><td valign="top">];

## examine the name trail to backup the tree to the last region that is not subdivided. 
@nameBase = @nameArray;

## first remove the level designation e.g. "name (hsa)"
for $i (0..((@nameBase) -1)) { $nameBase[$i] =~ s/\(.+\)// }

print qq[<tr><td align="center" ><a href="#" class="blueLink" onClick="navigate('$linkto','','$up5')"><img src="/cgi-bin/api/spotshed?idnum=$up5&amp;zoom=idnum" alt="Watershed Map"><br>$nameArray[5]</a></td></tr>] if ($up5 > 0);

print qq[<tr><td align="center" ><a href="#" class="blueLink" onClick="navigate('$linkto','','$up4')"><img src="/cgi-bin/api/spotshed?idnum=$up4&amp;zoom=idnum" alt="Watershed Map"><br>$nameArray[4]</a></td></tr>] if (($up4 >0) && ($nameBase[4] ne $nameBase[3]));

print qq[<tr><td align="center" ><a href="#" class="blueLink" onClick="navigate('$linkto','','$up3')"><img src="/cgi-bin/api/spotshed?idnum=$up3&amp;zoom=idnum" alt="Watershed Map"><br>$nameArray[3]</a></td></tr>] if (($up3 > 0) && ($nameBase[3] ne $nameBase[2]));

print qq[<tr><td align="center" ><a href="#" class="blueLink" onClick="navigate('$linkto','','$up2')"><img src="/cgi-bin/api/spotshed?idnum=$up2&amp;zoom=idnum" alt="Watershed Map"><br>$nameArray[2]</a></td></tr>] if (($up2 >0) && ($nameBase[2] ne $nameBase[1]));

print qq[<tr><td align="center" ><a href="#" class="blueLink" onClick="navigate('$linkto','','$up1')"><img src="/cgi-bin/api/spotshed?idnum=$up1&amp;zoom=idnum" alt="Watershed Map"><br>$nameArray[1]</a></td></tr>] if (($up1 >0) && ($nameBase[1] ne $nameBase[0]));

print qq[<tr><td align="center" ><a href="#" class="blueLink" onClick="navigate('$linkto','','$up1')"><img src="/cgi-bin/api/spotshed?idnum=$idnum&amp;zoom=idnum" alt="Watershed Map"><br>$nameArray[0]</a></td></tr>];

if ($mode eq 'drg') {
  print qq[</table></td><td width=$size valign="top" align="center"><table><tr><td align="center" >];
  print qq[<img height=$size width=$size border="0" name="mainImage" src="/cgi-bin/api/showshed?idnum=$idnum&zoom=idnum&amp;width=$size&amp;height=$size&amp;mode=drg" alt="Watershed Map">];
}
elsif  ($mode eq 'ls'){
  print qq[</table></td><td width=$size valign="top" align="center"><table><tr><td align="center" >];
  print qq[<img height=$size width=$size border="0" name="mainImage" src="/cgi-bin/api/showshed?idnum=$idnum&amp;zoom=idnum&amp;width=$size&amp;height=$size&amp;mode=ls" alt="Watershed Map">];
}
else {
  print qq[</table></td><td width=$size valign="top" align="center"><table><tr><td align="center" >];
  print qq[<img height=$size width=$size border="0" name="mainImage" src="/cgi-bin/api/showshed?idnum=$idnum&amp;zoom=idnum&amp;width=$size&amp;height=$size&amp;mode=sr" usemap="#shedMap" alt="Watershed Map">];
}



print qq[<br>];
#print qq[<p>calwater22:$idnum];
print qq[</td>];
print qq[</tr>];

print qq[</td></tr>];
print qq[</tr></td></tr>];

print qq[</table>];
map_shed($idnum,$size) if (length($idnum) < 12);
print qq[<!--name:$nameArray[0]-->];
$nq->finish;
$dbh->disconnect;

}

sub map_shed {

    my $idnum = shift;
    my $size = shift;
    my $linkto = shift;
    my $mode = shift;
    my ($name,$points,$coords,$dbh,$st);

    $linkto = 'geofinder' if (!($linkto));

    $size = 350 if (!($size));

$idnum = 0 if (!($idnum));
if (length($idnum) < 2) {
    $sql = qq/SELECT idnum,hrname from hr order by hrname/;
    $ext_sql = qq/select xmax(extent(the_geom)), xmin(extent(the_geom)), ymax(extent(the_geom)), ymin(extent(the_geom)) FROM hr/;
}
elsif (length($idnum) eq 2) {
    $sql = qq/SELECT idnum,(huname || ' (hu)') from hu WHERE idnum  like '$idnum%' order by huname/;
#    $ext_sql = qq/select xmax(extent(the_geom)), xmin(extent(the_geom)), ymax(extent(the_geom)), ymin(extent(the_geom)) FROM hr WHERE idnum = '$idnum'/;
    $ext_sql = qq/select xmax(extent(the_geom)), xmin(extent(the_geom)), ymax(extent(the_geom)), ymin(extent(the_geom)) FROM hr WHERE idnum = '$idnum'/;
}
elsif (length($idnum) eq 5) {
    $sql = qq/SELECT idnum,(haname || ' (ha)') from ha WHERE idnum  like '$idnum%' order by haname/;
    $ext_sql = qq/select xmax(extent(the_geom)), xmin(extent(the_geom)), ymax(extent(the_geom)), ymin(extent(the_geom)) FROM hu WHERE idnum = '$idnum'/;
}
elsif (length($idnum) eq 7) {
    $sql = qq/SELECT idnum,(hsaname || ' (hsa)') from hsa WHERE idnum  like '$idnum%' order by hsaname/;
    $ext_sql = qq/select xmax(extent(the_geom)), xmin(extent(the_geom)), ymax(extent(the_geom)), ymin(extent(the_geom)) FROM ha WHERE idnum = '$idnum'/;
}
elsif (length($idnum) eq 8) {
    $sql = qq/SELECT idnum,(spwsname || ' (spws)') from spws WHERE idnum  like '$idnum%' order by spwsname/;
    $ext_sql = qq/select xmax(extent(the_geom)), xmin(extent(the_geom)), ymax(extent(the_geom)), ymin(extent(the_geom)) FROM hsa WHERE idnum = '$idnum'/;
}
elsif (length($idnum) eq 10) {
    $sql = qq/SELECT idnum,(pwsname || ' (pws)') from pws WHERE idnum  like '$idnum%' order by pwsname/;
    $ext_sql = qq/select xmax(extent(the_geom)), xmin(extent(the_geom)), ymax(extent(the_geom)), ymin(extent(the_geom)) FROM spws WHERE idnum = '$idnum'/;
}
elsif (length($idnum) > 10) {
    $sql = qq/SELECT idnum,(pwsname || ' (pws)') from pws WHERE idnum  like '$up1%' order by pwsname/;
    $ext_sql = qq/select xmax(extent(the_geom)), xmin(extent(the_geom)), ymax(extent(the_geom)), ymin(extent(the_geom)) FROM spws WHERE idnum = '$idnum'/;
}

    print qq[<MAP NAME="shedMap">];

    $dbh= DBI->connect("dbi:Pg:dbname=gis");

    $st = $dbh->prepare($ext_sql);
    $st->execute();

    @ext = $st->fetchrow_array();
    $st->finish;

    $maxx = $ext[0];
    $minx = $ext[1];
    $maxy = $ext[2];
    $miny = $ext[3];
    
   # remove pesky quotes added to neg numbers 
    $maxx =~ s/\'//g;
    $minx =~ s/\'//g;
    $maxy =~ s/\'//g;
    $miny =~ s/\'//g;

    $st = $dbh->prepare($sql);
    $st->execute();
    $dat = $st->fetchall_arrayref();    
    $st->finish;

    foreach $row (@$dat) {
	$idnum = $row->[0];
	$name = $row->[1];
	$points = getPolygon('FRAP','calwater22',$idnum,3309);
	$points =~ s/[\(|\)|MULTIPOLYGON]//g;
	$points =~ s/\s/,/g;
	@pt = split(',',$points);
        $coords = create_imagemap($maxx,$maxy,$minx,$miny,$size);
	if ($linkto eq 'calw_tree') {
	    $area = qq[<area shape=POLY title="$name" coords="$coords" href="$linkto?idnum=$idnum&name=$lbl&mode=$mode">\n]}
	elsif ($linkto eq 'calwBrowse') {
	    $area = qq[<area shape=POLY title="$name" coords="$coords" href="#" onClick="navigate('calwBrowse','','$idnum')">\n]}
	else {
	    $area = qq[<area shape=POLY title="$name" coords="$coords" href="$linkto?auth=FRAP&coverage=calwater22&name=$idnum&lbl=$name" alt="$name">\n]}

	print "$area";
    }

    print qq[</MAP>];
    $dbh->disconnect();
}

sub getExtent {

    my $auth = shift;
    my $coverage = lc(shift);
    my $name = shift;
    my $srid = shift;
    my $ptBuffer = shift;

    $ptBuffer = 0 if (!($ptBuffer));

    my ($sql,$dbh,$st);

    my %view;
    $view{counties} = 'county_view';
    $view{bioregions} = 'bioregion_view';
    $view{grid} = 'grid_view';
    $view{city2k} = 'city2k';
    $view{zip_codes} = 'zip_view';
    $view{geoname} = 'geoname';
    $view{gnis} = 'calgnis';
    if (length($name) eq 2) {
	$view{calwater22} = 'hr'
	}
    elsif (length($name) eq 5) {
	$view{calwater22} = 'hu'
	}
    elsif (length($name) eq 7) {
	$view{calwater22} = 'ha'
	}
    elsif (length($name) eq 8) {
	$view{calwater22} = 'hsa'
	}
    elsif (length($name) eq 10) {
	$view{calwater22} = 'spws'
	}
    elsif (length($name) eq 12) {
	$view{calwater22} = 'pws'
	}

    if ($coverage =~  /counties|bioregions|zip_codes|grid/) {
	$sql = qq/SELECT xmax(extent(transform(footprint,$srid))), xmin(extent(transform(footprint,$srid))), ymax(extent(transform(footprint,$srid))), ymin(extent(transform(footprint,$srid))) FROM $view{$coverage} WHERE  upper(description) like upper('$name')/;
    }
    elsif ($coverage eq 'city2k') {
	$sql = qq/SELECT xmax(extent(transform(the_geom,$srid))), xmin(extent(transform(the_geom,$srid))), ymax(extent(transform(the_geom,$srid))), ymin(extent(transform(the_geom,$srid))) FROM $view{$coverage} WHERE upper(name) like upper('$name')/;
    }
    elsif ($coverage eq 'calwater22') {
#	$name = substr($name,0,5);
	$sql = qq/SELECT xmax(extent(transform(the_geom,$srid))), xmin(extent(transform(the_geom,$srid))), ymax(extent(transform(the_geom,$srid))), ymin(extent(transform(the_geom,$srid))) FROM $view{calwater22} WHERE idnum like '$name%'/;
    }
    elsif ($coverage eq 'geoname') {
	$sql = qq/SELECT xmax(extent(transform(the_geom,$srid))) + $ptBuffer, xmin(extent(transform(the_geom,$srid))) - $ptBuffer, ymax(extent(transform(the_geom,$srid))) + $ptBuffer, ymin(extent(transform(the_geom,$srid))) - $ptBuffer FROM geonamea WHERE gid = $name/;
    }
    elsif ($coverage =~ /gnis/) {
	$sql = qq/SELECT xmax(extent(transform(geom,$srid))) + $ptBuffer, xmin(extent(transform(geom,$srid))) - $ptBuffer, ymax(extent(transform(geom,$srid))) + $ptBuffer, ymin(extent(transform(geom,$srid))) - $ptBuffer FROM calgnis WHERE feature_id = $name/;
    }
     elsif ($coverage eq 'ca_outline') {
	$sql = qq/SELECT xmax(extent(transform(simplify(footprint,.2),$srid))), xmin(extent(transform(simplify(footprint,.2),$srid))), ymax(extent(transform(simplify(footprint,.2),$srid))), ymin(extent(transform(simplify(footprint,.2),$srid))) FROM ca_outline_view/;
    }
    
    $dbh = DBI->connect("dbi:Pg:dbname=gis"); 
    $st = $dbh->prepare($sql);
    $st->execute();
    @ext = $st->fetchrow_array();
    $st->finish;
    $dbh->disconnect;
    
    return \@ext;

}

sub getPolygon {

    my $auth = shift;
    my $coverage = shift;
    my $name = shift;
    my $srid = shift;
    $area = shift;

    my ($sql,$poly);

    my $dbh= DBI->connect("dbi:Pg:dbname=gis");

    if (($auth && $coverage && $name) || ($coverage eq 'ca_outline')){
	if (($auth eq 'FRAP') && ($coverage eq 'city2k')) {
	    $sql = qq/select AsText(MemGeomUnion(transform(simplify(the_geom,15),$srid))),Area(MemGeomUnion(transform(simplify(the_geom,15),$srid))) from city2k where upper(trim(name)) = upper('$name')/;
	}
	elsif (($auth eq 'FRAP') && ($coverage eq 'bioregions')) {
	    $sql = qq/select AsText(transform(simplify(footprint,.1),$srid)),Area(transform(simplify(footprint,.1),$srid)) from bioregion_view where upper(trim(description)) = upper('$name')/;
	}
	elsif (($auth eq 'GNIS') && ($coverage eq 'geoname')) {
	    $sql = qq/select AsText(envelope(expand(box3d(envelope(transform(the_geom,$srid))),0.02))),Area(envelope(expand(box3d(envelope(transform(the_geom,$srid))),0.015))) from geonamea where gid = $name/;
	}
	elsif (($auth eq 'GNIS') && ($coverage eq 'gnis')) {
	    $sql = qq/select AsText(envelope(expand(box3d(envelope(transform(geom,$srid))),0.02))),Area(envelope(expand(box3d(envelope(transform(geom,$srid))),0.015))) from calgnis where feature_id = $name/;
	}
	elsif (($auth eq 'FRAP') && ($coverage eq 'calwater22')) {
	    if (length($name) eq 2) {
		$sql = qq/select AsText(transform(simplify(the_geom,500),$srid)),Area(transform(simplify(the_geom,500),$srid)) from hr where idnum = '$name'/;
	    }
	    elsif (length($name) eq 5) {
                $sql = qq/select AsText(transform(simplify(the_geom,200),$srid)),Area(transform(simplify(the_geom,200),$srid)) from hu where idnum = '$name'/;
            }
	    elsif (length($name) eq 7) {
                $sql = qq/select AsText(transform(simplify(the_geom,15),$srid)),Area(transform(simplify(the_geom,15),$srid)) from ha where idnum = '$name'/;
            }
	    elsif (length($name) eq 8) {
                $sql = qq/select AsText(transform(simplify(the_geom,15),$srid)),Area(transform(simplify(the_geom,15),$srid)) from hsa where idnum = '$name'/;
            }
	    elsif (length($name) eq 10) {
                $sql = qq/select AsText(transform(simplify(the_geom,15),$srid)),Area(transform(simplify(the_geom,15),$srid)) from spws where idnum = '$name'/;
            }
	    elsif (length($name) gt 10) {
                $sql = qq/select AsText(transform(simplify(the_geom,15),$srid)),Area(transform(simplify(the_geom,15),$srid)) from pws where idnum = '$name'/;
            }

	}
	elsif (($auth eq 'CMCC') && ($coverage eq 'counties')) {
	    $sql = qq/select AsText(transform(simplify(footprint,.1),$srid)),Area(transform(simplify(footprint,.1),$srid)) from county_view where upper(trim(description)) = upper('$name')/;
	}
	elsif (($auth eq 'Census') && ($coverage eq 'zip_codes')) {
	    $sql = qq/select AsText(MemGeomUnion(transform(simplify(footprint,.0025),$srid))),Area(MemGeomUnion(transform(simplify(footprint,.0025),$srid))) from zip_view where upper(trim(description)) = upper('$name')/;
	}
	elsif (($auth eq 'DOQQ') && (lc($coverage) eq 'grid')) {
	    $sql = qq/select AsText(transform(footprint,$srid)),Area(transform(footprint,$srid)) from grid_view where upper(trim(description)) = upper('$name')/;
	}
	elsif (($auth eq 'CMCC') && (lc($coverage) eq 'ca_outline')) {
	    $sql = qq/select AsText(transform(simplify(footprint,.5),$srid)),Area(transform(simplify(footprint,.1),$srid)) from ca_outline_view/;
	}


	$st = $dbh->prepare($sql) or die $sql;;
	$st->execute();
        @row = $st->fetchrow_array; 
        $st->finish;
	$poly = $row[0];
	$$area = $row[1];
    }

    $dbh->disconnect;
    return $poly
}

sub getCentroid {

    my $auth = shift;
    my $coverage = shift;
    my $name = shift;
    $area = shift;

    my ($sql,$lat_long);

    my $dbh= DBI->connect("dbi:Pg:dbname=gis");

	if (($auth eq 'FRAP') && ($coverage eq 'city2k')) {
	    $sql = qq/select centroid(MemGeomUnion(the_geom)), xmax(extent(transform(the_geom,3310))) - xmin(extent(transform(the_geom,3310))) from city2k where upper(trim(name)) = upper('$name')/;
	}
	elsif (($auth eq 'FRAP') && ($coverage eq 'bioregions')) {
	    $sql = qq/select centroid(extent(footprint)), xmax(extent(transform(footprint,3310))) - xmin(extent(transform(footprint,3310))) from bioregion_view where upper(trim(description)) = upper('$name')/;
	}
	elsif (($auth eq 'GNIS') && ($coverage eq 'geoname')) {
	    $sql = qq/select centroid(extent(transform(the_geom,4269))), xmax(extent(transform(the_geom,3310))) - xmin(extent(transform(the_geom,3310))) from geonamea where gid = $name/;
	}
	elsif (($auth eq 'FRAP') && ($coverage eq 'calwater22')) {
	    if (length($name) eq 2) {
		$sql = qq/select centroid(extent(transform(the_geom,4269))), xmax(extent(transform(the_geom,3310))) - xmin(extent(transform(the_geom,3310))) from hr where idnum = '$name'/;
	    }
	    elsif (length($name) eq 5) {
                $sql = qq/select centroid(extent(transform(the_geom,4269))), xmax(extent(transform(the_geom,3310))) - xmin(extent(transform(the_geom,3310))) from hu where idnum = '$name'/;
            }
	    elsif (length($name) eq 7) {
                $sql = qq/select centroid(extent(transform(the_geom,4269))), xmax(extent(transform(the_geom,3310))) - xmin(extent(transform(the_geom,3310))) from ha where idnum = '$name'/;
            }
	    elsif (length($name) eq 8) {
                $sql = qq/select centroid(extent(transform(the_geom,4269))), xmax(extent(transform(the_geom,3310))) - xmin(extent(transform(the_geom,3310))) from hsa where idnum = '$name'/;
            }
	    elsif (length($name) eq 10) {
                $sql = qq/select centroid(extent(transform(the_geom,4269))), xmax(extent(transform(the_geom,3310))) - xmin(extent(transform(the_geom,3310))) from spws where idnum = '$name'/;
            }
	    elsif (length($name) gt 10) {
                $sql = qq/select centroid(extent(transform(the_geom,4269))), xmax(extent(transform(the_geom,3310))) - xmin(extent(transform(the_geom,3310))) from pws where idnum = '$name'/;
            }

	}
	elsif (($auth eq 'CMCC') && ($coverage eq 'counties')) {
	    $sql = qq/select centroid(extent(footprint)), xmax(extent(transform(footprint,3310))) - xmin(extent(transform(footprint,3310))) from county_view where upper(trim(description)) = upper('$name')/;
	}
	elsif (($auth eq 'Census') && ($coverage eq 'zip_codes')) {
	    $sql = qq/select centroid(extent(footprint)), xmax(extent(transform(footprint,3310))) - xmin(extent(transform(footprint,3310))) from zip_view where upper(trim(description)) = upper('$name')/;
	}
	elsif (($auth eq 'DOQQ') && (lc($coverage) eq 'grid')) {
	    $sql = qq/select centroid(extent(footprint)), xmax(extent(transform(footprint,3310))) - xmin(extent(transform(footprint,3310))) from grid_view where upper(trim(description)) = upper('$name')/;
	}
	elsif (($auth eq 'CMCC') && (lc($coverage) eq 'ca_outline')) {
	    $sql = qq/select centroid(extent(footprint)), xmax(extent(transform(footprint,3310))) - xmin(extent(transform(footprint,3310))) from ca_outline_view/;
	}
	else  {
	    $sql = qq/select centroid(extent(footprint)), xmax(extent(transform(footprint,3310))) - xmin(extent(transform(footprint,3310))) from ca_outline_view/;
	}


	$st = $dbh->prepare($sql);
	$st->execute();
        @row = $st->fetchrow_array; 
        $st->finish;
	$lat_long = $row[0];
	$$area = $row[1];

    $dbh->disconnect;
    return $lat_long
}

sub gaz_intersect {

    my $auth = shift;
    my $coverage = shift;
    my $name = shift;
    my $lbl = shift;

    my ($sql,$poly,$dbh);
    my (@views,@matchList);
    my $area;

    $lbl = $name if (!($lbl));
    my $dbh = DBI->connect("dbi:Pg:dbname=gis");
    my $dbh3 = DBI->connect("dbi:Pg:dbname=metadata");

    $poly = getPolygon($auth,$coverage,$name,3310,\$area);

    $sql = qq[select coverage, description from gazetteer where (transform(simplify(footprint,.2),3310) && GeometryFromText('$poly',3310)) and (coverage IN ('zip_codes','city2k','counties')) and intersects(transform(footprint,3310),GeometryFromText('$poly',3310)) and area(difference(transform(footprint,3310),GeometryFromText('$poly',3310))) < (area(GeometryFromText('$poly',3310)) * .8) and description <> '' order by coverage];
#    $sql = qq[select coverage, description from gazetteer where (simplify(footprint,.2) && GeometryFromText('$poly',4269)) and (coverage IN ('zip_codes','city2k','calwater22','counties')) and intersects(footprint,GeometryFromText('$poly',4269)) and description <> '' order by coverage];

    my $nq = $dbh->prepare($sql);
    $nq->execute();

    my ($shpname,$coverage);
    while (@row = $nq->fetchrow_array) {
      $coverage = $row[0];
      $shpname = $row[1];
      push(@matchList,"$coverage:$shpname");
   }

    $dbh->disconnect();
    $dbh3->disconnect();

    return \@matchList;
}

sub shp_intersect {

    my $auth = shift;
    my $coverage = shift;
    my $name = shift;
    my $lbl = shift;
    my $linkto = shift;
    my $toShp = shift;
    my $mode = shift;
    
    $linkto = 'geofinder' if (!($linkto));
    # if called with a toShp only return a list otherwise print the results
    my $printing = 1 unless ($toShp || ($mode eq 'quiet') );
    my ($sql,$poly,$dbh);
    my (@views,@matchList);
    my $area;

    $lbl = $name if (!($lbl));
    my $dbh = DBI->connect("dbi:Pg:dbname=gis");
##    my $dbh3 = DBI->connect("dbi:Pg:dbname=metadata");

    $poly = getPolygon($auth,$coverage,$name,4269,\$area);

    if ($toShp) {
	# only map to the requested shp type.
	@views = ($toShp)
    }
    elsif ($area lt .04){
	@views = ('bioregion_view','county_view','city2k','hu_view','grid_view','zip_view','geoname')
	}
    else {
	@views = ('bioregion_view','county_view','city2k','hu_view','grid_view')
	}

    print qq[<table border=0 width="100%"><tr>] if ($printing);
    foreach $view (@views) {
	if ($view eq 'bioregion_view') {
	    $to_auth = 'FRAP';
	    $to_coverage = 'bioregions';
	    $to_lbl = 'California Bioregions';
	}
	elsif ($view eq 'city2k') {
	    $to_auth = 'FRAP';
	    $to_coverage = 'city2k';
	    $to_lbl = 'Cities';	    
	}
	elsif ($view eq 'county_view') {
	    $to_auth = 'CMCC';
	    $to_coverage = 'counties';
	    $to_lbl = 'California Counties';
	}
	elsif ($view eq 'grid_view') {
	    $to_auth = 'DOQQ';
	    $to_coverage = 'Grid';
	    $to_lbl = 'Degree Grid/USGS 7.5 Minute Quads';
	}
	elsif ($view eq 'hu_view') {
	    $to_auth = 'FRAP';
	    $to_coverage = 'calwater22';
	    $to_lbl = 'Watersheds (CalWater Hydrologic Units)';
	}
	elsif ($view eq 'zip_view') {
	    $to_auth = 'Census';
	    $to_coverage = 'zip_codes';
	    $to_lbl = 'Zip Codes';
	}
	elsif ($view eq 'geoname') {
	    $to_auth = 'GNIS';
	    $to_coverage = 'geoname';
	    $to_lbl = 'Other Places (GNIS)';
	}
	elsif ($view eq 'spatialdomain') {
	    $to_auth = 'CERES';
	    $to_coverage = 'spatialdomain';
	    $to_lbl = 'CEIC Defined Regions';
	}

	print qq[<tr><th bgcolor="#6699CC" class="blueLink" >$to_lbl</th></tr><tr><td width="350" align="left">]  if ($printing);

	if ($view eq 'grid_view') {
	    if ($area lt .2) { 
 		$res = 7;
		$sql = qq/SELECT 'x', 'o' || description FROM $view WHERE length(trim(description)) = $res and (footprint && GeometryFromText('$poly',4269)) and intersects(footprint,GeometryFromText('$poly',4269)) order by description/;
		$nq = $dbh->prepare($sql);
		$nq->execute();
		while (@row = $nq->fetchrow_array) { push(@ocode,$row[1]) }
		$ocodeStr = join("','",@ocode);
		$sql = qq/SELECT substr(ocode,2,7),mapname || ' (' || substr(ocode,2,7) || ')' FROM mapgrida WHERE ocode IN ('$ocodeStr') order by mapname/;
		$nq = $dbh->prepare($sql);
		$nq->execute();
	    }
	    else { 
 		$res = 5;
		$sql = qq/SELECT distinct description,description FROM $view WHERE length(trim(description)) = $res and (footprint && GeometryFromText('$poly',4269)) and intersects(footprint,GeometryFromText('$poly',4269)) order by description/;
		$nq = $dbh->prepare($sql);
		$nq->execute();
	    }
	}
	elsif ($view eq 'hu_view') {
	    if ($area < .1 || ($toShp)){ 
		$sql = qq/SELECT distinct idnum,pwsname FROM pws WHERE (the_geom && transform(GeometryFromText('$poly',4269),3309)) and intersects(the_geom,transform(GeometryFromText('$poly',4269),3309)) order by pwsname/;
	    }
	    elsif ($area < .2 || ($toShp)){ 
		$sql = qq/SELECT distinct idnum,haname FROM ha WHERE (the_geom && transform(GeometryFromText('$poly',4269),3309)) and intersects(the_geom,transform(GeometryFromText('$poly',4269),3309)) order by haname/;
	    }
	    elsif ($area < .3 || ($toShp)){ 
		$sql = qq/SELECT distinct idnum,huname FROM hu WHERE (the_geom && transform(GeometryFromText('$poly',4269),3309)) and intersects(the_geom,transform(GeometryFromText('$poly',4269),3309)) order by huname/;
	    }
	    else {
		$sql = qq/SELECT distinct idnum,hrname FROM hr WHERE (the_geom && transform(GeometryFromText('$poly',4269),3309)) and intersects(the_geom,transform(GeometryFromText('$poly',4269),3309)) order by hrname/;
	    }
	    $nq = $dbh->prepare($sql);
	    $nq->execute();
	}
	elsif ($view eq 'city2k') {
	    $sql = qq/SELECT distinct name,name FROM $view WHERE (the_geom && transform(GeometryFromText('$poly',4269),3310)) and intersects(the_geom,transform(GeometryFromText('$poly',4269),3310)) and upper(name) not like '%(%)' order by name/;
	    $nq = $dbh->prepare($sql);
	    $nq->execute();
	}
	elsif ($view eq 'geoname') {
#	    $sql = qq/SELECT gid,name || ' (' || type || ')'  FROM geonamea WHERE type not IN ('church','school','building','po','hospital','tower') and (the_geom && transform(GeometryFromText('$poly',4269),3309)) and intersects(the_geom,transform(GeometryFromText('$poly',4269),3309)) order by type,name/;
	    $sql = qq/SELECT gid,name || ' (' || type || ')'  FROM geonamea WHERE (the_geom && transform(GeometryFromText('$poly',4269),3309)) and intersects(the_geom,transform(GeometryFromText('$poly',4269),3309)) order by type,name/;
	    $nq = $dbh->prepare($sql);
	    $nq->execute();
	}
	else {
#	    $sql = qq/SELECT distinct description,description FROM $view WHERE (footprint && GeometryFromText('$poly',4269)) and intersects(footprint,GeometryFromText('$poly',4269)) and (area(difference(footprint,GeometryFromText('$poly',4269))) < 1) order by description/;
	    $sql = qq/SELECT distinct description,description FROM $view WHERE (footprint && GeometryFromText('$poly',4269)) and intersects(footprint,GeometryFromText('$poly',4269)) order by description/;
	    $nq = $dbh->prepare($sql);
	    $nq->execute();
	}


    my ($shpname,$shpname_);
    while (@row = $nq->fetchrow_array) {
      $shpname = $row[0];
      $lbl = $row[1];
      $shpname_ = $shpname;
      $shpname_ =~ s/\s/_/g;
#      print qq[<a  href="$linkto?auth=$to_auth&amp;coverage=$to_coverage&amp;name=$shpname&amp;lbl=$lbl&amp;size=350&amp;ptBuffer=800">$lbl</a><br>] if ($printing);
      print qq[<a  href="$linkto?auth=$to_auth&amp;coverage=$to_coverage&amp;name=$shpname&amp;lbl=$lbl&amp;ptBuffer=800" class="blueLink">$lbl</a><br>] if ($printing);

      push(@matchList,"$lbl|$to_coverage|$to_coverage:$shpname"
);
   }
    print qq[</td></tr>] if ($printing);
}
    print qq[</table>] if ($printing);
    $dbh->disconnect();

    return \@matchList;
}

sub cnddb_query {
    @args = ("wget http://maps.dfg.ca.gov/webservices/cnddbquad/quad.asmx/GetQuadInfo?quadcode=3811911", $_);
    system qq[(@args)];
    print qq[$_];
}

sub address2ll {
 
    my $adr = shift;
    my ($ora) = Geo::Coder::US->geocode($adr);
    my @matches = ($ora->{long}, $ora->{lat});
    return @matches;
}

sub address2ll_old {

    my $adr = shift;
    my $response = `echo $adr | address2ll`;
    if ($response !~ /success/i) {
	return 0
	}
    else {
	$response =~ s/.*At/At/s;
	$response =~ s/End.*//is;
	$response =~ s/.*@//s;
	$response =~ s/\s//g;
	return split(',',$response);
    }
    
}

sub ceic_intersect {

    my $auth = shift;
    my $coverage = shift;
    my $name = shift;
    my $lbl = shift;
    
    $linkto = 'geofinder' if (!($linkto));
    # if called with a toShp only return a list otherwise print the results
    my $printing = 1 if (!($toShp));
    my ($sql,$poly,$dbh);
    my (@views,@matchList);
    my $area;

    $lbl = $name if (!($lbl));
    my $dbh3 = DBI->connect("dbi:Pg:dbname=metadata");

    $poly = getPolygon($auth,$coverage,$name,4269,\$area);

    $to_auth = 'CERES';
    $to_coverage = 'spatialdomain';
    $to_lbl = 'CEIC Defined Regions';

    print qq[<div align="left">]  if ($printing);

    $sql = qq/SELECT id,abs(area(the_geom) - area(transform(GeometryFromText('$poly',4269),3309))) as fit FROM spatialdomain WHERE (the_geom && transform(GeometryFromText('$poly',4269),3309)) and intersects(the_geom,transform(GeometryFromText('$poly',4269),3309)) and (area(the_geom) < 90000000000) order by fit/;
    $nq = $dbh3->prepare($sql);
    $nq->execute();
    my $dat = $nq->fetchall_arrayref;
    foreach $row (@$dat) {
	push(@sp_id,$row->[0]);
    }
    $list = join("','",@sp_id);
    $sql = qq/SELECT sp.id,trim(sp.name),count(*) FROM spatialdomain sp, dataset d WHERE d.spatialdomain_id = sp.id and sp.id IN ('$list') group by sp.id,sp.name/;    

    $nq = $dbh3->prepare($sql);
    $nq->execute();


    print qq[<b>Matched on Spatial Query</b><br>]; 
    my ($shpname,$shpname_);
    while (@row = $nq->fetchrow_array) {
      $shpname = $row[0];
      $lbl2 = $row[1];
      $cnt = $row[2];
      $shpname_ = $shpname;
      $shpname_ =~ s/\s/_/g;
      
      print qq[<a href="http://gis.resources.ca.gov/catalog/regionQuery.epl?sp_$shpname=1">$lbl2($cnt)</a><br>] if ($printing);

      push(@matchList,$shpname);
   }

    print qq[<hr>];

    $lbl =~ s/\(.*\)//;
    $sql = qq/SELECT d.id,trim(title),substr(places,1,20) FROM dataset d,catalog c WHERE d.catalog_id = c.id and upper(c.status) = 'PUBLIC' and upper(places) LIKE upper('$lbl%') order by title/;    

    print qq[<b>Matched on Place Name String Search</b><br>]; 
#    print qq[<b>$sql</b><br>]; 
    $nq = $dbh3->prepare($sql);
    $nq->execute();

    while (@row = $nq->fetchrow_array) {
      $shpname = $row[0];
      $lbl = $row[1];
      $places = $row[2];
      $shpname_ = $shpname;
      $shpname_ =~ s/\s/_/g;
      print qq[<a href="http://gis.resources.ca.gov/catalog/BrowseRecord.epl?id=$shpname">$lbl($places)</a><br>] if ($printing);
   }

    print qq[</div>] if ($printing);
    $dbh3->disconnect();
}
sub nrpi_intersect {

    my $auth = shift;
    my $coverage = shift;
    my $name = shift;
    my $lbl = shift;
    
    $linkto = 'geofinder' if (!($linkto));
    # if called with a toShp only return a list otherwise print the results
    my $printing = 1 if (!($toShp));
    my ($sql,$poly,$dbh);
    my (@views,@matchList);
    my $area;

    $lbl = $name if (!($lbl));
    my $dbh = DBI->connect("dbi:Pg:dbname=gis");

    $poly = getPolygon($auth,$coverage,$name,4269,\$area);

    $to_auth = 'CERES';
    $to_coverage = 'spatialdomain';
    $to_lbl = 'CEIC Defined Regions';

    print qq[<div align="left">]  if ($printing);

    $sql = qq/SELECT title,online_link FROM nrpi_link WHERE (the_geom && transform(GeometryFromText('$poly',4269),3309)) and intersects(the_geom,transform(GeometryFromText('$poly',4269),3309)) order by title/;
    $nq = $dbh->prepare($sql);
    $nq->execute();

    my ($shpname,$link,$cnt);
    $cnt = 0;
    while (@row = $nq->fetchrow_array) {
      $shpname = $row[0];
      $link = $row[1];
      $cnt++;
      print qq[<a href="$link">$shpname</a><br>] if ($printing);

   }

    print qq[<b>No Projects Found</b>] if ($cnt lt 1);
    print qq[</div>] if ($printing);
    $dbh->disconnect();
}

sub shp2quadMap {

    my $auth = shift;
    my $coverage = shift;
    my $name = shift;
    my $lbl = shift;
    my $size = shift;
    my $linkto = shift;
    my $mapName = shift;

    $mapName = 'quadMap' if (!($mapName));
    my ($sql,$poly,$dbh,$cell,$ocodeStr);

    $poly = getPolygon($auth,$coverage,$name,4269);

    my $dbh = DBI->connect("dbi:Pg:dbname=gis");

    $size = 350 if (!($size));
    $linkto = 'drg' if (!($linkto));

## prepare the quad query
    $sql = qq/SELECT distinct description,description FROM grid_view WHERE length(description) = 7 and footprint && GeometryFromText(?,4269) and intersects(footprint,GeometryFromText(?,4269)) order by description/;


    $quadQuery = $dbh->prepare($sql);
    $quadQuery->execute($poly,$poly) || die "Could Not Execute quadQuery";
    my (@row,@ocode);
    while (@row = $quadQuery->fetchrow_array) {
      $cell = $row[1];
      if (!(join(' ',@ocode) =~ m/$cell/i)) {
	  push(@ocode,$cell);
      }
  }

$dbh->disconnect;
$ocodeStr = join(',',@ocode);

    if ($coverage eq 'calwater22') {
	print qq[<img height=$size width=$size name="mappedImage" src="/cgi-bin/api/spotquad?idnum=$name&amp;code=$quad&amp;cell=$ocodeStr&amp;width=$size&amp;height=$size&amp;mode=sr&amp;zoom=1&amp;spot=$spot" usemap="#$mapName" alt="$lbl"><br><br>];
    }
    elsif ($coverage eq 'counties') {
	print qq[<img height=$size width=$size name="mappedImage" src="/cgi-bin/api/spotquad?cnty=$name&amp;code=$quad&amp;cell=$ocodeStr&amp;width=$size&amp;height=$size&amp;mode=sr&amp;zoom=1&amp;spot=$spot" usemap="#$mapName"  alt="$lbl"><br><br>];
    }
    else {
	print qq[<img height=$size width=$size name="mappedImage" src="/cgi-bin/api/spotquad?code=$quad&amp;cell=$ocodeStr&amp;width=$size&amp;height=$size&amp;mode=sr&amp;zoom=1&amp;spot=$spot" usemap="#$mapName"  alt="$lbl"><br><br>];
    }

# print qq[<b>Select a Quad Intersecting $lbl for $linkto Download</b>];

map_quad('','',$ocodeStr,'',$size,$linkto,$mapName);

}

sub place_select_form {

    my $auth = shift;
    my $coverage = shift;
    my $name = shift;
    my $lbl = shift;
    my $linkto = shift;
    $linkto = 'geofinder' if (!($linkto));

    my $dbh= DBI->connect("dbi:Pg:dbname=gis");
    print qq[<table border=1 width=464 bgcolor="#6699CC"><tr>];

## Select for Bioregion Shapes
    print qq[<td  align="left"><table border=0><tr><td>];

    $sql = "select distinct description FROM bioregion_view order by description";

    $nq = $dbh->prepare($sql);
    $nq->execute();

    my $i = 0;
    my ($shpname,$shpname_);
    print qq[<td align="left" >];
    print qq[<form method=post action="$linkto">];
    print qq[<input type="hidden" name="auth" value="FRAP">];
    print qq[<input type="hidden" name="coverage" value="bioregions">];

#    print qq[<img src="http://ceic.atlas.ca.gov/images/info_dot_gif.jpg">];
    print qq[Bioregion:<br><select name="name" onChange="form.submit()">];
    print qq[<option>&nbsp;</option>];
    while (@row = $nq->fetchrow_array) {
      $shpname = $row[0];
      $shpname_ = $shpname;
      $shpname_ =~ s/\s/_/g;
      if ($shpname ne $name) {
	  print qq[<option>$shpname</option>];
      }
      else {
      print qq[<option selected>$shpname</option>];
  }
      $i++;
  }
    print qq[</select></form></td></tr>];

    print qq[<tr><td  align="left">];

    $sql = "select distinct idnum,hrname FROM hr order by hrname";

    $nq = $dbh->prepare($sql);
    $nq->execute();

    my $i = 0;
    my ($shpname,$shpname_);
    print qq[<td align="left" >];
    print qq[<form method=post action="$linkto">];
    print qq[<input type="hidden" name="auth" value="FRAP">];
    print qq[<input type="hidden" name="coverage" value="calwater22">];
    print qq[<input type="hidden" name="lbl" value="$lbl">];
    print qq[Hydrologic Region:<br>];
    print qq[<select name="name" onChange="form.submit()">];
    print qq[<option>&nbsp;</option>];
    while (@row = $nq->fetchrow_array) {
      $shpname = $row[0];
      $lbl = $row[1];
      if ($shpname ne $name) {
	  print qq[<option value="$shpname">$lbl</option>];
      }
      else {
      print qq[<option selected value="$shpname">$lbl</option>];
  }
      $i++;
  }
    print qq[</select></form></td></tr>];

## Select for County Shapes
    print qq[<tr><td  align="left">];

    $sql = "select distinct name from cnty24k97 order by name";
    $nq = $dbh->prepare($sql);
    $nq->execute();

my $i = 0;
my ($shpname,$shpname_);
print qq[<td align="left" >];
print qq[<form method=post action="$linkto">];
print qq[<input type="hidden" name="auth" value="CMCC">];
print qq[<input type="hidden" name="coverage" value="counties">];
print qq[County:<br>];
print qq[<select name="name" onChange="form.submit()">];
print qq[<option>&nbsp;</option>];
while (@row = $nq->fetchrow_array) {
      $shpname = $row[0];
      $shpname_ = $shpname;
      $shpname_ =~ s/\s/_/g;
if (($shpname eq $name) && ($coverage eq 'counties')) {
    print qq[<option selected>$shpname</option>];
}
else {
      print qq[<option>$shpname</option>];
  }
      $i++;
  }
print qq[</select></form></td></tr>];

print qq[</table></td><td valign="top"><table><tr><td  align="left">];

## Select for City Shapes

$sql = qq/SELECT distinct name FROM city2k where name not like '%(%)' order by name/;

$nq = $dbh->prepare($sql);
$nq->execute();

$i = 0;
print qq[<td align="left" >];
print qq[<form method=post action="$linkto">];
print qq[<input type="hidden" name="auth" value="FRAP">];
print qq[<input type="hidden" name="coverage" value="city2k">];
print qq[City:<br>];
print qq[<select name="name" onChange="form.submit()">];
print qq[<option>&nbsp;</option>];
while (@row = $nq->fetchrow_array) {
      $shpname = $row[0];
      $shpname_ = substr($shpname,0,30);
if (($shpname eq $name) && ($coverage eq 'city2k')) {
    print qq[<option value="$shpname" selected>$shpname_</option>];
}
else {
      print qq[<option value="$shpname">$shpname_</option>];
  }
      $i++;
  }
print qq[</select></form></td>];

## Select for Zip Codes
print qq[<tr><td  align="left">];

$i = 0;
print qq[<td align="left" >];
print qq[<form method=post action="$linkto">];
print qq[<input type="hidden" name="auth" value="Census">];
print qq[<input type="hidden" name="coverage" value="zip_codes">];
print qq[Zip Code:<br>];
print qq[<input name="name" size=5 ];
print qq[value="$name" ] if ($coverage eq 'zip_codes'); 
print qq[onChange="form.submit()">];
print qq[</form></td>];

## Place Name Query Box
print qq[<tr><td  align="left">];
print qq[<td bgcolor="#6699CC" align="left" >];
print qq[<form action="shpNameQuery">];
print qq[Place Name:<br>];
print qq[<input type="text" size="20" name="name"  onChange="form.submit()">];
print qq[<input type="submit" value="Find">];
print qq[</form></td>];

print qq[</tr></table>];
print qq[</td></tr></table>];

$dbh->disconnect();
}

sub place_select_form_tight {

print qq[
<SCRIPT LANGUAGE="JavaScript"><!--

var	 action = '';

function jumpTo(auth,coverage,name,action,zip) {
    var url;
    if (action == 'address') {
	url =  'geofinder?' + 'street=' + name + '&action=address';
	if (zip) {
	    url = url + '&zip=' + zip;
	}
    }
    else {
	url =  'geofinder?' + 'auth=' + auth + '&coverage=' + coverage + '&name=' + name + '&lbl=' + name;
    }


//alert(url)
    window.location = url
}
function nameSearch() {
    var url =  'shpNameQuery?' + 'name=' + window.document.geofinderForm.elements.placename.value;
//alert(url)
    window.location = url
}

//--></SCRIPT>
 ];
    my $auth = shift;
    my $coverage = shift;
    my $name = shift;
    my $lbl = shift;
    my $linkto = shift;
    my $action = shift;

    $linkto = 'geofinder' if (!($linkto));

    my $dbh= DBI->connect("dbi:Pg:dbname=gis");

    print qq[<table border=0 width="%100"><tr>];
    print qq[<td align="left" valign="top">];
    print qq[<form action="geofinder" name="geofinderForm">];
    print qq[<table border=0>];

## Select for Bioregion Shapes
    $sql = "select distinct description FROM bioregion_view order by description";

    $nq = $dbh->prepare($sql);
    $nq->execute();

    my $i = 0;
    my ($shpname,$shpname_);
    print qq[<tr><td align="left"  valign="top">];
#    print qq[<img src="http://ceic.atlas.ca.gov/images/info_dot_gif.jpg">];
    print qq[Bioregion:<br><select name="name" onChange="jumpTo('FRAP','bioregions',this.value)">];
    print qq[<option>&nbsp;</option>];
    while (@row = $nq->fetchrow_array) {
      $shpname = $row[0];
      $shpname_ = substr(ucfirst(lc($shpname)),0,20);
      $shpname_ =~ s/_/\s/g;
      if ($shpname ne $name) {
	  print qq[<option value="$shpname" >$shpname_</option>];
      }
      else {
      print qq[<option  value="$shpname" selected>$shpname_</option>];
  }
      $i++;
  }
    print qq[</select></td></tr>];

## Select Hydro Regions
    $sql = "select distinct idnum,hrname FROM hr order by hrname";

    $nq = $dbh->prepare($sql);
    $nq->execute();

    my $i = 0;
    my ($shpname,$shpname_);
    print qq[<tr><td align="left" >];
    print qq[Hydrologic Region:<br>];
    print qq[<select name="name" onChange="jumpTo('FRAP','calwater22',this.value)">];
    print qq[<option>&nbsp;</option>];
    while (@row = $nq->fetchrow_array) {
      $shpname = $row[0];
      $shplbl = $row[1];
      $shplbl = substr(ucfirst(lc($shplbl)),0,20);
      if ($shpname ne $name) {
	  print qq[<option value="$shpname">$shplbl</option>];
      }
      else {
      print qq[<option selected value="$shpname">$shplbl</option>];
  }
      $i++;
  }
    print qq[</select></td></tr>];
    print qq[</table></td><td valign="top"><table border=0>];

## Select for County Shapes
    print qq[<tr><td align="left" colspan=2>];


    print qq[<table border=0><tr>];
    $sql = "select distinct name from cnty24k97 order by name";
    $nq = $dbh->prepare($sql);
    $nq->execute();

my $i = 0;
my ($shpname,$shpname_);
print qq[<td align="left" >];
print qq[County:<br>];
print qq[<select name="name" onChange="jumpTo('CMCC','counties',this.value)">];
print qq[<option>&nbsp;</option>];
while (@row = $nq->fetchrow_array) {
      $shpname = $row[0];
      $shpname_ = $shpname;
      $shpname_ =~ s/\s/_/g;
if (($shpname eq $name) && ($coverage eq 'counties')) {
    print qq[<option value="$shpname" selected>$shpname</option>];
}
else {
      print qq[<option value="$shpname">$shpname</option>];
  }
      $i++;
  }
print qq[</select>];
## Address Query Box
print qq[</td><td  align="left">];

print qq[<input type="hidden" value="address" name="action">];
print qq[Street Address/Zip Code:<br>];
## Zip Codes
print qq[<input type="text" size="20" name="street" ];
print qq[value="$lbl"] if ($action eq 'address');
print qq[>];
print qq[<input name="zip" size=4 value="$zip"];
print qq[value="$name"] if ($coverage eq 'zip_codes'); 
print qq[>];
print qq[<input type="button" value="Find" onClick="jumpTo('','',window.document.geofinderForm.street.value,'address',window.document.geofinderForm.zip.value)">];
#print qq[<input type="submit" value="Find">];

print qq[</td></tr></table>];
print qq[</td></tr>];

## Select for City Shapes
print qq[<tr>];
$sql = qq/SELECT distinct name FROM city2k where name not like '%(%)' order by name/;

$nq = $dbh->prepare($sql);
$nq->execute();

$i = 0;
print qq[<td align="left" >];
print qq[City:<br>];
print qq[<select name="name" onChange="jumpTo('FRAP','city2k',this.value)">];
print qq[<option>&nbsp;</option>];
while (@row = $nq->fetchrow_array) {
      $shpname = $row[0];
      $shpname_ = substr($shpname,0,20);
if (($shpname eq $name) && ($coverage eq 'city2k')) {
    print qq[<option value="$shpname" selected>$shpname_</option>];
}
else {
      print qq[<option value="$shpname">$shpname_</option>];
  }
      $i++;
  }
print qq[</select></td>];

## Place Name Query Box
print qq[<td align="left" >];

print qq[Place Name:<br>];
print qq[<input type="text" size="20" name="placename" onChange="nameSearch()"];
print qq[ value="$lbl"] if ($auth eq 'GNIS');
print qq[>];
print qq[<input type="button" value="Match" onClick="nameSearch()"></td>];

print qq[</tr></table>];
print qq[</form>];
print qq[</td></tr></table>];


$dbh->disconnect();
}

sub map_quad {

    my $quad = shift;
    my $cell = shift;
    my $ocodeStr = shift;
    my $spot = shift;
    my $size = shift;
    my $linkto = shift;
    my $mapName = shift;
    my $callback = shift;

    $mapName = 'quadMap' if (!($mapName));
    my ($name,$points,$sql,$coords,$dbh,$dbh2,$st);

    print qq[<MAP NAME="$mapName">];

    $dbh= DBI->connect("dbi:Pg:dbname=gis");

    $ocodeStr =~ s/,/','/g;

    if ($ocodeStr) {
	$ext_sql = qq/select xmax(extent(the_geom))::integer, xmin(extent(the_geom))::integer, ymax(extent(the_geom))::integer, ymin(extent(the_geom))::integer FROM mapgrida WHERE substr(ocode,2,7) IN ('$ocodeStr')/;
	$sql = qq/SELECT substr(ocode,2,7) as cell, AsText(THE_GEOM), mapname FROM mapgrida WHERE substr(ocode,2,7) IN ('$ocodeStr')/;
	$dbh= DBI->connect("dbi:Pg:dbname=gis");
    }
    else {
	$ext_sql = qq/select xmax(extent(transform(polygon,3309)))::integer, xmin(extent(transform(polygon,3309)))::integer, ymax(extent(transform(polygon,3309)))::integer, ymin(extent(transform(polygon,3309)))::integer FROM degreeGridView where cell = '$quad'/;
#	$ext_sql = qq/select xmax(extent(the_geom)), xmin(extent(the_geom)), ymax(extent(the_geom)), ymin(extent(the_geom)) FROM mapgrida WHERE substr(ocode,2,7) like '$quad%'/;
	$sql = qq/SELECT substr(ocode,2,7) as cell, AsText(THE_GEOM), mapname FROM mapgrida WHERE substr(ocode,2,7) like '$quad%'/;
    }

    # Execute extend query
    $st = $dbh->prepare($ext_sql);
    $st->execute();
    @ext = $st->fetchrow_array();
    $st->finish;
 
    $maxx = $ext[0];
    $minx = $ext[1];
    $maxy = $ext[2];
    $miny = $ext[3];

   # remove pesky quotes added to neg numbers 
    $maxx =~ s/\'//g;
    $minx =~ s/\'//g;
    $maxy =~ s/\'//g;
    $miny =~ s/\'//g;

    # Execute cell query
    my $st = $dbh->prepare($sql);
    $st->execute();
    $dat = $st->fetchall_arrayref();    
    $st->finish;
    $dbh->disconnect();

    foreach $row (@$dat) {
	$cell = $row->[0];
	$points = $row->[1];
	$name = $row->[2]; ## . ' (' . $cell . ')';
	$points =~ s/[\(|\)|MULTIPOLYGON]//g;
	$points =~ s/\s/,/g;
	# remove pesky quotes added to neg numbers
	$points =~ s/\'//g;
	@pt = split(',',$points);
        $coords = create_imagemap($maxx,$maxy,$minx,$miny,$size);
	if ($linkto eq 'drg') {
	    $area = qq[<area shape="poly" title="$name" coords="$coords" href="#" onClick="DRG_show('$cell','$name');" alt="$name">\n];
	}
	elsif ($linkto eq 'doqq') {
	    $cell =~ s/^o//;
	    $area = qq[<area shape="poly" title="$name" coords="$coords" href="#" onClick="DOQQ_show('$cell','$name');" alt="$name">\n];
	}
	elsif ($linkto eq 'callback') {
	    $cell =~ s/^o//;
	    $area = qq[<area shape="poly" title="$name" coords="$coords" href="#" onClick="$callback('$cell','$name');" alt="$name">\n];
	}
	elsif ($linkto eq 'cnddb') {
	    $a = substr($cell,5,1);
#	    my $quadcode = substr($cell,0,5) . (ord(lc($a)) - 96) . substr($cell,6,1);
	    $area = qq[<area shape="poly" title="$name" coords="$coords" href="#" onClick="CNDDB_show('$cell','$name','cnddbQueryDiv');" alt="$name">\n];
	}
        else {
	    $area = qq[<area shape="poly" title="$name" coords="$coords" href="$linkto?auth=DOQQ;coverage=Grid;name=$cell;lbl=$name" alt="$name">\n];
	}
	print "$area";
    }

    print qq[</MAP>];
}


sub map_degree {

    my $quad = shift;
    my $size = shift;
    my $linkto = shift;
    my $mapName = shift;

    $mapName = 'degreeMap' if (!($mapName));

    my ($points,$sql,$minx,$maxx,$miny,$maxy,$dat,$name,$points,$coords);

    print qq[<MAP NAME="$mapName">];

    my $dbh= DBI->connect("dbi:Pg:dbname=gis");

#    $sql = qq/select xmax(extent(transform(polygon,3310))), xmin(extent(transform(polygon,3310))), ymax(extent(transform(polygon,3310))), ymin(extent(transform(polygon,3310))) FROM degreeGridView/;
#    $sql = qq/select xmax(extent(polygon)), xmin(extent(polygon)), ymax(extent(polygon)), ymin(extent(polygon)) FROM degreeGridView/;
    $sql = qq/select xmax(extent(footprint)), xmin(extent(footprint)), ymax(extent(footprint)), ymin(extent(footprint)) FROM ca_outline_view/;

    my $st = $dbh->prepare($sql);
    $st->execute();
    @ext = $st->fetchrow_array();
    $st->finish();

    $maxx = int($ext[0]);
    $minx = int($ext[1]);
    $maxy = int($ext[2]);
    $miny = int($ext[3]);

   # remove pesky quotes added to neg numbers 
    $maxx =~ s/\'//g;
    $minx =~ s/\'//g;
    $maxy =~ s/\'//g;
    $miny =~ s/\'//g;

    $sql = qq/SELECT cell, AsText(transform(POLYGON,4269)) FROM degreeGridView order by cell/;
    $st = $dbh->prepare($sql);
    $st->execute();
    $dat = $st->fetchall_arrayref();    
    $st->finish();

    foreach $row (@$dat) {
	$name = $row->[0];
	$quad = $name;
	$points = $row->[1];
	$points =~ s/[\(|\)|POLYGON]//g;
	$points =~ s/\s/,/g;
	@pt = split(',',$points);
        $coords = create_imagemap($maxx,$maxy,$minx,$miny,$size);
	$area = qq[<area shape="poly" title="$name" coords="$coords" href="$linkto?auth=DOQQ;coverage=Grid;name=$quad;lbl=$name" alt="$name">\n];
	print "$area";
    }
    print qq[</MAP>];
    $dbh->disconnect();
}


sub map_intersect {
    my $auth = shift;
    my $coverage = shift;
    my $name = shift;
    my $toShp = shift;
    my $size = shift;
    my $linkto = shift;
    my $ptBuffer = shift;
    my $mapName = shift;

    $mapName = 'shpMap' if (!($mapName));
    my ($ext,$matches,$points,$coords,$area,$maxx,$maxy,$minx,$miny,$lbl,$src);

    my %view;
    $view{counties} = 'county_view';
    $view{bioregions} = 'bioregion_view';
    $view{grid} = 'grid_view';
    $view{city2k} = 'city2k';
    $view{zip_codes} = 'zip_view';
    $view{calwater22} = 'hu_view';
    $view{geoname} = 'geoname';

    my %authFor;
    $authFor{counties} = 'CMCC';
    $authFor{bioregions} = 'FRAP';
    $authFor{grid} = 'DOQQ';
    $authFor{city2k} = 'FRAP';
    $authFor{zip_codes} = 'Census';
    $authFor{calwater22} = 'FRAP';
    $authFor{geoname} = 'GNIS';

    $matches = shp_intersect($auth,$coverage,$name,'','',$view{$toShp});
    $ext = getExtent($auth,$coverage,$name,3309);
    $maxx = $ext[0];
    $minx = $ext[1];
    $maxy = $ext[2];
    $miny = $ext[3];
   # remove pesky quotes added to neg numbers 
    $maxx =~ s/\'//g;
    $minx =~ s/\'//g;
    $maxy =~ s/\'//g;
    $miny =~ s/\'//g;

    print qq[<MAP NAME="$mapName">];
    foreach $match (@{$matches}) {
	($lbl,$src,$id) = split(/\|/,$match);
	$id =~ s/^.*://;
	$points = getPolygon($authFor{$toShp},$toShp,$id,3309);
	$points =~ s/[\(|\)|MULTIPOLYGON]//g;
	$points =~ s/\s/,/g;
	@pt = split(',',$points);
        $coords = create_imagemap($maxx,$maxy,$minx,$miny,$size);
	$area = qq[<area shape="poly" title="$lbl" coords="$coords" href="$linkto?auth=$authFor{$toShp}&amp;coverage=$toShp&amp;name=$id&amp;lbl=$lbl">\n];
	print "$area";
    }
    print qq[</MAP>];
}

sub map_coverage {
    my $coverage = shift;
    my $size = shift;
    my $linkto = shift;
    my $mapName = shift;

    $mapName = 'locMap' if (!($mapName));
    my ($ext,$matches,$points,$coords,$area,$maxx,$maxy,$minx,$miny);

    my %view;
    $view{counties} = 'county_view';
    $view{bioregions} = 'bioregion_view';

    $view{grid} = 'grid_view';
    $view{city2k} = 'city2k';
    $view{zip_codes} = 'zip_view';
    $view{calwater22} = 'hu_view';
    $view{geoname} = 'geoname';

    my %authFor;
    $authFor{counties} = 'CMCC';
    $authFor{bioregions} = 'FRAP';
    $authFor{grid} = 'DOQQ';
    $authFor{city2k} = 'FRAP';
    $authFor{zip_codes} = 'Census';
    $authFor{calwater22} = 'FRAP';
    $authFor{geoname} = 'GNIS';

    my $dbh= DBI->connect("dbi:Pg:dbname=gis");
    my $sql = qq[select description, max(area(footprint)) from $view{$coverage} group by description];
    my $st = $dbh->prepare($sql);
    $st->execute();
    my $matches = $st->fetchall_arrayref();    
    $st->finish();

    $ext = getExtent('CMCC','ca_outline',0,3309);
    $maxx = $ext[0];
    $minx = $ext[1];
    $maxy = $ext[2];
    $miny = $ext[3];

   # remove pesky quotes added to neg numbers 
    $maxx =~ s/\'//g;
    $minx =~ s/\'//g;
    $maxy =~ s/\'//g;
    $miny =~ s/\'//g;

    print qq[<MAP NAME="$mapName">];
    foreach $row (@{$matches}) {
	$match = $row->[0];
	$points = getPolygon($authFor{$coverage},$coverage,$match,3309);
	$points =~ s/[\(|\)|MULTIPOLYGON]//g;
	$points =~ s/\s/,/g;
	@pt = split(',',$points);
        $coords = create_imagemap($maxx,$maxy,$minx,$miny,$size);
	$area = qq[<area shape="poly" title="$match" coords="$coords" href="$linkto?auth=$authFor{$coverage}&amp;coverage=$coverage&amp;name=$match&amp;lbl=$match">\n];
	print "$area";
    }
    print qq[</MAP>];
}

sub create_imagemap {

    #@polygon is a simple list that stores the real world coordinates of the object pulled from the database
    my @vertices = @pt;
    my $maxx = shift;
    my $maxy = shift;
    my $minx = shift;
    my $miny = shift;
    my $size = shift;

    my @imgshape;

    ## We need to square up our extent so it's proportional to the image
    my $sizex = abs($maxx - $minx);
    my $sizey = abs($maxy - $miny);
    if ($sizey > $sizex) {
	$minx -= ($sizey - $sizex)/2; 
	$maxx += ($sizey - $sizex)/2;
    }
    elsif ($sizex > $sizey) {
	$miny -= ($sizex - $sizey)/2; 
	$maxy += ($sizex - $sizey)/2;
    }

    #loop through the list grabbing each x and y coordinate and converting the geo-coordinate to image coordinates
    while (@vertices) {

         #grab first coordinate, remove any parentheses
        $xvert = shift(@vertices);
        $xvert =~ s/[\(\)]//g;
        $xvert =~ s/\'//g;

         #convert x map coordinate to image coordinates
        $cellsizex = ($maxx - $minx)/($size - 1);
        $ximg = int(($xvert - $minx)/$cellsizex);

        #add image coord. to the new list
        push (@imgshape,$ximg);

        #grab second coordinate, remove any parentheses
        $yvert = shift(@vertices);
        $yvert =~ s/[\(\)]//g;
        $yvert =~ s/\'//g;

        #convert y map coordinate to image coordinates
        $cellsizey = ($maxy -$miny)/($size - 1);
        $yimg = int(($maxy - $yvert)/$cellsizey);

        #add image coord. to the new list
        push (@imgshape,$yimg);

       }

    #form the <area> tag for html output
    $coords = join(",",@imgshape);
    return $coords;
}

sub navbar {

    my $clip = shift;
    my $lbl = shift;
    my $action = shift;
    my $mode = shift;

    my $zoomStr;
    my $intersectStr = "geofinder?clip=$clip&lbl=$lbl&action=crossTo";
    my $relatedStr = "geofinder?clip=$clip&lbl=$lbl&action=intersect";
    my $imgLoc = "http://atlas.ca.gov/geofinder/images";

    my $ext = getExtent($auth,$coverage,$name,3310,300);
    my $maxx = $ext[0];
    my $minx = $ext[1];
    my $maxy = $ext[2];
    my $miny = $ext[3];

   # remove pesky quotes added to neg numbers 
    $maxx =~ s/\'//g;
    $minx =~ s/\'//g;
    $maxy =~ s/\'//g;
    $miny =~ s/\'//g;

    my $mapsurferStr = qq[http://atlas.ca.gov/mapsurfer/index.phtml?extent=$minx,$miny,$maxx,$maxy];
    if ($coverage eq 'calwater22') {
	$mapZoomStr = qq[/cgi-bin/api/showshed?idnum=$name&name=$lbl&size=800&spot=1&zoom=1&mode=drg];
	$imgZoomStr = qq[/cgi-bin/api/showshed?idnum=$name&name=$lbl&size=800&spot=1&zoom=1&mode=ls];
    }
    else {
	$mapZoomStr = qq[/cgi-bin/api/spotshape?clip=$clip&size=800&spot=1&zoom=1];
	$imgZoomStr = qq[/cgi-bin/api/spotshape?clip=$clip&size=800&spot=1&zoom=1&mode=image];
    }

print qq[
<table border="0" cellpadding="0" cellspacing="0" width="160">
<!-- fwtable fwsrc="navbar.png" fwbase="navbar.gif" fwstyle="Dreamweaver" fwdocid = "742308039" fwnested="0" -->
  <tr>
   <td><img src="$imgLoc/spacer.gif" width="160" height="1" border="0"></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="1" border="0"></td>
	 </tr>
  <tr>
   <td><img name="navbar_r1_c1" src="$imgLoc/navbar_r1_c1.gif" width="160" height="55" border="0"></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="55" border="0"></td>
	 </tr>];

 if ($mode ne 'pick') {
  print qq[ <tr>];

    if ($coverage && $name) {
	print qq[
	 <td><a href="#"  onClick="ceicQueryGF('$clip','gis')"  title="GIS Datasets" onMouseOut="MM_swapImgRestore();"  onMouseOver="MM_swapImage('gisDatasets','','gisDatasets_o.gif',1);" ><img name="gisDatasets" src="$imgLoc/gisDatasets.gif" width="160" height="16" border="0"></a></td>]}
	      else {
	print qq[
		 <td><a href="#" class="blueLink"><img name="gisDatasets" src="$imgLoc/gisDatasets.gif" width="160" height="16" border="0"></a></td>]}

print qq[
   <td><img src="$imgLoc/spacer.gif" width="1" height="16" border="0"></td>
  </tr>
  <tr>
   <td><img name="navbar_r3_c1" src="$imgLoc/navbar_r3_c1.gif" width="160" height="2" border="0"></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="2" border="0"></td>
	 </tr>];
  print qq[ <tr>];

    if ($coverage && $name) {
	print qq[
	 <td><a href="#" onClick="ceicQueryGF('$clip','database')" title="Databases" onMouseOut="MM_swapImgRestore();"  onMouseOver="MM_swapImage('databases','','databases_o.gif',1);" ><img name="databases" src="$imgLoc/databases.gif" width="160" height="16" border="0"></a></td>]}
	      else {
	print qq[
		 <td><a href="#" class="blueLink"><img name="databases" src="$imgLoc/databases.gif" width="160" height="16" border="0"></a></td>]}

print qq[
   <td><img src="$imgLoc/spacer.gif" width="1" height="16" border="0"></td>
  </tr>
  <tr>
   <td><img name="navbar_r3_c1" src="$imgLoc/navbar_r3_c1.gif" width="160" height="2" border="0"></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="2" border="0"></td>
	 </tr>];

    if ($coverage && $name) {
	print qq[
	 <tr>
		 <td><a href="#"  onClick="ceicQueryGF('$clip','project')"  onMouseOut="MM_swapImgRestore();"  onMouseOver="MM_swapImage('navbar_r4_c1','','navbar_r4_c1_f2.gif',1);" ><img name="navbar_r4_c1" src="$imgLoc/navbar_r4_c1.gif" width="160" height="16" border="0"></a></td>]}
    else {
	print qq[
		 <td><a href="#" class="blueLink"><img name="navbar_r4_c1" src="$imgLoc/navbar_r4_c1.gif" width="160" height="16" border="0"></a></td>]}

print qq[
   <td><img src="$imgLoc/spacer.gif" width="1" height="16" border="0"></td>
  </tr>
  <tr>
   <td><img name="navbar_r5_c1" src="$imgLoc/navbar_r5_c1.gif" width="160" height="2" border="0"></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="2" border="0"></td>
  </tr>

	 ];

  print qq[ <tr>];

    if ($coverage && $name) {
	print qq[
	 <td><a href="#"  onClick="ceicQueryGF('$clip','other')"  title="Other Resources" onMouseOut="MM_swapImgRestore();"  onMouseOver="MM_swapImage('otherResources','','otherResources_o.gif',1);" ><img name="otherResources" src="$imgLoc/otherResources.gif" width="160" height="16" border="0"></a></td>]}
	      else {
	print qq[
		 <td><a href="#" class="blueLink"><img name="otherResources" src="$imgLoc/otherResources.gif" width="160" height="16" border="0"></a></td>]}

print qq[
   <td><img src="$imgLoc/spacer.gif" width="1" height="16" border="0"></td>
  </tr>
  <tr>
   <td><img name="navbar_r3_c1" src="$imgLoc/navbar_r3_c1.gif" width="160" height="2" border="0"></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="2" border="0"></td>
	 </tr>
  <tr>
];

## Species Data
if ($auth eq 'DOQQ' && length($name) eq 7) {
   my $a = substr($name,5,1);
#   my $quadcode = substr($name,0,5) . (ord(lc($a)) - 96) . substr($name,6,1);
    print qq[<td><a href="#" onClick="CNDDB_show('$name','$name','cnddbQueryDiv');" title="CNDDB Query" class="thickbox" onMouseOut="MM_swapImgRestore();"  onMouseOver="MM_swapImage('navbar_r6_c1','','navbar_r6_c1_f2.gif',1);" ><img name="navbar_r6_c1" src="$imgLoc/navbar_r6_c1.gif" width="160" height="16" border="0"></a></td>];
}
else {
    print qq[<td><a href="#" class="blueLink" onClick="window.location = 'geofinder?clip=$clip&action=shp2quads&linkto=cnddb'" onMouseOut="MM_swapImgRestore();"  onMouseOver="MM_swapImage('navbar_r6_c1','','navbar_r6_c1_f2.gif',1);" ><img name="navbar_r6_c1" src="$imgLoc/navbar_r6_c1.gif" width="160" height="16" border="0"></a></td>];
}

print qq[
   <td><img src="$imgLoc/spacer.gif" width="1" height="16" border="0"></td>
  </tr>
  <tr>
    <td><a href="#"  onClick="window.location = 'http://ceic.resources.ca.gov/fad.html'"  title="Statewide Datasets" onMouseOut="MM_swapImgRestore();"  onMouseOver="MM_swapImage('statewide','','statewide_o.gif',1);" ><img name="statewide" src="$imgLoc/statewide.gif" width="160" height="16" border="0"></a></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="16" border="0"></td>
  </tr>
  <tr>
   <td><img name="navbar_r3_c1" src="$imgLoc/navbar_r3_c1.gif" width="160" height="2" border="0"></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="2" border="0"></td>
  </tr>
  <tr>
];

print qq[<!-- End Query Section -->];

print qq[
  <tr>
   <td><img name="navbar_r7_c1" src="$imgLoc/navbar_r7_c1.gif" width="160" height="21" border="0"></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="21" border="0"></td>
  </tr>
	 <tr>];
    if ($coverage && $name) {
	print qq[
   <td><a href="#" class="blueLink" onClick="window.location = '$mapZoomStr'" onMouseOut="MM_swapImgRestore();"  onMouseOver="MM_swapImage('navbar_r8_c1','','navbar_r8_c1_f2.gif',1);" ><img name="navbar_r8_c1" src="$imgLoc/navbar_r8_c1.gif" width="160" height="16" border="0"></a></td>]
}
    else {
	print qq[
   <td><a href="#" class="blueLink" ><img name="navbar_r8_c1" src="$imgLoc/navbar_r8_c1.gif" width="160" height="16" border="0"></a></td>]    }
print qq[
   <td><img src="$imgLoc/spacer.gif" width="1" height="16" border="0"></td>
  </tr>
  <tr>
   <td><img name="navbar_r9_c1" src="$imgLoc/navbar_r9_c1.gif" width="160" height="2" border="0"></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="2" border="0"></td>
  </tr>
  <tr>
   <td><a href="#" class="blueLink" onClick="window.location = '$imgZoomStr'" onMouseOut="MM_swapImgRestore();"  onMouseOver="MM_swapImage('navbar_r10_c1','','navbar_r10_c1_f2.gif',1);" ><img name="navbar_r10_c1" src="$imgLoc/navbar_r10_c1.gif" width="160" height="16" border="0"></a></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="16" border="0"></td>
  </tr>
  <tr>
   <td><img name="navbar_r11_c1" src="$imgLoc/navbar_r11_c1.gif" width="160" height="2" border="0"></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="2" border="0"></td>
  </tr>
  <tr>
	 ];
if ($auth eq 'DOQQ' && length($name) eq 7) {
  print qq[<td><a href="#" class="blueLink" onClick="DRG_show('$name','$lbl')" onMouseOut="MM_swapImgRestore();"  onMouseOver="MM_swapImage('navbar_r12_c1','','navbar_r12_c1_f2.gif',1);" ><img name="navbar_r12_c1" src="$imgLoc/navbar_r12_c1.gif" width="160" height="16" border="0"></a></td>];
}
else {
    print qq[<td><a href="#" class="blueLink" onClick="window.location = 'geofinder?clip=$clip&action=shp2quads&linkto=drg&lbl=$lbl'" onMouseOut="MM_swapImgRestore();"  onMouseOver="MM_swapImage('navbar_r12_c1','','navbar_r12_c1_f2.gif',1);" ><img name="navbar_r12_c1" src="$imgLoc/navbar_r12_c1.gif" width="160" height="16" border="0"></a></td>];
}
print qq[
   <td><img src="$imgLoc/spacer.gif" width="1" height="16" border="0"></td>
  </tr>
  <tr>
   <td><img name="navbar_r13_c1" src="$imgLoc/navbar_r13_c1.gif" width="160" height="1" border="0"></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="1" border="0"></td>
  </tr>

  <tr>
	 ];
if ($auth eq 'DOQQ' && length($name) eq 7) {
    print qq[<td><a href="#" class="blueLink" onClick="DOQQ_show('$name','$lbl');" onMouseOut="MM_swapImgRestore();"  onMouseOver="MM_swapImage('navbar_r14_c1','','navbar_r14_c1_f2.gif',1);" ><img name="navbar_r14_c1" src="$imgLoc/navbar_r14_c1.gif" width="160" height="16" border="0"></a></td>];
}
else {
    print qq[<td><a href="#" class="blueLink" onClick="window.location = 'geofinder?clip=$clip&action=shp2quads&linkto=doqq&lbl=$lbl'" onMouseOut="MM_swapImgRestore();"  onMouseOver="MM_swapImage('navbar_r14_c1','','navbar_r14_c1_f2.gif',1);" ><img name="navbar_r14_c1" src="$imgLoc/navbar_r14_c1.gif" width="160" height="16" border="0"></a></td>];
}

print qq[
   <td><img src="$imgLoc/spacer.gif" width="1" height="16" border="0"></td>
  </tr>
  <tr>
   <td><img name="navbar_r15_c1" src="$imgLoc/navbar_r15_c1.gif" width="160" height="1" border="0"></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="1" border="0"></td>
	 </tr>];
}

print qq[
  <tr>
   <td><img name="navbar_r17_c1" src="$imgLoc/navbar_r17_c1.gif" width="160" height="19" border="0"></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="19" border="0"></td>
  </tr>
  <tr>
   <td><a href="#" class="blueLink" onClick="window.location = '$intersectStr' + '&toShp=bioregions'" onMouseOut="MM_swapImgRestore();"  onMouseOver="MM_swapImage('navbar_r18_c1','','navbar_r18_c1_f2.gif',1);" ><img name="navbar_r18_c1" src="$imgLoc/navbar_r18_c1.gif" width="160" height="16" border="0"></a></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="16" border="0"></td>
  </tr>
  <tr>
   <td><img name="navbar_r19_c1" src="$imgLoc/navbar_r19_c1.gif" width="160" height="2" border="0"></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="2" border="0"></td>
  </tr>
  <tr>
   <td><a href="#" class="blueLink" onClick="window.location = '$intersectStr' + '&toShp=calwater22'" onMouseOut="MM_swapImgRestore();"  onMouseOver="MM_swapImage('navbar_r20_c1','','navbar_r20_c1_f2.gif',1);" ><img name="navbar_r20_c1" src="$imgLoc/navbar_r20_c1.gif" width="160" height="16" border="0"></a></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="16" border="0"></td>
  </tr>
  <tr>
   <td><img name="navbar_r21_c1" src="$imgLoc/navbar_r21_c1.gif" width="160" height="2" border="0"></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="2" border="0"></td>
  </tr>
  <tr>
   <td><a href="#" class="blueLink" onClick="window.location = '$intersectStr' + '&toShp=counties'" onMouseOut="MM_swapImgRestore();"  onMouseOver="MM_swapImage('navbar_r22_c1','','navbar_r22_c1_f2.gif',1);" ><img name="navbar_r22_c1" src="$imgLoc/navbar_r22_c1.gif" width="160" height="16" border="0"></a></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="16" border="0"></td>
  </tr>
  <tr>
   <td><img name="navbar_r23_c1" src="$imgLoc/navbar_r23_c1.gif" width="160" height="1" border="0"></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="1" border="0"></td>
  </tr>
  <tr>
   <td><a href="#" class="blueLink" onClick="window.location = '$intersectStr' + '&toShp=city2k'" onMouseOut="MM_swapImgRestore();"  onMouseOver="MM_swapImage('cities','','cities_o.gif',1);" ><img name="cities" src="$imgLoc/cities.gif" width="160" height="16" border="0"></a></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="16" border="0"></td>
  </tr>
  <tr>
   <td><img name="navbar_r25_c1" src="$imgLoc/navbar_r25_c1.gif" width="160" height="1" border="0"></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="1" border="0"></td>
  </tr>
  <tr>
   <td><a href="#" class="blueLink" onClick="window.location = '$intersectStr' + '&toShp=zip_codes'" onMouseOut="MM_swapImgRestore();"  onMouseOver="MM_swapImage('navbar_r26_c1','','navbar_r26_c1_f2.gif',1);" ><img name="navbar_r26_c1" src="$imgLoc/navbar_r26_c1.gif" width="160" height="16" border="0"></a></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="16" border="0"></td>
  </tr>

  <tr>
   <td><img name="navbar_r25_c1" src="$imgLoc/navbar_r25_c1.gif" width="160" height="1" border="0"></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="1" border="0"></td>
  </tr>
  <tr>
   <td><a href="#" class="blueLink" onClick="window.location = '$intersectStr' + '&toShp=grid'" onMouseOut="MM_swapImgRestore();"  onMouseOver="MM_swapImage('navbar_quad1','','navbar_quad2.gif',1);" ><img name="navbar_quad1" src="$imgLoc/navbar_quad1.gif" width="160" height="16" border="0"></a></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="16" border="0"></td>
  </tr>


  <tr>
   <td><img name="navbar_r25_c1" src="$imgLoc/navbar_r25_c1.gif" width="160" height="1" border="0"></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="1" border="0"></td>
  </tr>
  <tr>
   <td><a href="#" class="blueLink" onClick="ceicQueryGF('$clip','related')" onMouseOut="MM_swapImgRestore();"  onMouseOver="MM_swapImage('navbar_related1','','navbar_related2.gif',1);" ><img name="navbar_related1" src="$imgLoc/navbar_related1.gif" width="160" height="16" border="0"></a></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="16" border="0"></td>
  </tr>
  <tr>
   <td><img name="navbar_r27_c1" src="$imgLoc/navbar_r27_c1.gif" width="160" height="20" border="0"></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="20" border="0"></td>
  </tr>
  <tr>
   <td><a href="#" class="blueLink" onClick="info_show();" onMouseOut="MM_swapImgRestore();"  onMouseOver="MM_swapImage('navbar_r28_c1','','navbar_r28_c1_f2.gif',1);" ><img name="navbar_r28_c1" src="$imgLoc/navbar_r28_c1.gif" width="160" height="16" border="0"></a></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="16" border="0"></td>
  </tr>
  <tr>
   <td><img name="navbar_r29_c1" src="$imgLoc/navbar_r29_c1.gif" width="160" height="2" border="0"></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="2" border="0"></td>
  </tr>
  <tr>
   <td><a href="#" class="blueLink" onClick="link_show()" onMouseOut="MM_swapImgRestore();"  onMouseOver="MM_swapImage('navbar_r30_c1','','navbar_r30_c1_f2.gif',1);" ><img name="navbar_r30_c1" src="$imgLoc/navbar_r30_c1.gif" width="160" height="16" border="0"></a></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="16" border="0"></td>
  </tr>
  <tr>
   <td><img name="navbar_r31_c1" src="$imgLoc/navbar_r31_c1.gif" width="160" height="17" border="0"></td>
   <td><img src="$imgLoc/spacer.gif" width="1" height="16" border="0"></td>
  </tr>
</table>
];

}

sub shpNameQuery {

my ($auth,$coverage);
my $name = shift;

my $dbh= DBI->connect("dbi:Pg:dbname=gis");

## prepare the box query
my $sql = qq/SELECT distinct auth,coverage,description FROM county_view where upper(description) like upper('%$name%') order by description/;

my $st = $dbh->prepare($sql);
$st->execute();

print qq[<p><table width="100%"><tr><th colspan=2 bgcolor="#6699CC" >Matching Counties</th></tr><tr><td valign="top">];
while (@row = $st->fetchrow_array) {
    $auth = $row[0];
    $coverage = $row[1];
    print qq[<a  href="geofinder?auth=$auth&coverage=counties&name=$row[2]">$row[2]</a><br>];
}

print qq[</td><td valign="top">];

print qq[</td></tr></table>];

## prepare the box query
$sql = qq/SELECT distinct 'FRAP','city2k',name FROM city2k where upper(name) like upper('%$name%') and upper(name) not like '%(%)' order by name/;

$st = $dbh->prepare($sql);
$st->execute();

print qq[<p><table width="100%"><tr><th colspan=2 bgcolor="#6699CC" >Matching Cities</th></tr><tr><td valign="top">];
while (@row = $st->fetchrow_array) {
    $auth = $row[0];
    $coverage = $row[1];
    print qq[<a  href="geofinder?auth=$auth&coverage=city2k&name=$row[2]">$row[2]</a><br>];
}

print qq[</td><td valign="top">];

print qq[</td></tr></table>];

## prepare the box query
$sql = qq/SELECT distinct ocode,mapname FROM mapgrida where upper(mapname) like upper('%$name%') order by ocode desc/;

$st = $dbh2->prepare($sql);
$st->execute();

print qq[<p><table width="100%"><tr><th colspan=2 bgcolor="#6699CC" >Matching USGS 7.5 Min Quads</th></tr><tr><td valign="top">];
while (@row = $st->fetchrow_array) {
    $quad = substr($row[0],1,5);
    $cell = substr($row[0],1,7);
    $lbl = $row[1] . ' (' . $cell . ')';
    push (@codes,$cell);
    print qq[<a  href="geofinder?auth=DOQQ&coverage=Grid&name=$cell&lbl=$lbl">$row[1] ($cell)</a><br>];
}

print qq[</td><td valign="top">];

my $ocodes = join(',',@codes);

print qq[</td></tr></table>];

print qq[<p><table width="100%"><tr><th colspan=2 bgcolor="#6699CC" >Matching Calwater Watersheds</th></tr><tr><td valign="top">];
## prepare the calw query
$sql = qq/SELECT distinct hrname FROM hr where upper(hrname) like upper('%$name%')/;

$st = $dbh->prepare($sql);
$st->execute();

while (@row = $st->fetchrow_array) {
    print "$row[0] (Region)\n";
}

## prepare the box query
$sql = qq/SELECT distinct hu.idnum,huname,hrname FROM hu,hr where hr.idnum = substr(hu.idnum,1,2) and upper(huname) like upper('%$name%')/;

$st = $dbh->prepare($sql);
$st->execute();

while (@row = $st->fetchrow_array) {
    print qq[<a  href="geofinder?auth=FRAP&coverage=calwater22&name=$row[0]">$row[2](HR) > $row[1](Unit)</a><br>];
}

## prepare the box query
$sql = qq/SELECT distinct ha.idnum,haname,huname,hrname FROM ha,hu,hr where hr.idnum = substr(ha.idnum,1,2) and hu.idnum = substr(ha.idnum,1,5) and upper(haname) like upper('%$name%')/;

$st = $dbh->prepare($sql);
$st->execute();

while (@row = $st->fetchrow_array) {
    print qq[<a  href="geofinder?auth=FRAP&coverage=calwater22&name=$row[0]">$row[3](HR) > $row[2](HU) > $row[1](HA)</a><br>];
}

## prepare the box query
$sql = qq/SELECT distinct hsa.idnum,hsaname,haname,huname,hrname FROM hsa,ha,hu,hr where ha.idnum = substr(hsa.idnum,1,7) and hu.idnum = substr(hsa.idnum,1,5) and hr.idnum = substr(hsa.idnum,1,2) and upper(hsaname) like upper('%$name%')/;

$st = $dbh->prepare($sql);
$st->execute();

while (@row = $st->fetchrow_array) {
    print qq[<a  href="geofinder?auth=FRAP&coverage=calwater22&name=$row[0]">$row[4](HR) > $row[3](HU) > $row[2](HA) > $row[1](HSA)</a><br>];
}

## prepare the box query
$sql = qq/SELECT distinct  spws.idnum,spwsname,huname,hrname FROM spws,hu,hr where hu.idnum = substr(spws.idnum,1,5) and hr.idnum = substr(spws.idnum,1,2) and upper(spwsname) like upper('%$name%')/;

$st = $dbh->prepare($sql);
$st->execute();

while (@row = $st->fetchrow_array) {
    print qq[<a  href="geofinder?auth=FRAP&coverage=calwater22&name=$row[0]">$row[3](HR) > $row[2](HU) > $row[1](SPWS)</a><br>];
}

## prepare the box query
$sql = qq/SELECT distinct pws.idnum,pwsname,huname,hrname FROM pws,hu,hr where hu.idnum = substr(pws.idnum,1,5) and hr.idnum = substr(pws.idnum,1,2) and upper(pwsname) like upper('%$name%')/;

$st = $dbh->prepare($sql);
$st->execute();

while (@row = $st->fetchrow_array) {
    print qq[<a  href="geofinder?auth=FRAP&coverage=calwater22&name=$row[0]">$row[3](HR) > $row[2](HU) > $row[1](PWS)</a><br>];
}

print qq[</td></tr></table>];

## prepare the box query
$sql = qq/SELECT gid,name,type FROM geonamea where upper(name) like upper('%$name%') order by type,name/;

$st = $dbh->prepare($sql);
$st->execute();

print qq[<p><table width="100%"><tr><th colspan=2 bgcolor="#6699CC" >Other Places (GNIS)</th></tr><tr><td valign="top">];
while (@row = $st->fetchrow_array) {
    $gid = $row[0];
    $name = $row[1];
    $type = ucfirst($row[2]);
    if ($type =~ /Building|School|Church/) {
	$ptBuffer = 800
    }
    else {
    $ptBuffer = 1600
    }
    print qq[<a  href="geofinder?auth=GNIS&coverage=geoname&name=$gid&lbl=$name&size=300&ptBuffer=$ptBuffer">$type&nbsp;-&nbsp;$name</a><br>];
}

print qq[</td></tr></table>];

$dbh->disconnect;
$dbh2->disconnect;


}

sub overview {

print qq[

<hr>
<table><tr><td  width=500>
<p><b>The CERES GeoFinder</b>

<p>GeoFinder is a tool that helps find and retrieve geographic and environmental data from the California Resources Agency, its consituent departments, and other partner organizations.  GeoFinder lets users drill down through a map of California using predefined geographies like zip code, watershed, bioregion, county, city or by place name.  Once an area of interest is identified, the user can easily locate and retrieve digital orthophoto quads and scanned USGS map sheets for the area.  A user can also ask for a list of rare species and natural communities inventoried by Fish and Game.s Natural Diversity Data Base.  GeoFinder also lets users easily crosswalk between these geographies. 
<p>Begin by selecing a region, searching on place name, or clicking on the state image.
<p>
</td></tr></table>
	 ];
}

sub links {

print qq[

<hr>
<table><tr><td width=500>
<p ><b>Links</b>

<p><a href="http://resources.ca.gov/" >California Resources Agency</a>
<p><a href="http://gis.resources.ca.gov/" >California Spatial Information Library</a>
<p><a href="http://ceres.ca.gov" >California Environmental Resources Evaluation System (CERES)</a>
<p><a href="http://gis.resources.ca.gov/catalog" >California Environmental Information Catalog (CEIC)</a>
	 ];
}

sub display_error {

    my $error = shift;

    print qq[

	     <hr>
	     <table><tr><td width=500>
	     <p ><b>A Problem Occured:</b></p>
	     <p > $error</p>
	     </td></tr></table>
	 ];
}

sub county_ismap {

  print qq[

<MAP NAME="locMap">
<area shape="poly" title="Alameda" coords="40,62,40,66,40,66,41,67,36,66,36,67,35,67,34,67,33,66,31,63,30,60,32,60,32,61,33,61,34,62,35,62,35,63,35,63,40,62" href="geofinder?clip=counties:Alameda&amp;lbl=Alameda">

<area shape="poly" title="Alpine" coords="59,45,63,49,62,49,62,50,63,50,63,52,63,53,63,53,62,53,62,54,62,54,62,54,61,54,61,53,61,53,60,53,59,54,57,52,57,53,57,49,59,48,59,47,59,46,59,46,59,45" href="geofinder?clip=counties:Alpine&amp;lbl=Alpine">

<area shape="poly" title="Amador" coords="57,49,57,52,57,52,55,52,54,52,53,52,53,53,52,52,52,53,51,53,51,54,49,55,49,55,49,55,48,56,46,56,46,55,46,52,46,51,47,51,47,51,49,51,50,51,51,52,54,51,55,50,55,50,56,50,57,49,57,49" href="geofinder?clip=counties:Amador&amp;lbl=Amador">

<area shape="poly" title="Butte" coords="42,27,42,27,42,28,42,28,43,28,42,29,42,31,42,31,42,31,42,31,42,32,43,32,43,32,43,32,43,32,44,33,44,33,44,34,44,34,44,34,45,35,45,35,46,35,46,36,45,36,45,36,45,36,45,37,43,37,43,37,43,38,43,38,42,39,40,40,40,40,39,40,36,40,37,38,37,37,37,36,35,36,35,36,36,35,35,35,36,35,35,34,36,34,36,33,36,34,36,33,36,33,35,33,35,32,35,32,35,32,35,32,35,31,38,31,39,30,39,30,40,29,40,29,40,28,41,28,41,28,42,28,42,27" href="geofinder?clip=counties:Butte&amp;lbl=Butte">

<area shape="poly" title="Calaveras" coords="57,52,57,53,57,52,58,53,56,54,53,58,53,59,52,59,52,59,52,59,52,60,52,60,51,61,50,61,47,58,46,56,48,56,49,55,49,55,49,55,51,54,51,53,52,53,52,52,53,53,53,52,54,52,55,52,57,52,57,52" href="geofinder?clip=counties:Calaveras&amp;lbl=Calaveras">

<area shape="poly" title="Colusa" coords="36,39,36,40,36,41,36,41,36,42,36,42,36,42,36,42,36,43,37,43,37,44,37,44,37,45,37,45,31,45,30,45,30,44,30,43,30,43,29,42,30,42,30,42,30,41,29,41,29,41,27,41,27,40,27,40,26,39,27,39,26,38,34,38,34,38,35,38,35,39,36,39" href="geofinder?clip=counties:Colusa&amp;lbl=Colusa">

<area shape="poly" title="Contra Costa" coords="31,58,32,58,33,58,34,58,36,58,36,58,37,58,37,59,38,58,39,58,40,57,40,58,40,58,40,58,40,59,40,59,40,59,40,59,40,60,40,61,40,61,40,62,35,63,35,63,35,62,34,62,33,61,33,61,32,60,30,60,30,60,30,59,31,58" href="geofinder?clip=counties:Contra Costa&amp;lbl=Contra Costa">

<area shape="poly" title="Del Norte" coords="12,1,12,1,12,0,19,0,19,0,19,0,19,1,19,1,18,1,18,1,18,2,17,2,18,3,17,3,18,4,17,5,17,5,17,5,17,6,17,6,17,6,18,7,18,7,18,8,18,8,18,8,18,9,18,9,16,9,16,8,13,7,13,6,13,6,13,5,12,5,12,4,12,3,12,3,12,3,11,3,12,2,12,0,12,1,12,1" href="geofinder?clip=counties:Del Norte&amp;lbl=Del Norte">

<area shape="poly" title="El Dorado" coords="51,46,52,45,53,44,55,44,56,44,56,43,58,43,58,44,59,46,59,46,59,47,59,48,57,49,57,49,57,49,56,50,55,50,55,50,55,51,52,52,51,52,50,51,49,51,48,51,48,51,48,51,48,51,47,51,47,51,46,52,45,48,45,49,45,48,45,47,45,47,46,47,45,47,46,47,46,46,46,46,46,46,46,46,47,45,47,45,48,45,48,45,48,45,49,44,49,44,50,45,50,45,50,45,50,45,51,45,51,46" href="geofinder?clip=counties:El Dorado&amp;lbl=El Dorado">

<area shape="poly" title="Fresno" coords="69,65,71,66,71,66,71,67,72,66,72,67,72,67,72,68,72,68,72,69,73,69,73,69,74,70,73,70,74,72,74,72,76,73,76,74,77,75,77,76,77,76,77,77,75,77,70,77,70,79,66,79,66,80,64,80,62,82,62,82,61,82,61,82,58,82,58,86,54,90,54,89,53,89,52,88,52,88,52,88,51,87,51,87,50,87,50,86,50,86,51,85,50,85,50,84,50,84,51,84,51,81,47,77,52,73,52,73,52,74,53,75,52,75,53,76,54,76,54,77,54,77,54,77,54,77,55,77,55,77,56,77,56,77,57,76,58,76,58,76,59,76,59,76,60,76,61,75,61,74,61,74,61,74,62,74,62,73,62,73,62,73,62,73,63,73,63,73,63,72,63,72,63,72,63,72,64,71,64,72,64,72,65,72,65,71,65,72,65,71,66,71,66,70,66,69,66,69,66,69,66,69,69,65" href="geofinder?clip=counties:Fresno&amp;lbl=Fresno">

<area shape="poly" title="Glenn" coords="35,32,35,32,35,33,36,33,36,33,36,34,36,33,36,34,35,34,36,35,35,35,36,35,35,36,35,36,37,36,37,37,37,38,36,39,35,39,35,38,34,38,34,38,27,38,27,35,25,35,25,34,25,34,25,33,25,33,25,32,35,32" href="geofinder?clip=counties:Glenn&amp;lbl=Glenn">

<area shape="poly" title="Humboldt" coords="13,7,16,8,16,9,19,9,20,9,20,10,20,10,20,10,20,10,20,11,20,12,20,12,20,13,20,13,20,13,20,14,20,14,19,16,18,15,18,15,18,15,18,16,18,16,18,16,18,16,18,17,18,17,18,17,18,18,18,29,13,29,12,28,12,28,12,27,11,27,9,25,9,24,9,23,9,22,9,21,10,19,10,20,10,19,10,19,10,19,10,19,10,19,11,18,11,18,10,18,11,19,11,18,11,18,11,18,11,18,11,17,11,18,11,17,12,17,13,17,12,16,12,16,11,17,11,18,11,18,12,16,12,14,12,14,12,13,12,13,12,12,12,12,13,11,13,7" href="geofinder?clip=counties:Humboldt&amp;lbl=Humboldt">

<area shape="poly" title="Imperial" coords="124,124,124,124,123,125,123,125,123,126,123,127,123,127,123,127,123,128,123,129,123,129,124,130,124,130,124,130,125,130,126,131,126,131,126,131,126,132,126,133,126,133,126,134,125,134,124,134,123,134,123,135,106,137,106,130,106,130,106,125,124,124" href="geofinder?clip=counties:Imperial&amp;lbl=Imperial">

<area shape="poly" title="Inyo" coords="83,67,97,79,110,90,109,90,109,90,83,91,83,91,82,91,82,90,82,90,82,89,81,89,82,88,81,88,81,87,81,87,81,86,81,86,80,85,80,84,80,83,80,82,80,82,80,82,79,82,79,81,79,81,79,81,79,81,78,80,78,80,78,79,78,79,77,79,78,78,77,78,78,78,77,78,77,77,77,76,77,76,77,75,76,74,76,73,74,72,74,72,73,70,74,70,73,69,73,69,72,69,72,68,72,68,72,67,72,67,83,67" href="geofinder?clip=counties:Inyo&amp;lbl=Inyo">

<area shape="poly" title="Kern" coords="82,91,86,91,86,92,86,92,86,93,86,93,86,93,86,93,86,105,71,106,71,106,70,106,70,106,67,106,67,105,67,105,67,105,65,105,65,104,64,104,64,102,64,102,64,102,63,102,63,100,62,100,62,99,60,99,60,98,59,98,59,97,58,97,58,96,57,95,57,94,56,94,56,91,82,91" href="geofinder?clip=counties:Kern&amp;lbl=Kern">

<area shape="poly" title="Kings" coords="63,81,63,81,63,82,64,82,64,84,63,84,63,91,55,91,55,91,55,91,55,90,55,90,54,90,58,86,58,82,61,82,61,82,62,82,62,82,63,81" href="geofinder?clip=counties:Kings&amp;lbl=Kings">

<area shape="poly" title="Lake" coords="25,35,27,35,27,38,26,38,27,39,26,39,28,41,29,41,30,42,30,42,30,42,29,42,30,43,30,44,30,45,31,45,30,45,30,45,31,46,30,46,31,47,30,47,30,47,30,48,28,49,28,48,27,48,27,47,26,46,26,46,25,46,25,45,24,45,24,44,23,44,23,44,23,43,23,43,23,41,23,41,24,40,24,40,24,40,24,40,23,39,23,38,23,38,23,38,23,36,25,36,25,35" href="geofinder?clip=counties:Lake&amp;lbl=Lake">

<area shape="poly" title="Lassen" coords="58,12,58,30,58,34,58,34,56,34,56,34,56,34,57,33,57,31,57,31,56,30,56,30,56,30,56,28,55,29,55,28,54,28,53,27,53,27,53,27,53,27,52,26,51,26,51,26,51,26,51,25,49,25,49,25,49,26,48,26,48,27,48,27,47,26,47,26,47,26,46,26,46,26,46,26,46,23,43,23,43,16,43,16,43,12,58,12" href="geofinder?clip=counties:Lassen&amp;lbl=Lassen">

<area shape="poly" title="Los Angeles" coords="86,105,86,109,86,113,86,117,85,117,85,117,85,117,85,118,85,118,83,118,83,119,82,120,81,121,81,121,81,121,81,121,81,121,81,121,81,121,80,121,80,121,80,121,80,121,80,121,80,121,79,121,79,121,79,121,79,121,79,121,79,121,79,122,79,122,78,121,77,121,77,121,78,120,78,120,77,118,77,118,77,118,77,118,76,117,73,117,72,118,72,117,71,117,71,116,73,115,74,115,74,114,75,114,74,113,71,106,86,105" href="geofinder?clip=counties:Los Angeles&amp;lbl=Los Angeles">

<area shape="poly" title="Madera" coords="66,63,67,63,67,63,68,63,69,64,69,65,66,69,66,69,66,69,66,69,66,70,66,71,65,71,65,71,65,71,64,72,64,71,63,72,63,72,63,72,63,72,63,73,63,73,62,73,62,73,62,73,62,73,62,74,61,74,61,74,61,74,61,75,60,75,60,76,59,76,59,76,58,76,58,76,57,76,56,77,56,77,55,77,55,77,55,77,55,77,54,77,54,77,54,77,54,76,53,76,52,75,53,75,52,74,52,73,52,72,54,72,55,71,57,71,61,68,62,68,62,67,62,67,62,66,63,66,63,66,66,62,66,63" href="geofinder?clip=counties:Madera&amp;lbl=Madera">

<area shape="poly" title="Marin" coords="24,55,23,54,24,54,24,54,24,54,26,56,28,56,28,56,29,57,31,58,30,59,30,61,29,61,28,61,27,60,27,60,27,60,27,60,27,60,27,60,26,60,25,59,24,58,25,58,25,58,24,58,24,58,24,58,24,58,24,58,24,57,24,58,24,57,24,58,24,57,24,58,24,58,24,58,23,58,24,59,23,59,24,56,23,55,25,58,25,57,25,57,24,56,24,55,24,55,24,55" href="geofinder?clip=counties:Marin&amp;lbl=Marin">

<area shape="poly" title="Mariposa" coords="65,61,65,61,66,62,63,66,63,66,62,66,62,67,62,67,62,68,61,68,57,71,57,70,56,70,56,69,55,67,55,67,53,64,54,64,53,64,53,64,54,64,54,64,54,64,54,64,54,63,55,63,55,63,55,63,55,62,56,62,56,62,56,62,57,62,59,63,59,63,59,62,60,63,62,62,62,62,62,61,63,61,63,61,63,60,63,60,64,61,65,61,65,61" href="geofinder?clip=counties:Mariposa&amp;lbl=Mariposa">

<area shape="poly" title="Mendocino" coords="18,29,18,29,25,30,25,30,25,30,25,31,25,32,25,32,25,33,25,33,25,34,25,34,25,36,23,36,23,38,23,38,23,38,23,39,24,40,24,40,24,40,24,40,23,41,23,41,23,43,23,43,23,44,23,44,24,44,24,45,25,45,25,46,26,46,23,46,23,46,22,46,22,47,19,47,19,47,18,47,18,47,17,46,16,45,15,45,15,44,15,44,16,44,16,43,16,42,15,41,15,40,15,39,15,39,15,39,15,39,14,38,15,37,15,37,15,37,15,36,15,35,15,36,15,35,15,34,15,33,15,31,14,31,14,30,13,29,18,29" href="geofinder?clip=counties:Mendocino&amp;lbl=Mendocino">

<area shape="poly" title="Merced" coords="53,64,55,67,55,67,56,69,56,70,57,70,57,71,55,71,54,72,52,72,47,77,45,76,44,75,44,75,43,75,44,74,43,74,43,74,43,73,44,73,43,72,44,72,44,72,47,69,46,68,53,64" href="geofinder?clip=counties:Merced&amp;lbl=Merced">

<area shape="poly" title="Modoc" coords="42,0,58,1,58,12,42,12,42,0" href="geofinder?clip=counties:Modoc&amp;lbl=Modoc">

<area shape="poly" title="Mono" coords="63,49,83,67,72,67,72,66,71,67,71,66,69,65,69,64,68,63,67,63,67,63,67,63,66,63,67,62,67,62,67,61,67,61,67,61,66,60,66,59,66,59,66,58,65,57,64,58,64,58,64,57,64,57,63,57,63,57,63,56,62,56,62,56,62,56,62,55,62,55,62,55,62,54,62,54,62,53,63,53,63,53,63,52,63,50,62,50,62,49,63,49,63,49" href="geofinder?clip=counties:Mono&amp;lbl=Mono">

<area shape="poly" title="Monterey" coords="39,75,39,76,40,77,41,77,41,77,41,78,40,78,41,78,42,79,42,79,42,80,43,80,42,81,43,81,43,81,46,84,46,84,46,84,46,84,46,84,46,84,47,84,47,84,48,84,49,85,49,85,50,85,49,84,50,84,51,85,50,86,50,86,50,87,51,87,51,87,52,88,52,88,52,88,53,89,54,89,54,90,55,90,55,90,55,91,55,91,55,91,42,91,42,91,41,90,40,88,39,88,38,86,36,85,36,84,35,84,36,83,35,83,35,82,35,80,35,80,35,80,35,80,35,79,36,79,36,79,37,76,37,76,37,75,38,75,38,75,38,75,39,75,39,75" href="geofinder?clip=counties:Monterey&amp;lbl=Monterey">

<area shape="poly" title="Napa" coords="31,46,31,46,32,46,32,49,33,49,33,50,33,50,33,50,34,51,33,53,34,54,34,54,33,54,33,55,32,55,33,55,32,55,33,56,32,56,33,56,30,56,31,56,31,56,31,56,31,55,30,55,30,54,29,52,29,52,29,52,29,51,28,50,28,50,28,50,28,49,30,48,30,47,30,47,31,47,30,46,31,46" href="geofinder?clip=counties:Napa&amp;lbl=Napa">

<area shape="poly" title="Nevada" coords="50,37,51,37,51,37,52,38,58,38,58,40,50,40,48,41,48,42,48,42,47,43,47,43,46,44,45,44,45,44,44,44,43,44,43,41,44,41,44,40,44,40,44,39,45,39,45,39,46,39,46,39,47,38,49,38,50,37" href="geofinder?clip=counties:Nevada&amp;lbl=Nevada">

<area shape="poly" title="Orange" coords="85,118,86,119,87,121,88,121,88,121,89,122,90,122,88,124,88,125,88,125,87,126,87,125,86,125,86,125,85,124,84,123,84,123,84,122,84,123,83,123,84,123,83,123,83,123,83,123,83,123,82,122,81,121,82,122,82,121,81,121,81,121,81,121,81,121,81,121,81,121,81,121,81,121,82,120,83,119,83,118,85,118" href="geofinder?clip=counties:Orange&amp;lbl=Orange">

<area shape="poly" title="Placer" coords="49,41,50,40,51,40,58,40,58,43,56,43,56,44,55,44,53,44,52,45,51,46,50,45,50,45,50,45,49,44,49,44,48,45,48,45,48,45,47,45,47,45,46,46,46,46,46,46,46,46,46,47,45,47,46,47,45,47,45,47,45,48,45,49,41,48,41,48,41,48,41,45,42,45,42,44,43,44,44,44,45,44,45,44,46,44,47,43,47,43,49,41" href="geofinder?clip=counties:Placer&amp;lbl=Placer">

<area shape="poly" title="Plumas" coords="43,23,46,23,46,26,46,26,46,26,47,26,47,26,47,26,48,27,48,27,48,26,49,26,49,25,49,25,51,25,51,26,51,26,51,26,52,26,53,27,53,27,53,27,53,27,54,28,55,28,55,29,56,28,56,30,56,30,56,30,57,31,57,31,57,33,56,34,56,34,56,34,51,34,50,34,49,34,49,34,49,33,48,33,48,33,47,34,47,34,47,35,46,36,46,35,45,35,45,35,44,34,44,34,43,33,43,32,43,32,43,32,42,32,42,31,42,31,42,31,42,31,42,29,43,28,42,28,42,28,42,27,42,27,42,27,43,27,43,26,43,25,43,25,42,25,41,24,41,24,41,23,41,23,41,23,43,23" href="geofinder?clip=counties:Plumas&amp;lbl=Plumas">

<area shape="poly" title="Riverside" coords="126,114,126,115,125,115,125,116,124,117,125,117,125,117,125,118,125,118,125,118,125,120,125,120,125,121,125,121,125,121,125,122,125,122,124,123,124,124,92,125,92,125,90,125,90,124,88,124,88,124,90,122,89,122,88,121,88,121,87,121,86,119,86,119,87,119,86,118,87,118,87,118,88,117,88,117,90,117,90,117,92,117,92,117,95,117,95,117,115,116,115,115,126,114" href="geofinder?clip=counties:Riverside&amp;lbl=Riverside">

<area shape="poly" title="Sacramento" coords="41,48,45,49,45,48,46,52,46,55,46,55,44,55,43,55,42,56,42,56,41,55,41,55,40,56,40,57,40,57,40,57,39,58,38,58,37,59,36,58,37,58,38,58,38,58,38,57,39,56,39,55,40,54,40,54,41,53,40,53,41,52,41,52,40,52,40,52,41,51,40,51,41,50,40,50,40,49,40,49,39,49,40,48,41,48" href="geofinder?clip=counties:Sacramento&amp;lbl=Sacramento">

<area shape="poly" title="San Benito" coords="44,74,43,75,44,75,44,75,45,76,47,77,51,81,51,84,50,84,50,84,50,84,49,84,49,85,49,85,49,85,48,84,47,84,47,84,46,84,46,84,46,84,46,84,46,84,46,84,43,81,43,81,42,81,43,80,42,80,42,79,42,79,41,78,40,78,41,78,41,77,40,77,39,75,39,75,40,75,40,74,41,74,41,74,44,74" href="geofinder?clip=counties:San Benito&amp;lbl=San Benito">

<area shape="poly" title="San Bernardino" coords="110,90,122,101,123,103,123,104,124,105,125,105,125,106,125,107,125,107,126,108,126,109,126,109,128,110,128,110,129,111,129,112,129,112,128,113,126,114,126,114,115,115,115,116,95,117,95,117,92,117,92,117,90,117,90,117,88,117,88,117,87,118,87,118,86,118,87,119,86,119,86,119,85,118,85,118,85,117,85,117,85,117,86,117,86,113,86,109,86,105,86,105,86,93,86,93,86,93,86,93,86,92,86,92,86,91,109,90,109,90,110,90" href="geofinder?clip=counties:San Bernardino&amp;lbl=San Bernardino">
<!--
<area shape="poly" title="San Diego" coords="" href="geofinder?clip=counties:San Diego&amp;lbl=San Diego">
-->
<area shape="poly" title="San Francisco" coords="30,60,30,60,31,63,29,63,29,62,29,62,29,61,30,61,30,60" href="geofinder?clip=counties:San Francisco&amp;lbl=San Francisco">

<area shape="poly" title="San Joaquin" coords="46,55,47,58,47,63,46,63,46,63,45,63,45,63,44,63,44,64,44,63,44,64,44,64,41,67,40,66,40,66,40,62,40,61,40,61,40,60,40,59,40,59,40,59,40,59,40,58,40,58,40,58,40,57,40,57,40,57,40,57,40,56,41,55,41,55,42,56,42,56,43,55,44,55,46,55,46,55" href="geofinder?clip=counties:San Joaquin&amp;lbl=San Joaquin">

<area shape="poly" title="San Luis Obispo" coords="55,91,56,91,56,94,57,94,57,95,58,96,58,97,59,97,59,98,60,98,60,99,62,99,62,100,63,100,63,102,64,102,64,102,64,102,64,104,64,104,62,103,60,103,60,103,59,102,59,102,58,102,57,101,56,102,56,102,56,102,56,102,55,103,55,103,55,103,54,103,54,103,54,104,54,104,53,103,52,103,50,104,50,103,50,102,50,101,49,100,49,101,48,100,47,99,48,98,48,98,48,98,48,98,48,98,48,97,47,97,46,96,45,95,44,94,43,93,43,93,42,92,42,92,42,91,55,91" href="geofinder?clip=counties:San Luis Obispo&amp;lbl=San Luis Obispo">

<area shape="poly" title="San Mateo" coords="29,63,31,63,34,66,32,67,32,68,33,69,32,69,33,69,33,70,32,70,32,71,31,71,31,72,31,72,30,71,30,70,30,70,30,68,29,66,29,66,29,66,29,66,29,65,29,65,29,64,29,63" href="geofinder?clip=counties:San Mateo&amp;lbl=San Mateo">

<area shape="poly" title="Santa Barbara" coords="64,104,65,104,65,111,65,111,65,112,64,112,63,112,62,112,61,112,60,112,59,112,58,111,56,111,54,111,52,111,52,111,52,110,51,109,50,110,50,109,51,107,50,107,51,105,50,104,50,104,50,103,50,104,52,103,53,103,54,104,54,104,54,103,54,103,55,103,55,103,55,103,56,102,56,102,56,102,56,102,57,101,58,102,59,102,59,102,60,103,60,103,62,103,64,104,64,104" href="geofinder?clip=counties:Santa Barbara&amp;lbl=Santa Barbara">

<area shape="poly" title="Santa Clara" coords="41,67,41,67,41,67,41,67,41,68,42,68,41,68,42,69,41,69,41,70,41,70,42,71,42,71,42,71,43,71,43,71,44,72,43,72,44,73,44,73,43,73,43,74,43,74,44,74,41,74,41,74,40,74,39,75,39,75,39,74,39,75,38,74,38,74,38,73,37,73,34,71,34,70,33,70,33,69,33,69,32,69,33,69,32,68,32,67,34,66,34,67,35,67,36,67,36,66,41,67" href="geofinder?clip=counties:Santa Clara&amp;lbl=Santa Clara">

<area shape="poly" title="Santa Cruz" coords="33,69,33,70,34,71,34,71,37,73,38,73,38,74,38,74,39,75,39,74,39,75,39,75,39,75,39,75,38,75,38,75,38,75,38,75,38,75,37,75,37,76,36,74,35,74,35,74,34,74,34,74,34,74,33,74,32,73,31,72,31,71,32,71,32,70,33,70,33,69" href="geofinder?clip=counties:Santa Cruz&amp;lbl=Santa Cruz">

<area shape="poly" title="Shasta" coords="42,12,43,12,43,16,43,16,43,23,40,23,40,23,39,23,37,23,37,23,35,23,33,24,34,24,33,24,33,24,33,24,32,24,31,24,30,24,28,25,27,24,27,24,26,24,25,25,25,25,24,25,24,24,24,24,24,23,25,23,26,22,27,22,27,21,28,21,28,20,27,20,28,20,27,19,28,19,28,19,28,18,28,18,28,17,29,16,29,16,29,15,30,15,30,14,30,14,31,13,31,12,30,12,42,12" href="geofinder?clip=counties:Shasta&amp;lbl=Shasta">

<area shape="poly" title="Sierra" coords="58,34,58,38,52,38,52,38,52,37,51,37,49,38,47,38,46,39,46,37,46,36,46,36,47,35,47,34,47,34,48,33,48,33,49,33,49,34,49,34,50,34,51,34,58,34,58,34" href="geofinder?clip=counties:Sierra&amp;lbl=Sierra">

<area shape="poly" title="Siskiyou" coords="19,0,32,0,42,0,42,12,30,12,30,12,30,11,30,11,30,10,31,10,30,9,29,10,29,10,29,10,28,11,28,11,27,11,27,12,26,12,26,12,25,12,25,12,25,13,26,14,26,14,26,15,25,14,24,15,24,14,23,13,22,13,21,12,21,12,20,12,20,11,20,10,20,10,20,10,19,9,18,9,18,8,18,7,18,7,17,6,17,6,17,6,17,5,17,5,17,5,18,4,17,3,18,3,17,2,18,2,18,1,18,1,19,1,19,1,19,0,19,0,19,0" href="geofinder?clip=counties:Siskiyou&amp;lbl=Siskiyou">

<area shape="poly" title="Solano" coords="35,51,36,51,36,51,37,51,38,51,38,51,38,54,40,54,39,56,38,57,38,58,38,58,37,58,37,58,36,58,36,58,34,58,33,58,32,58,31,58,30,56,33,56,32,56,33,56,32,55,33,55,32,55,33,55,33,54,34,54,34,54,33,53,34,51,34,51,35,52,35,51" href="geofinder?clip=counties:Solano&amp;lbl=Solano">

<area shape="poly" title="Sonoma" coords="26,46,27,47,27,48,28,48,28,49,28,50,28,50,29,51,29,52,29,52,29,52,30,54,30,54,30,55,31,55,31,56,31,56,31,56,30,56,31,58,29,57,28,56,28,56,26,56,24,54,24,54,24,54,23,54,23,54,23,54,23,54,23,54,23,54,22,54,23,53,22,52,20,50,19,48,18,47,18,47,18,47,19,47,19,47,22,47,22,46,23,46,23,46,26,46" href="geofinder?clip=counties:Sonoma&amp;lbl=Sonoma">

<area shape="poly" title="Stanislaus" coords="47,58,50,61,53,64,46,68,46,68,47,68,46,68,47,69,44,72,43,71,43,71,42,71,42,71,42,71,41,70,41,70,41,69,42,69,41,68,42,68,41,68,41,67,41,67,41,67,44,64,44,64,44,63,44,64,44,63,45,63,45,63,46,63,46,63,47,63,47,58" href="geofinder?clip=counties:Stanislaus&amp;lbl=Stanislaus">

<area shape="poly" title="Sutter" coords="39,40,39,40,39,40,39,40,39,41,40,41,39,41,40,41,40,43,40,43,40,43,40,44,40,44,40,45,40,45,42,44,42,45,41,45,41,48,41,48,41,48,40,48,40,48,39,47,39,48,39,48,39,48,39,48,39,48,39,48,39,47,38,47,38,46,38,46,38,46,37,46,37,46,37,45,37,45,37,44,37,44,37,44,37,43,36,43,36,42,36,42,36,42,36,42,36,41,36,41,36,40,39,40" href="geofinder?clip=counties:Sutter&amp;lbl=Sutter">

<area shape="poly" title="Tehama" coords="41,23,41,23,41,23,41,24,41,24,42,25,43,25,43,25,43,26,43,27,42,27,42,27,42,27,42,28,40,28,40,29,40,29,39,30,39,30,38,31,35,31,35,32,35,32,25,32,25,32,25,31,25,30,25,29,25,29,25,28,25,28,25,27,24,27,25,27,25,26,24,25,25,25,25,25,26,24,27,24,27,24,28,25,30,24,31,24,32,24,33,24,33,24,33,24,34,24,33,24,35,23,37,23,37,23,39,23,40,23,40,23,41,23" href="geofinder?clip=counties:Tehama&amp;lbl=Tehama">

<area shape="poly" title="Trinity" coords="30,12,31,12,31,13,30,14,30,14,30,15,29,15,29,16,29,16,28,17,28,18,28,18,28,19,28,19,27,19,28,20,27,20,28,20,28,21,27,21,27,22,26,22,25,23,24,23,24,24,24,24,24,25,25,26,25,27,24,27,25,27,25,28,25,28,25,28,25,29,25,30,18,29,18,18,18,17,18,17,18,17,18,16,18,16,18,16,18,16,18,15,18,15,18,15,19,16,20,15,20,15,20,14,20,14,20,13,20,13,20,13,20,13,20,12,20,12,20,12,21,12,21,12,22,13,23,13,24,14,24,15,25,14,26,15,26,14,26,14,25,13,25,12,25,12,26,12,26,12,27,12,27,11,28,11,28,11,29,10,29,10,29,10,30,9,31,10,30,10,30,11,30,11,30,12,30,12" href="geofinder?clip=counties:Trinity&amp;lbl=Trinity">

<area shape="poly" title="Tulare" coords="77,77,78,78,77,78,78,78,77,79,78,79,78,79,78,80,78,80,79,81,79,81,79,81,79,82,80,82,80,82,80,82,80,83,80,84,80,85,81,86,81,86,81,87,81,87,81,88,82,88,81,89,82,89,82,90,82,90,82,91,63,91,63,84,64,84,64,82,63,82,63,81,63,81,63,81,64,80,66,80,66,79,70,79,70,77,75,77,77,77" href="geofinder?clip=counties:Tulare&amp;lbl=Tulare">

<area shape="poly" title="Tuolumne" coords="58,53,59,54,60,53,61,53,61,54,62,54,62,55,62,55,62,55,62,56,62,56,62,56,63,56,63,57,63,57,64,57,64,57,64,58,64,58,65,57,66,58,66,59,66,59,66,60,67,61,67,61,67,61,67,62,67,62,66,63,66,62,65,62,65,61,65,61,65,61,64,61,64,61,63,60,63,60,63,61,63,61,62,61,62,62,62,62,60,63,59,62,59,63,59,63,57,62,56,62,56,62,56,62,55,62,55,63,55,63,55,63,54,63,54,64,54,64,54,64,53,64,54,64,53,64,50,61,51,61,52,60,52,60,52,59,52,59,52,59,53,59,53,58,56,54,58,53" href="geofinder?clip=counties:Tuolumne&amp;lbl=Tuolumne">

<area shape="poly" title="Ventura" coords="71,106,74,113,75,114,74,114,74,115,73,115,71,116,71,117,69,116,69,116,68,115,67,115,67,115,67,115,67,115,67,115,67,114,67,114,67,114,67,114,64,112,65,112,65,111,65,111,65,104,65,104,65,105,67,105,67,105,67,105,67,106,70,106,70,106,71,106" href="geofinder?clip=counties:Ventura&amp;lbl=Ventura">

<area shape="poly" title="Yolo" coords="37,45,37,46,37,46,37,46,38,46,38,46,38,46,38,47,39,47,39,48,39,48,39,48,39,48,39,48,39,47,40,48,40,48,39,49,40,49,40,49,40,50,41,50,40,51,41,51,40,52,40,52,41,52,41,52,40,53,41,53,40,54,40,54,40,54,38,54,38,51,38,51,37,51,37,51,36,51,35,52,34,51,34,51,33,50,33,50,33,50,33,49,32,49,32,46,31,46,30,45,30,45,37,45" href="geofinder?clip=counties:Yolo&amp;lbl=Yolo">

<area shape="poly" title="Yuba" coords="46,36,47,35,46,36,46,36,46,37,46,39,45,39,45,39,44,39,44,40,44,40,44,41,43,41,43,44,43,44,42,44,40,45,40,45,40,44,40,44,40,44,40,43,40,43,40,43,40,41,39,41,40,41,39,41,40,40,40,40,42,39,43,38,43,38,43,37,43,37,45,37,45,36,45,36,45,36,46,36,46,36" href="geofinder?clip=counties:Yuba&amp;lbl=Yuba">

</MAP>
	   ];
}

1
