#!/usr/bin/perl
##
## Spatial search for catalog entries
##
use CGI qw/:standard/;
use DBI;
use pfLib;
use LWP::UserAgent;
use HTTP::Status;
use HTTP::Response;
use Data::Serializer::JSON;

##sanitize;

##open (LOGFILE, '>>/var/log/ceicQuerySpatial.log');

my $xmlBase = "http://atlas.ca.gov/xml/orig/";
my $htmlBase = "http://atlas.ca.gov/catalog-ceic/";

##print "\n$ENV{QUERY_STRING}\n";
my $clip = lc(param('clip'));
my $ext = param('ext');
my $bbox = param('bbox');
my $srid = param('srid');
my $topic = lc(param('topic'));
my $cat = lc(param('cat'));
my $content = lc(param('content'));
my $term = lc(param('term'));
my $title = lc(param('title'));
my $titlesub = lc(param('titlesub'));
my $phrase = lc(param('phrase'));
my $org = lc(param('org'));
my $role = lc(param('role'));
my $issue = lc(param('issue'));
my $projectType = lc(param('projectType'));
my $fundType = lc(param('fundType'));
my $fmt = lc(param('fmt'));
my $node = param('node');
my $nodeshortname = param('nodeshortname');
my $collection = param('collection');
my $qStrOrder = param('order');
my $callback = param('callback');
my $linked = param('linked');
my $statewide = param('statewide');
my $atlas = param('atlas');
my $limit = param('limit');

## Paging???
##my $order = '';
#my $page_size = 160;
#my $page_num = param(page);
#my $offset = ($page_num - 1) * $page_size;

$clip = '' if (!($clip =~ /.:./));

$statewide = 1 if (!($clip) && !($bbox));
## some spatial ref sys tranforms to catch depricated values
$srid = 3310 if ($srid eq 2);
$srid = 3309 if ($srid eq 1);
$srid = 4269 if (!($srid));
$limit = 200 if (!($limit));

$term = $phrase if (!($term));

my $regterm = $term; 
$regterm =~ s/\s/&/g;

## Check for no query params
if (!($clip || $phrase || $ext || $topic || $cat || $term || $title || $titlesub || $org || $bbox || $issue || $projectType || $content || $collection || ($node) || $nodeshortname)) {
    print qq/{"ResultSet":{"Where":"No Filters","Result":[]}}/;
    exit;
}

db_connect();
#my $dbh3= DBI->connect("dbi:Pg:dbname=gforge");
#$dbh3->do("SET search_path TO ceic, public");

my $st,$sql,$cnt,$where;
my @names;

my @idArray = (); # results in array
my @titleArray = (); # results in array
my @catArray = (); # results in array
my @srcArray = (); # results in array
my @lonArray = (); # results in array
my @latArray = (); # results in array
my @linkArray = (); # results in array
my @xmlArray = (); # results in array

my $westbc,$eastbc,$northbc,$southbc;

## prepare the name query
if ($ext) {
    my @bc = split(',',$ext);
    $eastbc = $bc[0];
    $westbc = $bc[1];
    $northbc = $bc[2];
    $southbc = $bc[3];
}
elsif ($bbox) {
    my @bc = split(',',$bbox);
    $westbc = $bc[0];
    $eastbc = $bc[1];
    $northbc = $bc[2];
    $southbc = $bc[3];
    ## need to transform from 3310 albers to lat lon
    transformBBox(\$eastbc,\$westbc,\$northbc,\$southbc,$srid,$srid);
#    print "$eastbc,$westbc,$northbc,$southbc";
#    exit;
}
elsif ($clip) {
    ## USGS quad o series id with or without the leading "o"
    $clip =~ s/grid:o/grid:/;
    # Create a user agent object
    $ua = new LWP::UserAgent;
    $ua->agent("ceicSearch/1.1 " . $ua->agent);

    # Create a request for the list of catalogs
    my $req = new HTTP::Request GET => "http://atlas.ca.gov/cgi-bin/api/clipInfo2?clip=$clip";
    # Pass request to the user agent and get a response back
    $res = $ua->request($req);
    my $json = new JSON;
    my $RS = $json->decode($res->content);
    $eastbc = $RS->{ResultSet}->{Result}->[0]->{eastbc};
    $westbc = $RS->{ResultSet}->{Result}->[0]->{westbc};
    $northbc = $RS->{ResultSet}->{Result}->[0]->{northbc};
    $southbc = $RS->{ResultSet}->{Result}->[0]->{southbc};
    ## need to transform from 3310 albers to lat lon
    transformBBox(\$eastbc,\$westbc,\$northbc,\$southbc,4269,$srid);
#    print "$eastbc,$westbc,$northbc,$southbc";
#    exit;
}

##$sql = qq[SELECT distinct d.id, d.title, d.category, d.centroid[0], d.centroid[1] ,n.shortname,d.link_url,'$xmlBase' || n.shortname || '/' || filename as meta_url,d.rank ];

if ($srid eq 3310 || $srid eq 3309) {
    $sql = qq[SELECT distinct d.id, d.title, d.category, d.centroid3310[0] AS lat, d.centroid3310[1] AS lon, n.shortname,d.link_url,'$xmlBase' || n.shortname || '/' || filename as meta_url,d.rank ];}
else {
    $sql = qq[SELECT distinct d.id, d.title, d.category, d.centroid[0] AS lat, d.centroid[1] AS lon, n.shortname,d.link_url,'$xmlBase' || n.shortname || '/' || filename as meta_url,d.rank ];
}

$from = qq[FROM dataset d, ceic_node n ];
$where = qq[WHERE (d.status = 'public') AND d.ceic_node_id = n.id ];


if ($cat) {
    if ($cat =~ /gis/) { $cat .= ",vector digital data,raster digital data,map,spatial data,wms,gis-data,gis-map,gis-service"};
    if ($cat =~ /other/) { $cat .= ",website,computer file,tabular digital data"};
    if ($cat =~ /org/) { $cat .= ",organization"};
    my @type = split(/,/, $cat);
    my $quotedTypes = uc(join("','",@type)); 
    $where .= qq[ and upper(d.category) IN ('$quotedTypes')];
    $whereText .= qq[resource type IN ('$cat') ];
}
if ($term || $phrase) {
    if ($srid eq 3310 || $srid eq 3309) {
	$sql = qq[SELECT d.id, f.headline, d.category, d.centroid3310[0] AS lat, d.centroid3310[1] AS lon, n.shortname,d.link_url,'$xmlBase' || n.shortname || '/' || filename as meta_url,d.rank ];}
    else {
	$sql = qq[SELECT d.id, f.headline, d.category, d.centroid[0] AS lat, d.centroid[1] AS lon, n.shortname,d.link_url,'$xmlBase' || n.shortname || '/' || filename as meta_url,d.rank ];
    }

##        $sql = qq[SELECT d.id, f.headline, d.category, d.centroid[0], d.centroid[1], n.shortname,d.link_url,'$xmlBase' || n.shortname || '/' || filename as meta_url, f.rank, d.rank ];


    $from .= qq[, dresults('$regterm') f ];

    if ($phrase) {
	$where .= qq[ and f.id = d.id and (lower(d.title) ~ '.*$phrase.*' or lower(abstract) ~ '.*$phrase.*' or lower(searchterms) ~ '.*$phrase.*') ];
    }
    else {
	$where .= qq[ and f.id = d.id ];
    }
    $whereText .= qq[term of '$term' ];
    $order = qq[  f.rank DESC ];
}

if ($topic || $issue) {
    $from .= qq[, dataset_theme t ];
    $where .= qq[ AND t.dataset_id = d.id];
    if ($topic) {
       my @top = split(/,/, $topic);
       my $quotedTopics = uc(join("','",@top)); 
#       $where .= qq[ AND upper(t.themekey) IN ('$quotedTopics') AND upper(t.category) = 'ASSIGNED'];
#       $where .= qq[ AND upper(t.themekey) IN ('$quotedTopics') AND upper(t.themekt) = 'TOPIC'];
       $where .= qq[ AND upper(t.themekey) IN ('$quotedTopics')];
    }
    if ($issue) {
       my @top = split(/,/, $issue);
       my $quotedTopics = uc(join("','",@top)); 
       $where .= qq[ AND upper(t.themekey) IN ('$quotedTopics') AND upper(t.thesaurusuri) LIKE '%RESOURCE_ISSUE'];
    }
}

if ($projectType) {
    $from .= qq[, dataset_content c ];
    $where .= qq[ AND c.dataset_id = d.id];
    $where .= qq[ AND lower(c.content) LIKE '$projectType%'];
}

if ($content) {
    $from .= qq[, dataset_content c ];
    $where .= qq[ AND c.dataset_id = d.id];
    my @cnt = split(/,/, $content);
    my $quotedContents = uc(join("','",@cnt)); 
    $where .= qq[ AND upper(c.content) IN ('$quotedContents')];
}

if ($fundType) {
    $from .= qq[, dataset_funding f ];
    $where .= qq[ AND f.dataset_id = d.id ];
    $where .= qq[ AND lower(f.program_type) LIKE '$fundType%'];
}

if ($collection) {
    $from .= qq[, ds_collection coll ];
    $where .= qq[ AND coll.dataset_id = d.id];
    $where .= qq[ AND coll.collection_id = $collection];
}

if ($node) {
    $where .= qq[ and d.ceic_node_id IN ($node) ];
    $whereText .= qq[ CEIC Node IN $node ];
}

if ($nodeshortname) {
    $where .= qq[ and d.nodeshortname = '$nodeshortname' ];
    $whereText .= qq[ CEIC Node Name = $nodeshortname ];
}

## if ($statewide) {
if (0) {
    $where .= qq[ and d.statewide ];
    $whereText .= qq[ statewide dataset ];
}

if ($title) {
    $where .= qq[ and upper(d.title) like upper('$title%')];
    $whereText .= qq[ Title like $title ];
}

if ($titlesub) {
    $where .= qq[ and upper(d.title) like upper('%$titlesub%')];
    $whereText .= qq[ Title like $titlesub ];
}

if ($linked) {
    $where .= qq[ and (d.link_url_resp = '200' OR d.dist_url_resp = '200')];
}

if ($atlas) {
    $where .= qq[ and (d.gforge_group_id is not Null OR d.category = 'gis-service')];
}

if ($org) {
    $sql .= qq[, o.role ];
    $from .= qq[, dataset_org o ];
    $org = smartName($org);
    if ($role) {
	if ($role eq 'participant') {
         $where .= qq[ AND o.dataset_id = d.id and (upper(o.shortname) LIKE upper('%$org%') AND upper(o.role) NOT IN ('FUNDER','LEAD AGENCY'))];
	}
	else {
	  my @roles = split(/,/,$role);
	  my $roleStr = "'" . uc(join("','",@roles)) . "'";

         $where .= qq[ AND o.dataset_id = d.id and (upper(o.shortname) LIKE upper('%$org%') AND upper(o.role) IN (] . $roleStr . '))';
	}
    }
    else {
       $where .= qq[ AND o.dataset_id = d.id and upper(o.shortname) LIKE upper('%$org%') ];
   }
    $whereText .= qq[ AND organization = $org ];
}

if ($eastbc && $westbc && $northbc && $southbc) {

    if ($srid eq 3310 || $srid eq 3309) {
	$where .= qq[ AND '(($eastbc,$northbc),($westbc,$southbc))'::BOX @> box(point (d.centroid3310), point (d.centroid3310)) ];
    }
    else {
	$where .= qq[ AND '(($eastbc,$northbc),($westbc,$southbc))'::BOX @> box(point (d.centroid), point (d.centroid)) ];
    }
    $whereText .= qq[ intersecting map extent ]; 
#    print "\n$where\n";

}

if (!($qStrOrder)) {
    $order = qq[  d.title ] if (!($order));
}
else {
    $qStrOrder = 'd.last_update DESC' if ($qStrOrder =~ /recent/i);  
    $order = qq[  $qStrOrder ];
}

#$sql .= $from . $where . $order . "OFFSET $offset LIMIT $page_size";

if ($statewide) {
    $sql .= $from . $where . "ORDER BY d.rank," . $order . " LIMIT $limit";
}
else {
    $sql .= $from . $where . "ORDER BY " . $order . " LIMIT $limit";
}

my $sqlCount = 'select count(*) ' . $from . $where;
$st = $dbh3->prepare($sqlCount);
$st->execute();
@row = $st->fetchrow_array;
my $recCount = $row[0];

## Log the SQL
##print LOGFILE "\n$sqlCount\n";
##print LOGFILE "\n$sql\n";
print "\n$sql\n";

my @timeData = localtime(time);
my $t = $timeData[1] . ':' . $timeData[0];
##print LOGFILE "\n$t";

$st = $dbh3->prepare($sql);
$st->execute();

@timeData = localtime(time);
$t = $timeData[1] . ':' . $timeData[0];
##print LOGFILE " -- $t\n";
##close(LOGFILE);
while (@row = $st->fetchrow_array) {
     ## purge all double quotes!
    foreach $i (0,1,2,3,4,5,6) {
        $row[$i] =~ s/(['"])/\\$1/g;
        $row[$i] =~ s/\n//g;
        $row[$i] = 'Null' if ($row[$i] =~ /^HASH/);
#        $row[$i] =~ s/([()])/-/g;
#	$row[$i] =~ s/\"/\'/g;
        $row[$i] = '' if ($row[$i] =~ /^\\/);
    }

    ## preventing some repeats
    my $currentIds = join(' ',@idArray);
    my $id = $row[0];
    if (!($currentIds =~ /$id/)) {

	push(@idArray,"$row[0]");
	push(@titleArray,"$row[1]");
	push(@catArray,"$row[2]");

	if (!($srid =~ /4269|4326/)) {
	    ## need to transform from lat lon decimal to request srid
#	    transformBBox(\$row[3],\$row[3],\$row[4],\$row[4],4269,$srid);
	}

	push(@lonArray,"$row[3]");
	push(@latArray,"$row[4]");

	push(@srcArray,"$row[5]");
	push(@linkArray,"$row[6]");
	push(@xmlArray,"$row[7]");
	push(@roleArray,"$row[8]") if ($org);
   }
}

if ($fmt eq 'atom') {
    use ceicAtom;
    
    my $output;
    my $writer = new XML::Writer(OUTPUT => \$output);
    
    $query = new CGI;
    print $query->header(-type=>'application/atom+xml');

    $writer->xmlDecl("UTF-8");
    $writer->startTag("feed","xmlns" => "http://www.w3.org/2005/Atom", "xmlns:geo" => "http://www.geo.org/geo", "xml:lang" => "en-ud");

    writeAtomFeed($writer);

    my $quotedIDs = uc(join("','",@idArray)); 
    if ($srid) {
	$sql = qq[select '$htmlBase' || n.shortname || '/' || d.shortname || '.html',d.title,substr(d.abstract,0,255),d.category,date_trunc('hour',d.last_update) || 'Z',d.link_url,date_trunc('year',d.last_update) || 'Z',astext(transform(centroid(envelope(d.geom)),$srid)),'$xmlBase' || n.shortname || '/' || filename as meta_url from dataset d, ceic_node n where d.ceic_node_id = n.id and d.id IN ('$quotedIDs')] . qq[ ORDER BY d.last_update DESC];
    }
    else {
	$sql = qq[select d.id,d.title,substr(d.abstract,0,255),d.category,date_trunc('hour',d.last_update) || 'Z',d.link_url,date_trunc('year',d.last_update) || 'Z',astext(transform(centroid(envelope(d.geom)),4269)),'$xmlBase' || n.shortname || '/' || filename as meta_url from dataset d, ceic_node n where d.ceic_node_id = n.id and d.id IN ('$quotedIDs')] . qq[ ORDER BY d.last_update DESC];
    }
    $st = $dbh3->prepare($sql);
    $st->execute();
    while (@row = $st->fetchrow_array) {
	writeAtomEntry($writer,\@row)
    }
    $writer->endTag("feed");
    $writer->end();
    print $output;
}
elsif ($fmt eq 'rss') {
##    use ceicAtom;
    
    my $output;
    my $writer = new XML::Writer(OUTPUT => \$output);
    
    $query = new CGI;
    print $query->header(-type=>'application/rss+xml');

    $writer->xmlDecl("UTF-8");
    $writer->startTag("rss","version" => "2.0", "xmlns:geo" => "http://www.w3.org/2003/01/geo/wgs84_pos#");

    $writer->startTag("channel");

    writeRSSChannel($writer);

    my $quotedIDs = uc(join("','",@idArray)); 
    if ($srid) {
	$sql = qq[select d.id,d.title,substr(d.abstract,0,255),d.category,date_trunc('hour',d.last_update) || 'Z',d.link_url,date_trunc('year',d.last_update) || 'Z',astext(transform(centroid(envelope(d.geom)),$srid)),'$xmlBase' || n.shortname || '/' || filename as meta_url from dataset d, ceic_node n where d.ceic_node_id = n.id and d.id IN ('$quotedIDs')] . qq[ ORDER BY d.last_update DESC];
    }
    else {
	$sql = qq[select d.id,d.title,substr(d.abstract,0,255),d.category,date_trunc('hour',d.last_update) || 'Z',d.link_url,date_trunc('year',d.last_update) || 'Z',astext(transform(centroid(envelope(d.geom)),4269)),'$xmlBase' || n.shortname || '/' || filename as meta_url from dataset d, ceic_node n where d.ceic_node_id = n.id and d.id IN ('$quotedIDs')] . qq[ ORDER BY d.last_update DESC];

    }
    $st = $dbh3->prepare($sql);
    $st->execute();
    while (@row = $st->fetchrow_array) {
	writeRSSEntry($writer,\@row)
    }

    $writer->endTag('channel');
    $writer->endTag("rss");
    $writer->end();
    print $output;
}
else {
    $query = new CGI;
    print $query->header(-type=>'application/json');
    my $jsonText;

    $whereText = '';
    my $jsonText = '';
    if ($callback) { $jsonText = qq/$callback(/ }

    $jsonText .=  qq/{"ResultSet":{"ResultType":"ds","Where":"$whereText","recCount":"$recCount","Result":[/;
    for $i (0..((@idArray) - 1)) {
	$jsonText .= "{\"id\":\"$idArray[$i]\",\"label\":\"$titleArray[$i]\",\"cat\":\"$catArray[$i]\"";
	$jsonText .= ",\"role\":\"$roleArray[$i]\"" if ($org);
	$jsonText .= ",\"lon\":\"$lonArray[$i]\"";
	$jsonText .= ",\"lat\":\"$latArray[$i]\"";
	$jsonText .= ",\"src\":\"$srcArray[$i]\"";
	$jsonText .= ",\"link\":\"$linkArray[$i]\"";
	$jsonText .= ",\"xml\":\"$xmlArray[$i]\"";
	$jsonText .= ",\"idx\":\"$i\"";
	$jsonText .= "},";
    }


    $jsonText =~ s/\,$//;
    $jsonText .= qq/]}}/;
    if ($callback) { $jsonText .= qq/)/ }
    print $jsonText;
}

sub smartName {

    my $name = shift;

    @parts = map ucfirst, split(' ',$name);
    $name = join(' ' , @parts);

    $name =~ s/^California //gi;
    $name =~ s/California/CA/gi;
    $name =~ s/Department Of //gi;
    $name =~ s/Town Of //gi;
    $name =~ s/County Of //gi;
    $name =~ s/City Of //gi;
    $name =~ s/Ca Dept. Of //gi;
    $name =~ s/County/Co/gi;
    $name =~ s/City/Ci/gi;
    $name =~ s/(Catalog|Catalogue)//gi;
    $name =~ s/Metropolitan/Metro/gi;
    $name =~ s/Association/Assoc/gi;
    $name =~ s/Department/Depart/gi;
    $name =~ s/District/Dist/gi;
    $name =~ s/Consortium/Consort/gi;
    $name =~ s/Commission/Comm/gi;
    $name =~ s/Division/Div/gi;
    $name =~ s/Environmental/Env/gi;
    $name =~ s/Program/Prog/gi;
    $name =~ s/Management/Man/gi;
    $name =~ s/Conservancy/Consrv/gi;
    $name =~ s/Government/Gov/gi;
    $name =~ s/Municipal/Muni/gi;
    $name =~ s/Transportation/Trans/gi;

    # Get rid of blanks and non word chars                                                                                                   
    $name =~ s/[\s,\.]//gi;
    $name =~ s/\W//g;

    return $name;

}

