#!/usr/bin/perl
use XML::DOM;
use XML::Writer;
use Digest::MD5 qw(md5_hex);

sub writeAtomFeed {

    $writer = shift;
    $writer->startTag("title");
    $writer->characters("California Data.Gov Catalog");
    $writer->endTag('title');
    $writer->startTag("id");
    $writer->characters("http://atlas.ca.gov/");
    $writer->endTag("id");
    $writer->startTag("updated");
    $writer->characters("");
    $writer->endTag("updated");
    $writer->startTag("link","href" => "http://atlas.ca.gov/");
    $writer->endTag('link');
    $writer->startTag("link","rel" => "self","href" => "http://atlas.ca.gov/cgi-bin/api/dgFeed");
    $writer->endTag('link');
    $writer->startTag("icon");
    $writer->characters("http://atlas.ca.gov/ceic/images/ceres_40.png");
    $writer->endTag('icon');
    $writer->startTag("author");
    $writer->startTag("name");
    $writer->characters("California Natural Resources Agency CERES Program");
    $writer->endTag('name');
    $writer->endTag('author');
    $writer->startTag("subtitle");
    $writer->characters("Selected Entries from the Cal-Atlas Catalog");
    $writer->endTag('subtitle');
} 

sub writeAtomEntry {

    $writer = shift;
    $dat = shift; 

    $writer->startTag("entry");
    $writer->startTag("link");
    $writer->characters("$dat->[5]");
    $writer->endTag("link");
    $writer->startTag("g:id");
    $writer->characters(md5_hex($dat->[5]));
    $writer->endTag("g:id");

# Using smaller md5sum id (above)
#    $writer->startTag("id");
#    $writer->characters("$dat->[0]");
#    $writer->endTag("id");
    $writer->startTag("title");
    $writer->characters($dat->[1]);
    $writer->endTag("title");
    $writer->startTag("summary");
    $writer->characters($dat->[2]);
    $writer->endTag("summary");
    $writer->startTag("updated");

    my $ts = $dat->[4];
    $ts =~ s/\s/T/;
    $writer->characters($ts);
    $writer->endTag("updated");

    $writer->startTag("g:item_type");
    $writer->characters("Dataset");
    $writer->endTag("g:item_type");

    $writer->startTag("g:image_link");
    $writer->characters("$dat->[9]");
    $writer->endTag("g:image_link");

    my $pt = $dat->[7];
    $pt =~ s/POINT\(//;
    $pt =~ s/\)//;
    my @ptArray = split(/\s/,$pt);

    $writer->startTag("g:longitude");
    $writer->characters("$ptArray[0]");
    $writer->endTag("g:longitude");


    $writer->startTag("g:latitude");
    $writer->characters("$ptArray[1]");
    $writer->endTag("g:latitude");

    $writer->startTag("c:other_link", "type" => "string");
    $writer->characters("$dat->[5]");
    $writer->endTag("c:other_link");

    $writer->startTag("c:resource_type", "type" => "string");
    $writer->characters("$dat->[3]");
    $writer->endTag("c:resource_type");

    $writer->startTag("c:agency", "type" => "string");
    $writer->characters("$dat->[11]");
    $writer->endTag("c:agency");

    $writer->startTag("c:smartorg", "type" => "string");
    $writer->characters(smartName($dat->[11]));
    $writer->endTag("c:smartorg");

    $writer->startTag("c:domain", "type" => "string");
    $writer->characters("$dat->[14]");
    $writer->endTag("c:domain");

    if (($email = $dat->[12]) || ($email = $dat->[13])) {
	
	$writer->startTag("c:contact_email", "type" => "string");
	$writer->characters("$email");
	$writer->endTag("c:contact_email");
    }

    ## create a DataGov category type from resource type
    my $dataGovType = DataGov_Content($dat->[3]);
    $writer->startTag("c:category_type", "type" => "string");
    $writer->characters("$dataGovType");
    $writer->endTag("c:category_type");

    my @topic = split(/,/,$dat->[8]);

    ## create new DataGov topics from included words
    my  $dataGovTopic = $dat->[8];
##    foreach $t (@dataGovTopics) {
       $writer->startTag("c:category", "type" => "string");
       $writer->characters("$dataGovTopic");
       $writer->endTag("c:category");
##    }

    foreach $t (@topic) {
       $writer->startTag("c:topic", "type" => "string");
       $writer->characters("$t");
       $writer->endTag("c:topic");
   }

    my @org = split(/,/,$dat->[10]);
    foreach $o (@org) {
       $writer->startTag("c:originator", "type" => "string");
       $writer->characters("$o");
       $writer->endTag("c:originator");
   }

    $writer->endTag("entry");
}

1;
