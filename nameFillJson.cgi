#!/usr/bin/perl
##
##
use CGI qw/:standard/;
use DBI;
print header;
print "foo";
__END__
sanitize;

my $name = param('name');
my $query = param('query');
my $src = param('src');
my $mode = param('mode');
my $node = param('node');
my $county = param('county');
my $placetype = param('type');
my $srid = param('srid');
my $typeList = uc(param('resourcetype'));
my $callback = param('callback');

my $dbh= DBI->connect("dbi:Pg:dbname=gis");
$dbh->do("SET search_path TO ceic, public");

my $st,$sql,$cnt,$where;
my @names;

$name = $query if ($query && (!($name)));
$name =~ s/\*/%/g;
$name =~ s/\?/_/g;

$typeList = "populated place" if ($placetype =~ /pop/i);
$typeList = "populated place','civil" if ($placetype =~ /jm/i);
$typeList = "civil" if ($placetype =~ /county/i);
$typeList = "schools','hospital','airport','building','dam" if ($placetype =~ /build/i);
$typeList = "locale','creek','basin','lake','river" if ($placetype =~ /locales/i);
$typeList = "stream','bay','canal','channel','falls','harbor','rapids','reservoir','creek','lake','river','sea','spring','swamp" if ($placetype =~ /water/i);

my @id = (); # results in array
my @label1 = (); # results in array
my @label2 = (); # results in array
my @url = (); # results in array
my @src = (); # results in array
my @cat = (); # results in array
my @lat = (); # results in array
my @lon = (); # results in array
my @pType = (); # results in array
my @shortname = (); # results in array

## prepare the name query
if ($src eq 'gaz') {
    if ($mode eq 'substr') {
	$where = qq[ where upper(description) LIKE upper('%$name%')];
    }
    else {
	$where = qq[ where upper(description) LIKE upper('$name%')];
    }
    $sql = qq/SELECT distinct description,coverage FROM gazetteer $where order by description limit 60/;
    $st = $dbh->prepare($sql);
    $st->execute();
    while (@row = $st->fetchrow_array) {
	$lbl = $row[0] . "(" . $row[1] . ")";
	push(@label1,$lbl);
	push(@id,"$row[1]:$row[0]");
    }
}
elsif ($src eq 'oesplaces') {
    if ($mode eq 'substr') {
	$where = qq[ where upper(label) LIKE upper('%$name%')];
    }
    else {
	$where = qq[ where upper(label) LIKE upper('$name%')];
    }

    $sql = qq/SELECT distinct label,clip,sort FROM oesplace $where order by sort,label limit 60/;
    $st = $dbh->prepare($sql);
    $st->execute();
    while (@row = $st->fetchrow_array) {
	$lbl = $row[0];
	push(@label1,$lbl);
	push(@id,"$row[1]");
    }
    if ((@row) < 5) {
	$where = qq[ where upper(feature_name) LIKE upper('$name%')];
	$sql = qq/SELECT distinct feature_name,feature_class,county_name,sortorder,feature_id FROM calgnis $where order by sortorder,feature_name,county_name limit 20/;
	$st = $dbh->prepare($sql);
	$st->execute();
	while (@row = $st->fetchrow_array) {
	    $lbl = $row[0] . " - " . $row[2] . "(" . $row[1] . ")";
	    push(@label1,$lbl);
	    push(@id,"gnis:$row[4]");
	}
    }
}
elsif ($src eq 'hydro') {

    if ($mode eq 'substr') {
	$where = qq[ where upper(name) LIKE upper('%$name%')];
    }
    else {
	$where = qq[ where upper(name) LIKE upper('$name%')];
    }

  ## prepare the name query
  $sql = qq/SELECT distinct name FROM hydrogp $where order by name limit 10/;

  $st = $dbh->prepare($sql);
  $st->execute();

  while (@row = $st->fetchrow_array) {
      $is_there = grep /$row[0]/, @id;
      if (!($is_there)) {
        push (@label1, "$row[0]");
        push (@id, "hydro:$row[0]");
      }
  }
  ## prepare the name query
  $sql = qq/SELECT distinct name FROM hydrogl $where order by name limit 10/;

  $st = $dbh->prepare($sql);
  $st->execute();

  while (@row = $st->fetchrow_array) {
      $is_there = grep /$row[0]/, @id;
      if (!($is_there)) {
        push (@label1, "$row[0]");
        push (@id, "hydro:$row[0]");
    }
  }
  ## force in "State Water Projects" label and "geoname:12307" (for cal statewide)
    if ($name =~ /^st/i) {
	push (@label1, "State Water Projects");
	push (@id, "geoname:12307");
    }
  ## when the id and labels are the same we can sort the arrays correctly.
  @id = sort(@id);
  @label1 = sort(@label1);

}
elsif ($src eq 'clip') {

## prepare the name query
$sql = qq/SELECT distinct description FROM county_view where upper(description) like upper('$name%') order by description limit 10/;

$st = $dbh->prepare($sql);
$st->execute();

while (@row = $st->fetchrow_array) {
    push (@label1, "$row[0] County");
    push (@id, "counties:$row[0]");
    push (@src, "California Counties");
}

## prepare the name query
$sql = qq/SELECT distinct name FROM city2k where upper(name) like upper('$name%') order by name limit 10/;

$st = $dbh->prepare($sql);
$st->execute();

while (@row = $st->fetchrow_array) {
    push (@label1, "$row[0]");
    push (@id, "city2k:$row[0]");
    push (@src, "Census2000 Urban Areas");
}

## prepare the name query
#$sql = qq/SELECT distinct name, clip FROM placeclip where type='Watershed' and upper(name) like upper('$name%') order by name limit 10/;

$sql = qq/SELECT distinct feature_name, 'gnis:' || feature_id FROM calgnis where feature_class = 'Populated Place' and upper(feature_name) like upper('$name%') order by feature_name limit 10/;

$st = $dbh->prepare($sql);
$st->execute();

while (@row = $st->fetchrow_array) {
    push (@label1, "$row[0]");
    push (@id, "$row[1]");
    push (@src, "GNIS California Place Names");
}

## prepare the name query
$sql = qq/SELECT distinct mapname,'grid:' || substr(ocode,2,7) FROM mapgrida where upper(mapname) like upper('$name%') order by mapname limit 20/;

$st = $dbh->prepare($sql);
$st->execute();
while (@row = $st->fetchrow_array) {
    push (@label1, "$row[0] (Quad Name)");
    push (@id, $row[1]);
    push (@src, "USGS 7.5min Quad Names");
}

## prepare the name query
$sql = qq/SELECT distinct mtrs,'pls:' || mtrs FROM pls_all where upper(mtrs) like upper('$name%') order by mtrs limit 20/;

$st = $dbh->prepare($sql);
$st->execute();
while (@row = $st->fetchrow_array) {
    push (@label1, "$row[0]");
    push (@id, $row[1]);
    push (@src, "Public Land Survey System");
}

## prepare the name query
#$sql = qq/SELECT distinct name, clip, type FROM placeclip where type != 'Watershed' and upper(name) like upper('$name%') order by name limit 20/;

$sql = qq/SELECT distinct feature_name, 'gnis:' || feature_id,feature_class FROM calgnis where feature_class <> 'Populated Place' and upper(feature_name) like upper('$name%') order by feature_name limit 10/;

$st = $dbh->prepare($sql);
$st->execute();

while (@row = $st->fetchrow_array) {
    push (@label1, "$row[0] - $row[2]");
    push (@id, $row[1]);
    push (@src, "GNIS California Place Names");
}

}
elsif ($src eq 'city') {
## prepare the name query
   $sql = qq/SELECT distinct name,label FROM city2k where upper(label) like upper('$name%') order by name limit 10/;

   $st = $dbh->prepare($sql);
   $st->execute();

   while (@row = $st->fetchrow_array) {
    push (@label1, "$row[1]");
    push (@id, "city2k:$row[0]");
   }
}
elsif ($src eq 'title') {
    $dbh->disconnect;
    $dbh= DBI->connect("dbi:Pg:dbname=gforge");
    $dbh->do("SET search_path TO ceic, public");

    if ($mode eq 'substr') {
	$where = qq[ where d.ceic_node_id = n.id and upper(d.title) LIKE upper('%$name%')];
    }
    else {
	$where = qq[ where d.ceic_node_id = n.id and upper(d.title) LIKE upper('$name%')];
    }
    if ($typeList) {
	my @type = split(/,/, $typeList);
	my $quotedTypes = join("','",@type); 
	$where .= qq[ and upper(d.category) IN ('$quotedTypes')];
	$whereText .= qq[resource type IN ('$typeList') ];
    }

    $sql = qq/SELECT d.id, d.title, (n.node_url || d.filename), d.link_url, n.title, d.category FROM dataset d, ceic_node n $where order by d.title limit 60/;
    $st = $dbh->prepare($sql);
    $st->execute();
    while (@row = $st->fetchrow_array) {
	push(@label1,"$row[1]");
	push(@id,"$row[0]");
	push(@src,"$row[4]");
	push(@cat,"$row[5]");
	if ($row[3] =~ /nrpi/i) {
	    push(@url,"$row[3]");
	}
	else {
	    push(@url,"$row[2]");
	}
    }
}
elsif ($src =~ /org/i) {
    $dbh->disconnect;
    $dbh= DBI->connect("dbi:Pg:dbname=gforge");
    $dbh->do("SET search_path TO ceic, public");

    if ($mode eq 'substr') {
	$where = qq[ where o.dataset_id = d.id and upper(o.name) LIKE upper('%$name%')];
    }
    else {
	$where = qq[ where o.dataset_id = d.id and upper(o.name) LIKE upper('$name%')];
    }
    if ($node) {
	$where .= qq[ and d.ceic_node_id = $node];
    }
    $sql = qq/SELECT distinct name FROM dataset_org o, dataset d $where order by o.name limit 60/;
    $st = $dbh->prepare($sql);
    $st->execute();
    while (@row = $st->fetchrow_array) {
	$lbl = $row[0];
	push(@label1,$lbl);
	push(@id,"$row[0]");
    }
}
elsif ($src =~ /funder/i) {
    $dbh->disconnect;
    $dbh= DBI->connect("dbi:Pg:dbname=gforge");
    $dbh->do("SET search_path TO ceic, public");

    if ($mode eq 'substr') {
	$where = qq[ where upper(funder) LIKE upper('%$name%')];
    }
    else {
	$where = qq[ where upper(funder) LIKE upper('$name%')];
    }
    $sql = qq/SELECT distinct funder FROM dataset_funding $where order by funder limit 60/;
    $st = $dbh->prepare($sql);
    $st->execute();
    while (@row = $st->fetchrow_array) {
	$lbl = $row[0];
	push(@label1,$lbl);
	push(@id,"$row[0]");
    }
}
elsif ($src =~ /fund/i) {
    $dbh->disconnect;
    $dbh= DBI->connect("dbi:Pg:dbname=gforge");
    $dbh->do("SET search_path TO ceic, public");

    if ($mode eq 'substr') {
	$where = qq[ where upper(program_type) LIKE upper('%$name%')];
    }
    else {
	$where = qq[ where upper(program_type) LIKE upper('$name%')];
    }
    $sql = qq/SELECT distinct program_type FROM dataset_funding $where order by program_type limit 60/;
    $st = $dbh->prepare($sql);
    $st->execute();
    while (@row = $st->fetchrow_array) {
	$lbl = $row[0];
	push(@label1,$lbl);
	push(@id,"$row[0]");
    }
}
elsif ($src eq 'keyword') {
    $dbh->disconnect;
    $dbh= DBI->connect("dbi:Pg:dbname=gforge");
    $dbh->do("SET search_path TO ceic, public");

    if ($mode eq 'substr') {
	$where = qq[ where upper(themekey) LIKE upper('%$name%')];
    }
    else {
	$where = qq[ where upper(themekey) LIKE upper('$name%')];
    }
    $sql = qq/SELECT distinct themekey FROM dataset_theme $where order by themekey limit 60/;
    $st = $dbh->prepare($sql);
    $st->execute();
    while (@row = $st->fetchrow_array) {
	$lbl = $row[0];
	push(@label1,$lbl);
	push(@id,"$row[0]");
    }
}
elsif ($src eq 'projectTheme') {
    $dbh->disconnect;
    $dbh= DBI->connect("dbi:Pg:dbname=gforge");
    $dbh->do("SET search_path TO ceic, public");

    if ($mode eq 'substr') {
	$where = qq[ where themekt = 'NRPI Keywords' and upper(themekey) LIKE upper('%$name%')];
    }
    else {
	$where = qq[ where themekt = 'NRPI Keywords' and upper(themekey) LIKE upper('$name%')];
    }
    $sql = qq/SELECT distinct themekey FROM dataset_theme $where order by themekey limit 60/;
    $st = $dbh->prepare($sql);
    $st->execute();
    while (@row = $st->fetchrow_array) {
	$lbl = $row[0];
	push(@label1,$lbl);
	push(@id,"$row[0]");
    }
}
elsif ($src eq 'projectType') {
    $dbh->disconnect;
    $dbh= DBI->connect("dbi:Pg:dbname=gforge");
    $dbh->do("SET search_path TO ceic, public");

    if ($mode eq 'substr') {
	$where = qq[ where upper(term) LIKE upper('%$name%')];
    }
    else {
	$where = qq[ where upper(term) LIKE upper('$name%')];
    }
    $sql = qq/SELECT distinct term FROM vocab $where order by term limit 60/;
    $st = $dbh->prepare($sql);
    $st->execute();
    while (@row = $st->fetchrow_array) {
	$lbl = $row[0];
	push(@label1,$lbl);
	push(@id,"$row[0]");
    }
}
elsif ($src eq 'fundSrc') {
    $dbh->disconnect;
    $dbh= DBI->connect("dbi:Pg:dbname=gforge");
    $dbh->do("SET search_path TO ceic, public");

    if ($mode eq 'substr') {
	$where = qq[ where upper(name) LIKE upper('%$name%')];
    }
    else {
	$where = qq[ where upper(name) LIKE upper('$name%')];
    }
    $sql = qq/SELECT distinct name FROM fund $where order by name limit 60/;
    $st = $dbh->prepare($sql);
    $st->execute();
    while (@row = $st->fetchrow_array) {
	$lbl = $row[0];
	push(@label1,$lbl);
	push(@id,"$row[0]");
    }
}
elsif ($src eq 'node') {
    $dbh->disconnect;
    $dbh= DBI->connect("dbi:Pg:dbname=gforge");
    $dbh->do("SET search_path TO ceic, public");

    if ($mode eq 'substr') {
	$where = qq[ where upper(title) LIKE upper('%$name%')];
    }
    else {
	$where = qq[ where upper(title) LIKE upper('$name%')];
    }
    $sql = qq/SELECT id,title,shortname FROM ceic_node $where order by shortname limit 60/;
    $st = $dbh->prepare($sql);
    $st->execute();
    while (@row = $st->fetchrow_array) {
	$lbl = $row[1];
	push(@label1,$lbl);
	push(@id,"$row[0]");
	push(@shortname,"$row[2]");
    }
}
else {
    $src = 'place';
    if ($mode eq 'substr') {
	$where = qq[ where upper(feature_name) LIKE upper('%$name%')];
    }
    else {
	$where = qq[ where upper(feature_name) LIKE upper('$name%')];
    }

    if ($county) {
       my @counties = split(/,/, $county);
       my $countyList = join("','",@counties); 
       $countyList = uc($countyList);
       $where .= qq[ and upper(county_name) IN  ('$countyList')];
    }
    $where .= qq[ and upper(feature_name) LIKE '%COUNTY'] if ($placetype eq 'county');
    $where .= qq[ and lower(feature_class) IN ('$typeList')] if ($placetype);
    if ($srid) {
	$sql = qq/SELECT distinct feature_name,feature_class,county_name,sortorder,feature_id,astext(transform(geometryFromText('POINT(' || prim_long_dec || ' ' || prim_lat_dec || ')',4269),$srid)) FROM calgnis $where order by sortorder,feature_name,county_name limit 60/;
    }
    else {
	$sql = qq/SELECT distinct feature_name,feature_class,county_name,sortorder,feature_id,prim_lat_dec,prim_long_dec FROM calgnis $where order by sortorder,feature_name,county_name limit 60/;
    }

#    print $sql;
    $st = $dbh->prepare($sql);
    $st->execute();
    my @pts;
    my $ptStr;
    while (@row = $st->fetchrow_array) {
	if ($srid) {
	    $ptStr = $row[5];
	    $ptStr =~ s/POINT\(//i;
	    $ptStr =~ s/\)//i;
	    @pts = split(/ /,$ptStr);
	    $lat = $pts[1];
	    $lon = $pts[0];
	}
	else {
	    $lat = $row[5];
	    $lon = $row[6];
	}

#	$lbl = $row[0] . " - " . $row[2] . "(" . $row[1] . ")";
	$lbl = $row[0] . " - " . $row[2];
        $lbl2 = $row[0];
	$typ = $row[1];
	if (($lbl2 =~ /county/i) && ($typ =~ /civil/i)) {
	    $typ = 'County';
	    $lbl2 =~ s/ County//i
	    }

	$typ = 'Town/Area' if ($typ =~ /ppl/i);
	$typ = 'State' if (($lbl2 =~ /California/i) && ($typ =~ /civil/i));

#	if ($typ =~ /Town\/Area|County|State/i) {

	## preventing some repeating names with multi entries in GNIS
	my $currentNames = join(' ',@label1);
	if ((!($typ =~ /bridge|locale|build|church/i)) && (!($currentNames =~ /$lbl/i))) {
	push(@lat,$lat);
	push(@lon,$lon);
	push(@label1,$lbl);
	push(@pType,$typ);
	push(@label2,$lbl2);
	push(@id,"gnis:$row[4]");
    }
    }
}

 my $jsonText = '';
if ($callback) { $jsonText = qq/$callback(/ }
$jsonText .=  qq/{"ResultSet":{"Type":"$src","Result":[/;
for $i (0..((@id) - 1)) {
    if ($placetype eq 'jm') {
     $jsonText .= "{\"id\":\"$id[$i]\",\"label\":\"$label2[$i]\",\"type\":\"$pType[$i]\",\"lat\":\"$lat[$i]\",\"lon\":\"$lon[$i]\"},"
     }
    elsif ($src eq 'title') {
     $jsonText .= "{\"id\":\"$id[$i]\",\"label\":\"$label1[$i]\",\"url\":\"$url[$i]\",\"src\":\"$src[$i]\",\"cat\":\"$cat[$i]\"},"
     }
    elsif ($src eq 'place') {
     $jsonText .= "{\"id\":\"$id[$i]\",\"label\":\"$label1[$i]\",\"type\":\"$pType[$i]\",\"lat\":\"$lat[$i]\",\"lon\":\"$lon[$i]\"},"
     }
    elsif ($src eq 'node') {
     $jsonText .= "{\"id\":\"$id[$i]\",\"label\":\"$label1[$i]\",\"shortname\":\"$shortname[$i]\"},";
     }
    else {
     $jsonText .= "{\"id\":\"$id[$i]\",\"label\":\"$label1[$i]\",\"src\":\"$src[$i]\"},"
     }
}

$jsonText =~ s/\,$//;
$jsonText .= qq/]}}/;
if ($callback) { $jsonText .= qq/)/ }
print $jsonText;



