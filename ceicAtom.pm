#!/usr/bin/perl
use XML::DOM;
use XML::Writer;

sub writeAtomFeed {

    $writer = shift;
    $writer->startTag("title");
    $writer->characters("California Environmental Information Clearinghouse");
    $writer->endTag('title');
    $writer->startTag("id");
    $writer->characters("http://atlas.ca.gov/");
    $writer->endTag("id");
    $writer->startTag("updated");
    $writer->characters("2006-12-04T00:00:00Z");
    $writer->endTag("updated");
    $writer->startTag("link","href" => "http://atlas.ca.gov/ceic/");
    $writer->endTag('link');
    $writer->startTag("link","rel" => "self","href" => "http://atlas.ca.gov/ceic/ceicTest.xml");
    $writer->endTag('link');
    $writer->startTag("icon");
    $writer->characters("http://atlas.ca.gov/ceic/images/ceres_40.png");
    $writer->endTag('icon');
    $writer->startTag("author");
    $writer->startTag("name");
    $writer->characters("California Resources Agency CERES Program");
    $writer->endTag('name');
    $writer->endTag('author');
    $writer->startTag("subtitle");
    $writer->characters("Selected Entries from the CEIC Catalog");
    $writer->endTag('subtitle');
} 

sub writeRSSChannel {

    $writer = shift;
    $writer->startTag("title");
    $writer->characters("California Environmental Information Clearinghouse");
    $writer->endTag('title');
    $writer->startTag("link");
    $writer->characters("http://atlas.ca.gov/ceic/");    
    $writer->endTag('link');
    $writer->startTag("webMaster");
    $writer->characters("metadata\@ceres.ca.gov");
    $writer->endTag('webMaster');
    $writer->startTag("description");
    $writer->characters("Selected Entries from the CEIC Catalog");
    $writer->endTag('description');
} 

sub writeRSSEntry {

    $writer = shift;
    $dat = shift; 

    $writer->startTag("item");

    my $pt = $dat->[7];
    $pt =~ s/POINT\(//;
    $pt =~ s/\)//;
    my @ptArray = split(/\s/,$pt);

    $writer->startTag("geo:lat");
    $writer->characters("$ptArray[1]");
    $writer->endTag("geo:lat");
    $writer->startTag("geo:long");
    $writer->characters("$ptArray[0]");
    $writer->endTag("geo:long");

    $writer->startTag("title");
    $writer->characters($dat->[1]);
    $writer->endTag("title");
    $writer->startTag("description");
#    my $OK_CHARS='-a-zA-Z0-9_.@\s,'; # A restrictive list of metacharacters
#    $dat->[2] =~ s/[^$OK_CHARS]/_/go;
    $writer->characters($dat->[2]);
    $writer->endTag("description");
    $writer->startTag("link");
    $writer->characters("$dat->[8]");
    $writer->endTag("link");
    $writer->startTag("guid");
    $writer->characters("$dat->[8]");
    $writer->endTag("guid");

    $writer->endTag("item");
}

sub writeAtomEntry {

    $writer = shift;
    $dat = shift; 

    $writer->startTag("entry");
    $writer->startTag("id");
#    $writer->characters("http://atlas.ca.gov/ceic/api/objInfo.php?$dat->[0]");
    $writer->characters("$dat->[0]");
    $writer->endTag("id");
    $writer->startTag("title");
    $writer->characters($dat->[1]);
    $writer->endTag("title");
    $writer->startTag("summary");
##    $writer->characters($dat->[2]);
    $writer->endTag("summary");
    $writer->startTag("updated");
    my $ts = $dat->[4];
    $ts =~ s/\s/T/;
    $writer->characters($ts);
    $writer->endTag("updated");
    $writer->startTag("link","href" => "$dat->[5]");
    $writer->endTag("link");
    $writer->startTag("link","href" => "$dat->[5]");
    $writer->endTag("link");

    my $pt = $dat->[7];
    $pt =~ s/POINT\(//;
    $pt =~ s/\)//;
    my @ptArray = split(/\s/,$pt);
    $writer->startTag("geo:Point");
    $writer->startTag("geo:lat");
    $writer->characters("$ptArray[1]");
    $writer->endTag("geo:lat");
    $writer->startTag("geo:long");
    $writer->characters("$ptArray[0]");
    $writer->endTag("geo:long");
    $writer->endTag("geo:Point");
    $writer->endTag("entry");
}

1;
